from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from dynamic_preferences.registries import global_preferences_registry
from places.apis.internals.base_viewset import BaseViewSet
from places.serializers import RegionSerializer
from places.models import Region


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
global_preferences = global_preferences_registry.manager()


class RegionViewSet(BaseViewSet):
    """
    create:
        End point for Region
    retrieve:
        End point for Region
    update:
        End point for Region
    delete:
        End point for Region
    """
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    model = 'region'
