from .country import CountryViewSet
from .region import RegionViewSet
from .commune import CommuneViewSet
from .street import StreetViewSet
from .street_location import StreetLocationViewSet
from .tower import TowerViewSet
from .floor import FloorViewSet
from .complete_location import CompleteLocationViewSet

__all__ = [
    "CountryViewSet",
    "RegionViewSet",
    "CommuneViewSet",
    "StreetViewSet",
    "StreetLocationViewSet",
    "TowerViewSet",
    "FloorViewSet",
    "CompleteLocationViewSet"

]