from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from dynamic_preferences.registries import global_preferences_registry
from places.apis.internals.base_viewset import BaseViewSet
from places.serializers import StreetSerializer
from places.models import Street


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
global_preferences = global_preferences_registry.manager()


class StreetViewSet(BaseViewSet):
    """
    create:
        End point for Street
    retrieve:
        End point for Street
    update:
        End point for Street
    delete:
        End point for Street
    """
    queryset = Street.objects.all()
    serializer_class = StreetSerializer
    model = 'street'
