from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from dynamic_preferences.registries import global_preferences_registry
from places.apis.internals.base_viewset import BaseViewSet
from places.serializers import FloorSerializer
from places.models import Floor


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
global_preferences = global_preferences_registry.manager()


class FloorViewSet(BaseViewSet):
    """
    create:
        End point for Floor
    retrieve:
        End point for Floor
    update:
        End point for Floor
    delete:
        End point for Floor
    """
    queryset = Floor.objects.all()
    serializer_class = FloorSerializer
    model = 'floor'
