from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from dynamic_preferences.registries import global_preferences_registry
from places.apis.internals.base_viewset import BaseViewSet
from places.serializers import TowerSerializer
from places.models import Tower


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
global_preferences = global_preferences_registry.manager()


class TowerViewSet(BaseViewSet):
    """
    create:
        End point for Tower
    retrieve:
        End point for Tower
    update:
        End point for Tower
    delete:
        End point for Tower
    """
    queryset = Tower.objects.all()
    serializer_class = TowerSerializer
    model = 'tower'
