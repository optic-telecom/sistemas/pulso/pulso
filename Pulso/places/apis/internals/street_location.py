from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from dynamic_preferences.registries import global_preferences_registry
from places.apis.internals.base_viewset import BaseViewSet
from places.serializers import StreetLocationSerializer
from places.models import StreetLocation


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
global_preferences = global_preferences_registry.manager()


class StreetLocationViewSet(BaseViewSet):
    """
    create:
        End point for StreetLocation
    retrieve:
        End point for StreetLocation
    update:
        End point for StreetLocation
    delete:
        End point for StreetLocation
    """
    queryset = StreetLocation.objects.all()
    serializer_class = StreetLocationSerializer
    model = 'streetlocation'
