from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from dynamic_preferences.registries import global_preferences_registry
from places.apis.internals.base_viewset import BaseViewSet
from places.serializers import CompleteLocationSerializer
from places.models import CompleteLocation


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
global_preferences = global_preferences_registry.manager()


class CompleteLocationViewSet(BaseViewSet):
    """
    create:
        End point for CompleteLocation
    retrieve:
        End point for CompleteLocation
    update:
        End point for CompleteLocation
    delete:
        End point for CompleteLocation
    """
    queryset = CompleteLocation.objects.all()
    serializer_class = CompleteLocationSerializer
    model = 'completelocation'
