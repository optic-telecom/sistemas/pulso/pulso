from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator
from rest_framework import status
from rest_framework import viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from dynamic_preferences.registries import global_preferences_registry
from utils.views import send_webhook_transaction
from utils.permissions import IsSystemInternalPermission


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
global_preferences = global_preferences_registry.manager()


@method_decorator(cache_page(CACHE_TTL), name='dispatch')
class BaseViewSet(viewsets.ModelViewSet):

    permission_classes = (IsSystemInternalPermission | IsAuthenticated,)
    model: str = None

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        debug = settings.DEBUG
        if debug:
            return [AllowAny(), ]
        else:
            return [permission() for permission in self.permission_classes]

    def create(self, request, *args, **kwargs):
        """
        Envía una transacción al endpoint de
        WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        # Si tiene replicate ya se replicó
        if not replicate:
            send_webhook_transaction(
                model=self.model, action=1, data=serializer.data
                )
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data,
            status=status.HTTP_201_CREATED,
            headers=headers
            )

    def destroy(self, request, *args, **kwargs):
        """
        Envía una transacción al endpoint de WebHookTransaction
        si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        instande_id = instance.id
        self.perform_destroy(instance)
        # Si tiene replicate ya se replicó
        if not replicate:
            data = {
                'id': str(instande_id)
            }
            send_webhook_transaction(model=self.model, action=3, data=data)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        """
        Envía una transacción al endpoint de
        WebHookTransaction si no se ha replicado aún
        """
        partial = kwargs.pop('partial', False)
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=request.data, partial=partial
            )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        # Si tiene replicate ya se replicó
        if not replicate:
            send_webhook_transaction(
                model=self.model, action=2, data=serializer.data
                )

        return Response(serializer.data)
