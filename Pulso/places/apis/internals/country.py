from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from dynamic_preferences.registries import global_preferences_registry
from places.apis.internals.base_viewset import BaseViewSet
from places.serializers import CountrySerializer
from places.models import Country


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
global_preferences = global_preferences_registry.manager()


class CountryViewSet(BaseViewSet):
    """
    create:
        End point for Country
    retrieve:
        End point for Country
    update:
        End point for Country
    delete:
        End point for Country
    """
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    model = 'country'
