from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from dynamic_preferences.registries import global_preferences_registry
from places.apis.internals.base_viewset import BaseViewSet
from places.serializers import CommuneSerializer
from places.models import Commune


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
global_preferences = global_preferences_registry.manager()


class CommuneViewSet(BaseViewSet):
    """
    create:
        End point for Commune
    retrieve:
        End point for Commune
    update:
        End point for Commune
    delete:
        End point for Commune
    """
    queryset = Commune.objects.all()
    serializer_class = CommuneSerializer
    model = 'commune'
