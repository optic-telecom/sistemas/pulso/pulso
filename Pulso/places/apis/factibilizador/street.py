from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from places.models import Street
from places.serializers import StreetSerializer
from places.apis.factibilizador.base_viewset import SearchManagerViewSet

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@method_decorator(cache_page(CACHE_TTL), name='dispatch')
class SearchStreetViewSet(SearchManagerViewSet):
    """ Calles para Factibilizador """
    queryset = Street.objects.order_by('name')
    serializer_class = StreetSerializer
    param = 'street'
    filter_by = 'commune'
