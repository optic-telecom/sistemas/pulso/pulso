from django.conf import settings
from django.db.models import Q
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from places.models import CompleteLocation
from places.serializers import CompleteLocationSz
from places.apis.factibilizador.base_viewset import SearchManagerViewSet

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@method_decorator(cache_page(CACHE_TTL), name='dispatch')
class SearchCompleteLocationViewSet(SearchManagerViewSet):
    """ Direcciones completas para Factibilizador """
    serializer_class = CompleteLocationSz

    def customFilter(self, qs):
        argumento = self.request.GET.get('floor_num', None)
        if argumento:
            _list_word = argumento.split()
            query = Q()
            for word in _list_word:
                query = (
                        query &
                        Q(floor__number__istartswith=word) |
                        Q(number__istartswith=word)
                    )
            qs = qs.filter(query)
        return qs

    def get_queryset(self):
        filtro = dict(
            street_location__id=self.kwargs.get('streetlocation', None)
            )
        qs = (CompleteLocation.objects.filter(
                **filtro
            ).order_by(
                'floor__floor_num',
                'floor__number',
                'id',
            )
            )
        return qs
