from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from places.models import Commune
from places.serializers import CommuneSerializer
from places.apis.factibilizador.base_viewset import SearchManagerViewSet

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@method_decorator(cache_page(CACHE_TTL), name='dispatch')
class SearchCommuneViewSet(SearchManagerViewSet):
    """ Comunas para Factibilizador """
    serializer_class = CommuneSerializer
    queryset = Commune.objects.order_by('name')
    param = 'commune'
    filter_by = 'region'
