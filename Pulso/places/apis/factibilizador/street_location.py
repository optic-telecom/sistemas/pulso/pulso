from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from places.models import StreetLocation
from places.serializers import StreetLocationSerializer
from places.apis.factibilizador.base_viewset import SearchManagerViewSet

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@method_decorator(cache_page(CACHE_TTL), name='dispatch')
class SearchStreetLocationViewSet(SearchManagerViewSet):
    """ Ubicaciones por calle para Factibilizador """
    queryset = StreetLocation.objects.order_by('id')
    serializer_class = StreetLocationSerializer
    param = 'streetlocation'
    lookup = 'street_no__istartswith'
    filter_by = 'street'
