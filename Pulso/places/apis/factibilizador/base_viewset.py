from django.db.models import Q
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from utils.permissions import IsSystemInternalPermission
from dynamic_preferences.registries import global_preferences_registry
CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
global_preferences = global_preferences_registry.manager()


# BUSQUEDA ESCALONADA
@method_decorator(cache_page(CACHE_TTL), name='dispatch')
class SearchManagerViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Clase encargada de manejar los filtros a través de un lookup
    apoyada por un parametro get que varia según la clase
    que hereda
    """
    permission_classes = (IsSystemInternalPermission | IsAuthenticated,)
    param = None
    lookup = 'name__icontains'
    filter_by = None

    def get_queryset(self):
        qs = super().get_queryset()
        if self.filter_by:
            arg = self.kwargs.get(self.filter_by, None)
            filtro = {f'{self.filter_by}__id': arg}
            qs = qs.filter(**filtro)
        return qs

    def customFilter(self, qs):
        argumento = self.request.GET.get(self.param.lower(), None)
        if argumento:
            _list_word = argumento.split()
            query = Q()
            for word in _list_word:
                query = query & Q(**{self.lookup: word})
            qs = qs.filter(query)
        return qs

    def list(self, request, *args, **kwargs):
        qs = self.customFilter(self.get_queryset())
        serializer = self.serializer_class(qs, many=True)
        return Response(serializer.data)
