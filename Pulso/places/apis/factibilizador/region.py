from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from places.models import Region
from places.serializers import RegionSerializer
from places.apis.factibilizador.base_viewset import SearchManagerViewSet

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@method_decorator(cache_page(CACHE_TTL), name='dispatch')
class SearchRegionViewSet(SearchManagerViewSet):
    """ Regiones para Factibilizador """
    serializer_class = RegionSerializer
    queryset = Region.objects.order_by('name')
    param = 'region'
    lookup = 'name__icontains'
