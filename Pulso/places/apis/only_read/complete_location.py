from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
# from rest_framework.response import Response
from places.models import CompleteLocation
from places.serializers import CompleteLocationSz
from dynamic_preferences.registries import global_preferences_registry
from utils.permissions import IsSystemInternalPermission


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
global_preferences = global_preferences_registry.manager()


@method_decorator(cache_page(CACHE_TTL), name='dispatch')
class CompleteLocationRead(viewsets.ReadOnlyModelViewSet):
    queryset = CompleteLocation.objects.order_by('id')
    serializer_class = CompleteLocationSz
    permission_classes = (IsSystemInternalPermission | IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        try:
            return self.retrieve(request, *args, **kwargs)
        except Exception as e:
            print(str(e))
            return super().list(request, *args, **kwargs)

    def get_queryset(self):
        qs = super().get_queryset()

        country = self.request.query_params.get('country', "")
        region = self.request.query_params.get('region', "")
        commune = self.request.query_params.get('commune', "")
        street = self.request.query_params.get('street', "")
        street_location = self.request.query_params.get('street_location', "")
        house_number = self.request.query_params.get('house_number', "")

        if country:
            filtro = {
                'street_location__street__commune__region__country__name__istartswith': country
                }
            qs = qs.filter(**filtro)

        if region:
            filtro = {
                'street_location__street__commune__region__name__icontains': region
                }
            qs = qs.filter(**filtro)

        if commune:
            filtro = {
                'street_location__street__commune__name__icontains': commune
                }
            qs = qs.filter(**filtro)

        if street:
            filtro = {
                'street_location__street__name__icontains': street
                }
            qs = qs.filter(**filtro)

        if street_location:
            filtro = {
                'street_location__street_no__icontains': street_location
                }
            qs = qs.filter(**filtro)

        if house_number:
            filtro = {
                'number__icontains': house_number
                }
            qs = qs.filter(**filtro)

        return qs

