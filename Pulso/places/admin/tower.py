from django.contrib import admin
from utils.admin import BaseAdmin

from places.models import (
        Tower
    )

from places.forms import (
        TowerForm,
    )


@admin.register(Tower)
class TowerAdmin(BaseAdmin):
    form = TowerForm
    list_display = (
        'id',
        'name',
        'street_location',
        'get_street',
        'get_commune',
        'get_region',
        'get_country'
    )
    list_filter = ('street_location__street__commune__region',)
    search_fields = (
        'street_location__street__commune__region__name',
        'street_location__street__commune__name',
        'street_location__street__name',
        'name',
    )

    def get_country(self, obj):
        return obj.street_location.street.commune.region.country
    get_country.short_description = 'Country'

    def get_region(self, obj):
        return obj.street_location.street.commune.region
    get_region.short_description = 'Region'

    def get_commune(self, obj):
        return obj.street_location.street.commune
    get_commune.short_description = 'Commune'

    def get_street(self, obj):
        return obj.street_location.street
    get_street.short_description = 'Street'

    fieldsets = (
        ('Datos Generales', {
            # 'classes': ('collapse',),
            'fields': ('name',)
        }),
        ('Ubicación', {
            # 'classes': ('collapse',),
            'fields': ('region', 'commune', 'street', 'street_location',)
        }),

    )