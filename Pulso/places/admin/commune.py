from django.contrib import admin
from utils.admin import BaseAdmin
from places.models import (Commune)

from places.forms import (
    CommuneForm,
    )


@admin.register(Commune)
class CommuneAdmin(BaseAdmin):
    form = CommuneForm
    list_display = ('id', 'name', 'region', 'get_country')
    search_fields = ('region__name', 'name')
    list_filter = ('region',)
    autocomplete_fields = ("region", )

    def get_country(self, obj):
        return obj.region.country
    get_country.short_description = 'Country'

    fieldsets = (
        ('Datos Generales', {
            # 'classes': ('collapse',),
            'fields': ('name',)
        }),
        ('Ubicación', {
            # 'classes': ('collapse',),
            'fields': ('region',)
        }),

    )