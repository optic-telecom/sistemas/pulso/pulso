from django.contrib import admin
from places.models import (
        Direcciones,
    )


@admin.register(Direcciones)
class DireccionesAdmin(admin.ModelAdmin):
    change_list_template = 'admin/CompleteLocationAdmin.html'
    change_form_template = 'admin/CompleteLocationAdmin.html'
