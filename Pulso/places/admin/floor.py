from django.contrib import admin
from utils.admin import BaseAdmin

from places.models import (
        Floor
    )

from places.forms import (
        FloorForm
    )


@admin.register(Floor)
class FloorAdmin(BaseAdmin):
    form = FloorForm
    list_display = ('tower', 'number', 'floor_num', 'get_street_location',
                    'get_street', 'get_commune', 'get_region', 'get_country')
    list_filter = ('tower__street_location__street__commune__region',)
    search_fields = (
        'tower__street_location__street__commune__region__name',
        'tower__street_location__street__commune__name',
        'tower__street_location__street__name',
        'number',
    )

    def get_country(self, obj):
        return obj.tower.street_location.street.commune.region.country
    get_country.short_description = 'Country'

    def get_region(self, obj):
        return obj.tower.street_location.street.commune.region
    get_region.short_description = 'Region'

    def get_commune(self, obj):
        return obj.tower.street_location.street.commune
    get_commune.short_description = 'Commune'

    def get_street(self, obj):
        return obj.tower.street_location.street
    get_street.short_description = 'Street'

    def get_street_location(self, obj):
        return obj.tower.street_location
    get_street_location.short_description = 'Street Location'

    fieldsets = (
        ('Ubicación', {
            # 'classes': ('collapse',),
            'fields': (
                'region',
                'commune',
                'street',
                'street_location',
                'tower'
            )
        }),
        ('Apartamento', {
            'classes': ('collapse',),
            'fields': ('floor_num', 'number')
        }),
    )
