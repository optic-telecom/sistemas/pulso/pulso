from django.contrib import admin
from utils.admin import BaseAdmin
from places.models import (Street)

from places.forms import (
    StreetForm,
    )


@admin.register(Street)
class StreetAdmin(BaseAdmin):
    form = StreetForm
    list_display = ('name', 'commune', 'get_region', 'get_country')
    search_fields = ('commune__region__name', 'commune__name', 'name')
    list_filter = ('commune__region',)
    autocomplete_fields = ("commune", )

    def get_country(self, obj):
        return obj.commune.region.country
    get_country.short_description = 'Country'

    def get_region(self, obj):
        return obj.commune.region
    get_region.short_description = 'Region'

    fieldsets = (
        ('Datos Generales', {
            # 'classes': ('collapse',),
            'fields': ('name',)
        }),
        ('Ubicación', {
            # 'classes': ('collapse',),
            'fields': (
                'region',
                'commune',)
        }),

    )