from django.contrib import admin
from utils.admin import BaseAdmin
from places.models import Country


@admin.register(Country)
class CountryAdmin(BaseAdmin):
    list_display = ('id', 'code', 'name')
    list_filter = ('name', )
    search_fields = ("name", )