from .country import CountryAdmin
from .region import RegionAdmin
from .commune import CommuneAdmin
from .street import StreetAdmin
from .street_location import StreetLocationAdmin
from .tower import TowerAdmin
from .floor import FloorAdmin
from .complete_location import CompleteLocationAdmin

from .proxy import (
    DireccionesAdmin
)

__all__ = [
    "CountryAdmin",
    "RegionAdmin",
    "CommuneAdmin",
    "StreetAdmin",
    "StreetLocationAdmin",
    "TowerAdmin",
    "FloorAdmin",
    "CompleteLocationAdmin",
    "DireccionesAdmin",

]