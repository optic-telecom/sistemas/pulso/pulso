from django.contrib import admin
from utils.admin import BaseAdmin

from places.models import (
        StreetLocation
    )

from places.forms import (
        StreetLocationForm
    )


@admin.register(StreetLocation)
class StreetLocationAdmin(BaseAdmin):
    form = StreetLocationForm

    list_display = (
        'street_no',
        'street',
        'commune',
        'get_region',
        'get_country',
        'edification_type'
        )
    list_filter = ('street__commune__region', 'edification_type',)
    search_fields = (
        'street__commune__region__name',
        'street__commune__name',
        'street__name',
    )

    def get_country(self, obj):
        return obj.street.commune.region.country
    get_country.short_description = 'Country'

    def get_region(self, obj):
        return obj.street.commune.region
    get_region.short_description = 'Region'

    def get_commune(self, obj):
        return obj.street.commune
    get_commune.short_description = 'Commune'

    fieldsets = (
        ('Datos Generales', {
            # 'classes': ('collapse',),
            'fields': ('street_no', 'edification_type', 'homepassed',)
        }),
        ('Ubicación', {
            # 'classes': ('collapse',),
            'fields': ('region', 'commune', 'street',)
        }),

        ('Coordenadas', {
            # 'classes': ('collapse',),
            'fields': ('gps_latitude', 'gtd_longitude')
        }),

    )