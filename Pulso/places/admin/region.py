from django.contrib import admin
from utils.admin import BaseAdmin
from places.models import Region


@admin.register(Region)
class RegionAdmin(BaseAdmin):
    list_display = ('id', 'id_region', 'name', 'country')
    search_fields = ('country__name', 'name')
    list_filter = ('country__name',)
    autocomplete_fields = ("country", )
