from django.contrib import admin
from utils.admin import BaseAdmin

from places.models import (
        CompleteLocation,
    )

from places.forms import (
        CompleteLocationForm,
    )


@admin.register(CompleteLocation)
class CompleteLocationAdmin(BaseAdmin):
    form = CompleteLocationForm
    list_display = ('get_tipo_edificacion', 'number', 'floor',
                    'street_location', 'get_street',
                    'get_commune', 'get_region', 'get_country')
    list_filter = (
        'street_location__street__commune__region',
        'street_location__edification_type'
    )
    search_fields = (
        'street_location__street__commune__region__name',
        'street_location__street__commune__name',
        'street_location__street__name',
        'floor__tower__name',
        'floor__floor_num',
    )
    readonly_fields = ('id', )

    def get_tipo_edificacion(self, obj):
        tipo_edif = obj.street_location.get_edification_type_display()
        return tipo_edif.upper()
    get_tipo_edificacion.short_description = 'Edification'

    def get_country(self, obj):
        return obj.street_location.street.commune.region.country
    get_country.short_description = 'Country'

    def get_region(self, obj):
        return obj.street_location.street.commune.region
    get_region.short_description = 'Region'

    def get_commune(self, obj):
        return obj.street_location.street.commune
    get_commune.short_description = 'Commune'

    def get_street(self, obj):
        return obj.street_location.street
    get_street.short_description = 'Street'

    fieldsets = (
        ('Datos Generales', {
            # 'classes': ('collapse',),
            'fields': ('id', 'number')
        }),
        ('Ubicación', {
            # 'classes': ('collapse',),
            'fields': ('region', 'commune', 'street', 'street_location')
        }),
        ('Solo si es Edificio', {
            'classes': ('collapse',),
            'fields': ('tower', 'floor',)
        }),


    )
