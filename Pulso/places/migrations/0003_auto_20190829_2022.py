# Generated by Django 2.2.3 on 2019-08-29 20:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('places', '0002_auto_20190826_1611'),
    ]

    operations = [
        migrations.AlterField(
            model_name='country',
            name='code',
            field=models.CharField(max_length=3, unique=True),
        ),
        migrations.AlterField(
            model_name='country',
            name='name',
            field=models.CharField(max_length=255, unique=True),
        ),
        migrations.AlterField(
            model_name='historicalcountry',
            name='code',
            field=models.CharField(db_index=True, max_length=3),
        ),
        migrations.AlterField(
            model_name='historicalcountry',
            name='name',
            field=models.CharField(db_index=True, max_length=255),
        ),
    ]
