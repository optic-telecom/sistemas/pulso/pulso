from django.db import models
from utils.models import BaseModel
from django.core.validators import MaxValueValidator, MinValueValidator


TYPE_BUILDING = 1
TYPE_CONDOMINIUM = 2
TYPE_HOUSE = 3
TYPE_COMPANY = 4
TYPE_TERRAIN = 5

LOCAL_COMERCIAL = 6
LOCAL_CORPORATIVO = 7
SITIO_ERIAZO = 8
BLOCK = 9
SITIO_AGRICOLA = 10

INDUSTRIA = 11
CENTRO_RECREATIVO = 12
EDIFICIO_CORPORATIVO = 13
EN_ACTUALIZACION = 14

EDIFICATION_TYPE_CHOICES = (
    (TYPE_BUILDING, "Edificio"),
    (TYPE_CONDOMINIUM, "Condominio"),
    (TYPE_HOUSE, "Casa"),
    (TYPE_COMPANY, "Empresa"),
    (TYPE_TERRAIN, "Terreno"),
    (LOCAL_COMERCIAL, 'Local comercial'),
    (LOCAL_CORPORATIVO, 'Local corporativo'),
    (SITIO_ERIAZO, 'Sitio eriazo'),
    (BLOCK, 'Block'),
    (SITIO_AGRICOLA, 'Sitio agricola'),
    (INDUSTRIA, 'Industria'),
    (CENTRO_RECREATIVO, 'Centro recreativo'),
    (EDIFICIO_CORPORATIVO, 'Edificio corporativo'),
    (EN_ACTUALIZACION, 'En actualizacion'),

)


class StreetLocationManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related(
            "street"
        )


class StreetLocation(BaseModel):
    """ Información de la ubicación dentro de una calle. """
    street_no = models.CharField(max_length=20)
    street = models.ForeignKey("Street", on_delete=models.CASCADE)
    edification_type = models.PositiveSmallIntegerField(
        choices=EDIFICATION_TYPE_CHOICES
        )
    homepassed = models.BooleanField(default=False)
    gps_latitude = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        validators=[
            MinValueValidator(-90),
            MaxValueValidator(90)
        ],
        blank=True,
        null=True
    )
    gtd_longitude = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        validators=[
            MinValueValidator(-180),
            MaxValueValidator(180)
        ],
        blank=True,
        null=True
    )
    id_address = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )
    id_address_full = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )
    objects = StreetLocationManager()

    def __str__(self):
        return (f'CALLE {self.street} NRO. {self.street_no}, \
                    COMUNA {self.street.commune}, \
                    REGION {self.street.commune.region},\
                    PAÍS {self.street.commune.region.country}')

    @property
    def street_no_format(self):
        self.street_no = self.street_no.replace("0.", "0")
        self.save()
        return self.street_no

    @property
    def commune(self):
        return self.street.commune.name

    @property
    def region(self):
        return self.street.commune.region.name

    class Meta:
        ordering = ["-street"]
        # unique_together = ['gps_latitude', 'gtd_longitude']