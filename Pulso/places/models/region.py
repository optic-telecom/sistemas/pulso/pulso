from django.db import models
from utils.models import BaseModel


class RegionManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related(
            "country"
        )


class Region(BaseModel):
    """ Información de las regiones de un país. """
    name = models.CharField(max_length=255)
    country = models.ForeignKey("Country", on_delete=models.CASCADE)
    id_region = models.CharField(
        max_length=5,
        verbose_name="Id Región",
        blank=True,
        null=True
    )

    objects = RegionManager()

    def __str__(self):
        return f'{self.name}'