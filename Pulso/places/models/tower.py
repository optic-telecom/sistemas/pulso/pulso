from django.db import models
from utils.models import BaseModel


class TowerManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related(
            "street_location"
        )


class Tower(BaseModel):
    """ Información de las torres de edificios y empresas. """
    name = models.CharField(max_length=255, default="1")
    street_location = models.ForeignKey(
        'StreetLocation',
        on_delete=models.CASCADE
    )
    objects = TowerManager()
    def __str__(self):
        return f'{self.name}, {self.street_location}'

    class Meta:
        ordering = ["-street_location"]
        unique_together = [
            'name',
            'street_location'
            ]
