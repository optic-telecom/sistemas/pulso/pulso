from django.db import models
from utils.models import BaseModel


class StreetManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related(
            "commune"
        )


class Street(BaseModel):
    """ Información de las calles de una comuna. """
    name = models.CharField(max_length=255)
    commune = models.ForeignKey(
        'Commune',
        on_delete=models.CASCADE
    )
    id_street = models.CharField(
        max_length=50,
        verbose_name="Id Street",
        blank=True,
        null=True
    )
    objects = StreetManager()

    class Meta:
        ordering = ["-commune"]
        unique_together = ['name', 'commune']

    def __str__(self):
        return f'{self.name}'
