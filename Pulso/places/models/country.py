from django.db import models
from utils.models import BaseModel
from django.core.validators import RegexValidator

CODE_REGEX = '^[A-Z]{2,3}$'
msg = 'El código debe ser letras en mayúsculas.'


class Country(BaseModel):
    """ Información de los países. """
    name = models.CharField(max_length=255, unique=True)
    code = models.CharField(
        max_length=3,
        unique=True,
        validators=[
            RegexValidator(CODE_REGEX, message=msg)
            ]
        )

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ["-code"]