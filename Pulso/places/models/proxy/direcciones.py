from places.models.country import Country


class Direcciones(Country):

    class Meta:
        proxy = True
        verbose_name = 'Dirección JSON'
        verbose_name_plural = 'Direcciones JSON'