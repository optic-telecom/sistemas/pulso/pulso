from django.db import models
from utils.models import BaseModel


class CommuneManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related(
            "region"
        )


class Commune(BaseModel):
    """ Información de las comunas de una región. """
    name = models.CharField(max_length=255)
    region = models.ForeignKey('Region', on_delete=models.CASCADE)
    id_municipality = models.CharField(
        max_length=5,
        verbose_name="Id Municipio",
        blank=True,
        null=True)
    
    objects = CommuneManager()

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ["-region"]
        unique_together = ['name', 'region']