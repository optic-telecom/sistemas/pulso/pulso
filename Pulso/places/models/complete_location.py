from places.models.street_location import StreetLocationManager
from django.db import models
from utils.models import BaseModel
from django.core.validators import MaxValueValidator, MinValueValidator


class CompleteLocationManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related(
            "floor", "street_location"
        )


class CompleteLocation(BaseModel):
    """
    Modelo que contiene la ubicación/número de
    oficina/condominio/departamento.
    """
    floor = models.ForeignKey(
        'Floor',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    number = models.CharField(max_length=20)
    street_location = models.ForeignKey(
        'StreetLocation',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    gps_latitude = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        validators=[
            MinValueValidator(-90),
            MaxValueValidator(90)
        ],
        blank=True,
        null=True
    )
    gtd_longitude = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        validators=[
            MinValueValidator(-180),
            MaxValueValidator(180)
        ],
        blank=True,
        null=True
    )
    objects = CompleteLocationManager()

    def __str__(self):
        if self.floor:
            return f'{self.floor}'
        else:
            tipo_edif = self.street_location.get_edification_type_display()
            return f'{tipo_edif.upper()} {self.street_location}'

    @property
    def country(self):
        return self.street_location.street.commune.region.country.name

    @property
    def country_id(self):
        return self.street_location.street.commune.region.country.pk

    @property
    def region(self):
        return self.street_location.street.commune.region.name

    @property
    def region_id(self):
        return self.street_location.street.commune.region.pk

    @property
    def commune(self):
        return self.street_location.street.commune.name

    @property
    def commune_id(self):
        return self.street_location.street.commune.pk

    @property
    def street(self):
        return f'{self.street_location.street.name}'

    @property
    def street_id(self):
        return self.street_location.street.pk

    @property
    def edification_type(self):
        return self.street_location.get_edification_type_display()

    @property
    def street_location_pk(self):
        return self.street_location.pk

    @property
    def floor_num(self):
        if self.floor:
            return f'Nro. {self.floor.floor_num}'
        else:
            return None

    @property
    def floor_num_id(self):
        if self.floor:
            return self.floor.pk
        else:
            return None
    
    @property
    def street_location_pk(self):
        return self.street_location.pk

    @property
    def departament(self):
        if self.floor:
            apto = self.floor.number.replace(")","")
            piso = self.floor.floor_num
            resp = f"Dpto-{apto} Piso ({piso})"
            return resp
        else:
            return self.street_location.street_no

    @property
    def tower(self):
        if self.floor:
            return f'{self.floor.tower.name}'
        else:
            return None
        # return self.street_location.get_edification_type_display()

    @property
    def tower_id(self):
        if self.floor:
            return self.floor.tower.pk
        else:
            return None
        # return self.street_location.get_edification_type_display()

    class Meta:
        ordering = ["-street_location"]
