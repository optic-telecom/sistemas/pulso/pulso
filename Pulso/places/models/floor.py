from django.db import models
from utils.models import BaseModel


class FloorManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related(
            "tower"
        )


class Floor(BaseModel):
    """ Información de los pisos de una torre. """
    tower = models.ForeignKey('Tower', on_delete=models.CASCADE)
    number = models.CharField(
        max_length=20,
        default="1",
        verbose_name="Apartamento"
    )
    floor_num = models.CharField(
        max_length=20,
        default="1",
        verbose_name='Piso'
    )
    objects = FloorManager()

    def __str__(self):
        tipo_edif = self.tower.street_location.get_edification_type_display()
        return f"""Dpto. {self.number}, {tipo_edif.upper()} {self.tower}"""

    @property
    def get_floor_num(self):
        return f'Dpto. {self.number}'

    class Meta:
        ordering = ["-tower"]
        unique_together = ['tower', 'number']
