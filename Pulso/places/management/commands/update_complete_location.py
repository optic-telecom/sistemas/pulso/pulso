from django.core.management.base import BaseCommand
from django.utils import timezone
from places.models import Region,Commune,Street,StreetLocation, Floor,Tower,CompleteLocation

import requests
import time

class Command(BaseCommand):
    
    help = 'Comando que llena las ubicaciones por comuna.'


    def body_request(self,url):
        """
        Cuerpo génerico de la petición
        """
        request = requests.get(url,timeout=1)
        try :
            data = request.json()
            return data
        except ValueError :
            print ("El contenido de respuesta no es un JSON")

    def add_arguments(self, parser):
        parser.add_argument('region', type=str, help='Indica la nomenclatura usada por movistar para tratar las Regiones de Chile')


    def update_cl(self,region):
        _filtro = {'street__commune__region__pk':region}
        _filtro_ = {'tower__street_location__street__commune__region__pk':region}

        street_l = StreetLocation.objects.filter(**_filtro)
        floors = Floor.objects.filter(**_filtro_)

        print("=== Agregando Direcciones de calle ===")
        for sl in street_l:
            cl = CompleteLocation.objects.filter(street_location=sl,
                                                number=sl.street_no)
            if not cl.exists():
                new = CompleteLocation.objects.create(
                                                    street_location=sl,
                                                    number=sl.street_no)
                print('Se ha generado una nueva direccion')

        print("=== Agregando Pisos de Edificios ===")
        for f in floors:
            cl = CompleteLocation.objects.filter(street_location=f.tower.street_location,
                                                number=f.tower.name,
                                                floor=f)
            if not cl.exists():
                new = CompleteLocation.objects.create(street_location=f.tower.street_location,
                                                    number=f.tower.name,
                                                    floor=sl)
                print('Se ha generado un nuevo Piso de edificio.')

    def handle(self,*args,**kwargs):
        region = kwargs['region']
        self.update_cl(region)