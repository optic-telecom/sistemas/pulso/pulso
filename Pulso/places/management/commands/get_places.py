from django.core.management.base import BaseCommand
from django.utils import timezone
from places.models import Region,Commune,Street,StreetLocation, Floor,Tower,CompleteLocation

import requests
import time

class Command(BaseCommand):

    help = 'Comando que llena las ubicaciones por comuna.'

    def switch_ubicaciones(self,tipo:str)->int:

        switch = {
            'Edificio' : 1,
            'Condominio' : 2,
            'Casa' : 3,
            'Empresa' : 4,
            'Terreno' : 5,

            'Local comercial':6,
            'Local corporativo':7,
            'Sitio eriazo':8,
            'Block':9,
            'Sitio agricola':10,
            'Industria':11,
            'Centro recreativo':12,
            'Edificio corporativo':13,
            'En actualizacion ':14
        }

        return switch.get(tipo,3)

    def body_request(self,url):
        """
        Cuerpo génerico de la petición
        """
        request = requests.get(url,timeout=120)
        try :
            data = request.json()
            return data
        except ValueError :
            print ("El contenido de respuesta no es un JSON")


    def add_arguments(self, parser):
        parser.add_argument('region', type=str, help='Indica la nomenclatura usada por movistar para tratar las Regiones de Chile')

    def get_commune(self,region):
        regions = Region.objects.get(id_region = region)
        # url = f'https://www.movistar.cl/factibilizador-servicios-fijos?p_p_id=colportalfactibilidadtecnica_WAR_colportalfactibilizadortecnicoportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=municipality&p_p_cacheability=cacheLevelPage&p_p_col_id=_118_INSTANCE_6OXelFBTdDA2__column-1&p_p_col_count=1&region={region}'
        url = f'https://catalogo.movistar.cl/servicioshogar/servicios/comunas/?id={region}'
        data = self.body_request(url)
        data = data['datos']['comunaListDTO']['comunaList']
        comunas = Commune.objects.all()

        for dato in data:
            obj = {'region':regions,
                    'name':dato['nombreComuna'],
                    'id_municipality' : dato['idComuna']}
            if not comunas.filter(**obj).exists():
                try:
                    comuna = Commune(**obj)
                    comuna.save()
                    print(f'Nueva comuna en {regions.name}, nombre: {comuna.name}')
                except Exception as e:
                    print(str(e))
                
            else:

                comuna = comunas.get(**obj)

            print(comuna)

    def get_street(self,region):

        print('Inicio de carga de calles')

        filtro = {'region__id_region':region}
        communes = Commune.objects.filter(**filtro)

        filtro = {'commune__region__id_region':region,}
        streets = Street.objects.filter(**filtro)

        letras = ['a','b','c','d','e','f','g','h','i',
                'j','k','l','m','n','o','p','q','r','s',
                't','u','v','w','x','y','z',]

        for commune in communes:
            comuna = commune.name
            for letra in letras:
                # url = f'https://www.movistar.cl/factibilizador-servicios-fijos?p_p_id=colportalfactibilidadtecnica_WAR_colportalfactibilizadortecnicoportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=street&p_p_cacheability=cacheLevelPage&p_p_col_id=_118_INSTANCE_6OXelFBTdDA2__column-1&p_p_col_count=1&municipality={comuna}&street={letra}'
                url = f'https://catalogo.movistar.cl/servicioshogar/servicios/streets?street={letra}&state={comuna}'
                data = self.body_request(url)
                if data:
                    data = data['datos']['calleListDTO']['calleList']
                else:
                    data = list()

                for dato in data:
                    obj = dict(name=dato['descripcion'], 
                            commune=commune,
                            id_street=dato['codigo'])
                    try:

                        if not streets.filter(**obj).exists():
                            street = Street(**obj)
                            street.save()
                            print(f'nueva calle {street}')

                    except Exception as e:
                        print(str(e))

    def get_street_location(self,region):
        print('Street Location V2')
        filtroX = {'region__id_region':region}
        id = 'street'
        communes = Commune.objects.filter(**filtroX).order_by('-name')
        filtro = {'commune__region__id_region__exact':region,
                'commune__in':communes,}
        streets = Street.objects.filter(**filtro).order_by('-commune')


        streetLocation = StreetLocation.objects.all().order_by(id)

        for street in streets.iterator():
            streetNumber = street.id_street 
            commune = street.commune.id_municipality 
            # url = f'https://www.movistar.cl/factibilizador-servicios-fijos?p_p_id=colportalfactibilidadtecnica_WAR_colportalfactibilizadortecnicoportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=streetNumber&p_p_cacheability=cacheLevelPage&p_p_col_id=_118_INSTANCE_6OXelFBTdDA2__column-1&p_p_col_count=1&streetNumber={streetNumber}&municipality={commune}'
            url = f'https://catalogo.movistar.cl/servicioshogar/servicios/address/index/idCalle/{streetNumber}/idComuna/{commune}'
            data = self.body_request(url)

            

            if data:
                data = data['datos']['alturaListDTO']['alturaList']
            else:
                data = list()


            for dato in data:
                # print(dato['edificationType'].capitalize())
                try:
                    obj = dict(street=street, 
                                street_no = dato['altura'],
                                id_address=dato['idDireccion'],
                                id_address_full=dato['idDireccion24Caracteres'])
                    # validamos la no existencia para su posterior salvado
                    if not streetLocation.filter(**obj).exists():
                        # Agregamos el tipo de edificación
                        obj['edification_type'] = self.switch_ubicaciones(dato['tipoEdificacion'].capitalize())
                        street_loc_new = StreetLocation(**obj)
                        # guardamos el nuevo objeto
                        street_loc_new.save()
                        print(street_loc_new)
                    else:
                        street_loc_old = StreetLocation.objects.get(id_address=dato['idDireccion'])
                        print(f'La ubicación {street_loc_old}, ya existe en la bd de pulso.')


                except Exception as e:
                    print(str(e))
                
                # time.sleep(1)

    def get_tower_and_floor(self,region):
        print('Inicia el registro de nuevas torres ')

        filtro = {
            'edification_type__in':[1,9,13],
            'street__commune__region__id_region__exact':region,
            
        }
        id = '-street_location'

        street_with_edif = StreetLocation.objects.filter(**filtro)
        street_with_edif = street_with_edif.order_by('-street')
        print(street_with_edif.count())

        towers = Tower.objects.all().order_by(id)
        floors = Floor.objects.all().order_by(id)

            
        for street in street_with_edif:
            idAddress = street.id_address
            obj = dict(name=street.street_no,street_location=street)
            # validamos que no exista la torre para agregarla
            if not towers.filter(**obj).exists():
                # Acá salvamos la torre
                tower_new = Tower(name=street.street_no,street_location=street)
                tower_new.save()
                print(f'nueva torre {tower_new}')
                # url = f'https://www.movistar.cl/factibilizador-servicios-fijos?p_p_id=colportalfactibilidadtecnica_WAR_colportalfactibilizadortecnicoportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=apartment&p_p_cacheability=cacheLevelPage&p_p_col_id=_118_INSTANCE_6OXelFBTdDA2__column-1&p_p_col_count=1&idAddress={idAddress}'
                url = f'https://catalogo.movistar.cl/servicioshogar/servicios/dependencias/?idDireccion={idAddress}'
                data = self.body_request(url)
                

                try:
                    if data:
                        data = data['datos']['dependenciaListDTO'][0]['dependenciaList']
                    else:
                        data = list()
                except KeyError:
                    print('json erroneo')

                
                for dato in data:
                    try:
                        obj_f = dict(floor_num=dato['piso'],
                                    number=dato['numero'],
                                    tower=tower_new)
                        if not floors.filter(**obj_f).exists():
                            piso = Floor(**obj_f)
                            piso.save()
                            print(f'{piso.tower.street_location.get_edification_type_display()} {piso.tower.name}, piso {piso.floor_num}, departamento {piso.number}')
                        else:
                            piso = Floor.objects.get(**obj_f)
                            print(f'Este piso {piso} existe en nuestra bd') 

                    except Exception as e:
                        print(str(e))
            
            time.sleep(1)

    def get_address_full_by_region(self,region):
        print('Inicia la carga de direcciones completas...')
        filtro = {'street__commune__region__id_region__exact':region,
                }
        
        street_location = StreetLocation.objects.filter(**filtro).order_by('-street_no')
        street_without_edif = street_location.exclude(edification_type__in=[1,9,13]) 
        complete_locations = CompleteLocation.objects.all()
        
        for street in street_without_edif.iterator():
            obj = dict(number=street.street_no,street_location=street)
            if not complete_locations.filter(**obj).exists():
                cl = CompleteLocation(**obj)
                cl.save()
                print(f'Nueva esta dirección en pulso {cl}')
            else:
                cl = complete_locations.get(**obj)
                print(f'Existe esta dirección en pulso {cl}')

        
        street_with_edif = street_location.filter(edification_type__in=[1,9,13])

        filtro = {'tower__street_location__street__commune__region__id_region__exact':region,
                }
        pisos = Floor.objects.filter(**filtro).order_by("-tower")

        for piso in pisos.iterator():
            obj2 = dict(
                floor=piso,
                number=piso.tower.street_location.street_no,
                street_location=piso.tower.street_location
            )
            if not CompleteLocation.objects.filter(**obj2).exists():
                cl = CompleteLocation(**obj2)
                cl = cl.save()
                print(f'Nueva esta dirección en pulso {cl}')
            else:
                cl = CompleteLocation.objects.get(**obj2)
                print(f'Existe esta dirección en pulso {cl}')
   

    def handle(self,*args,**kwargs):

        region = kwargs['region']
        r = Region.objects.get(id_region=region)
        name = r.name.lower().replace(' ','_')

        self.get_commune(region)
        self.get_street(region)
        self.get_street_location(region)
        self.get_tower_and_floor(region)
        self.get_address_full_by_region(region)

        with open(f'region_{name}.txt','w+') as reg:
            reg.write(f'{name} lista!!!')

