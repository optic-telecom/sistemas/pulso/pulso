from django.core.management.base import BaseCommand
from places.models import StreetLocation
from django.db.models import Q


class Command(BaseCommand):
    help = ("Este comando busca todos los street location,\
            street_no que inicien con 0. ó 0, y los reemplaza")

    def handle(self, *args, **kwargs):
        queryset = StreetLocation.objects.filter(
            Q(street_no__istartswith="0.") |
            Q(street_no__istartswith="0,")
        )

        for qs in queryset:
            print(f"Anterior valor {qs.street_no}")
            qs.street_no_format
            print(f"Nuevo valor {qs.street_no}")

