from django.core.management.base import BaseCommand
from geopy.geocoders import Nominatim
from places.models import StreetLocation


class Command(BaseCommand):
    help = 'Comando que llena las coordenadas por región'

    def handle(self, *args, **kwargs):
        geolocator = Nominatim(user_agent="geo")
        street_locations = StreetLocation.objects.filter(
            street__commune__region__name__icontains="Arica"
        ).order_by("street")
        x = 0
        for sl in street_locations:
            print(
                f"{sl.street_no} {sl.street.name} {sl.street.commune.name}\
                        {sl.street.commune.region.name}\
                            {sl.street.commune.region.country.name}"
            )
            try:
                location = geolocator.geocode(
                    f"{sl.street_no} {sl.street.name} {sl.street.commune.name}\
                        {sl.street.commune.region.name}\
                            {sl.street.commune.region.country.name}"
                    )
                sl.gps_latitude = location.latitude
                sl.gtd_longitude = location.longitude
                sl.save()
                x += 1
                print(f"Se han salvado hasta ahora {x} registros con coordenadas")
            except Exception as e:
                print(str(e))