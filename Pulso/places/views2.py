from dynamic_preferences.registries import global_preferences_registry

from rest_framework import status
from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.generics import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import action

from utils.views import send_webhook_transaction
from utils.permissions import IsSystemInternalPermission

from .serializers import CountrySerializer, RegionSerializer,\
    CommuneSerializer, StreetSerializer, StreetLocationSerializer,\
    TowerSerializer, FloorSerializer, CompleteLocationSerializer,\
    CompleteLocationSz

from .models import Country, Region, Commune, Street, StreetLocation,\
    Tower, Floor, CompleteLocation

from models.models import Node

from django.db.models import Q
from models.models import Operator
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
from django.utils.decorators import method_decorator

__all__ = ['Country', 'Region', 'Commune', 'Street', 'StreetLocation',
    'Tower', 'Floor', 'CompleteLocation']

global_preferences = global_preferences_registry.manager()

@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class CountryViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Country
    retrieve:
        End point for Country
    update:
        End point for Country
    delete:
        End point for Country
    """
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='country', action=1, data=serializer.data)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    def destroy(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        instande_id = instance.id
        self.perform_destroy(instance)
        if not replicate: # Si tiene replicate ya se replicó
            data = {
                'id': str(instande_id)
            }
            send_webhook_transaction(model='country', action=3, data=data)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        partial = kwargs.pop('partial', False)
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='country', action=2, data=serializer.data)

        return Response(serializer.data)

@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class RegionViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Region
    retrieve:
        End point for Region
    update:
        End point for Region
    delete:
        End point for Region
    """
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)


    def create(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='region', action=1, data=serializer.data)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    
    def destroy(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        instande_id = instance.id
        self.perform_destroy(instance)
        if not replicate: # Si tiene replicate ya se replicó
            data = {
                'id': str(instande_id)
            }
            send_webhook_transaction(model='region', action=3, data=data)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        partial = kwargs.pop('partial', False)
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='region', action=2, data=serializer.data)

        return Response(serializer.data)


@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class CommuneViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Commune
    retrieve:
        End point for Commune
    update:
        End point for Commune
    delete:
        End point for Commune
    """
    queryset = Commune.objects.all()
    serializer_class = CommuneSerializer
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='commune', action=1, data=serializer.data)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    
    def destroy(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        instande_id = instance.id
        self.perform_destroy(instance)
        if not replicate: # Si tiene replicate ya se replicó
            data = {
                'id': str(instande_id)
            }
            send_webhook_transaction(model='commune', action=3, data=data)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        partial = kwargs.pop('partial', False)
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='commune', action=2, data=serializer.data)

        return Response(serializer.data)


@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class StreetViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Street
    retrieve:
        End point for Street
    update:
        End point for Street
    delete:
        End point for Street
    """
    queryset = Street.objects.all()
    serializer_class = StreetSerializer
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='street', action=1, data=serializer.data)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    
    def destroy(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        instande_id = instance.id
        self.perform_destroy(instance)
        if not replicate: # Si tiene replicate ya se replicó
            data = {
                'id': str(instande_id)
            }
            send_webhook_transaction(model='street', action=3, data=data)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        partial = kwargs.pop('partial', False)
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='street', action=2, data=serializer.data)

        return Response(serializer.data)


@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class StreetLocationViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for StreetLocation
    retrieve:
        End point for StreetLocation
    update:
        End point for StreetLocation
    delete:
        End point for StreetLocation
    """
    queryset = StreetLocation.objects.all()
    serializer_class = StreetLocationSerializer
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='streetlocation', action=1, data=serializer.data)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    
    def destroy(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        instande_id = instance.id
        self.perform_destroy(instance)
        if not replicate: # Si tiene replicate ya se replicó
            data = {
                'id': str(instande_id)
            }
            send_webhook_transaction(model='streetlocation', action=3, data=data)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        partial = kwargs.pop('partial', False)
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='streetlocation', action=2, data=serializer.data)

        return Response(serializer.data)

@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class TowerViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Tower
    retrieve:
        End point for Tower
    update:
        End point for Tower
    delete:
        End point for Tower
    """
    queryset = Tower.objects.all()
    serializer_class = TowerSerializer
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='tower', action=1, data=serializer.data)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    
    def destroy(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        instande_id = instance.id
        self.perform_destroy(instance)
        if not replicate: # Si tiene replicate ya se replicó
            data = {
                'id': str(instande_id)
            }
            send_webhook_transaction(model='tower', action=3, data=data)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        partial = kwargs.pop('partial', False)
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='tower', action=2, data=serializer.data)

        return Response(serializer.data)


@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class FloorViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Floor
    retrieve:
        End point for Floor
    update:
        End point for Floor
    delete:
        End point for Floor
    """
    queryset = Floor.objects.all()
    serializer_class = FloorSerializer
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='floor', action=1, data=serializer.data)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    
    def destroy(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        instande_id = instance.id
        self.perform_destroy(instance)
        if not replicate: # Si tiene replicate ya se replicó
            data = {
                'id': str(instande_id)
            }
            send_webhook_transaction(model='floor', action=3, data=data)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        partial = kwargs.pop('partial', False)
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='floor', action=2, data=serializer.data)

        return Response(serializer.data)


@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class CompleteLocationViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for CompleteLocation
    retrieve:
        End point for CompleteLocation
    update:
        End point for CompleteLocation
    delete:
        End point for CompleteLocation
    """
    queryset = CompleteLocation.objects.all()
    serializer_class = CompleteLocationSerializer
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='completelocation', action=1, data=serializer.data)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    
    def destroy(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        instande_id = instance.id
        self.perform_destroy(instance)
        if not replicate: # Si tiene replicate ya se replicó
            data = {
                'id': str(instande_id)
            }
            send_webhook_transaction(model='completelocation', action=3, data=data)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        partial = kwargs.pop('partial', False)
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='completelocation', action=2, data=serializer.data)

        return Response(serializer.data)



@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class CompleteLocationRead(viewsets.ReadOnlyModelViewSet):
    queryset = CompleteLocation.objects.order_by('id')
    serializer_class = CompleteLocationSz
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        try:
            return self.retrieve(request, *args, **kwargs)
        except Exception as e:
            print(str(e))
            return super().list(request, *args, **kwargs)

class SearchCompleteLocationRead(viewsets.ReadOnlyModelViewSet):
    queryset = CompleteLocation.objects.order_by('id')
    serializer_class = CompleteLocationSz
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)

    def get_queryset(self):
        qs = super().get_queryset()
        
        country = self.request.query_params.get('country',None)
        region = self.request.query_params.get('region',None)
        commune = self.request.query_params.get('commune',None)
        street = self.request.query_params.get('street',None)
        street_location = self.request.query_params.get('street_location',None)
        house_number = self.request.query_params.get('house_number',None)


        if country:
            filtro = {'street_location__street__commune__region__country__name__istartswith':country}
            qs = qs.filter(**filtro)
        
        if region:
            filtro = {'street_location__street__commune__region__name__icontains':region}
            qs = qs.filter(**filtro)

        if commune:
            filtro = {'street_location__street__commune__name__icontains':commune}
            qs = qs.filter(**filtro)

        if street:
            filtro = {'street_location__street__name__icontains':street}
            qs = qs.filter(**filtro)
        
        if street_location:
            filtro = {'street_location__street_no__icontains':street_location}
            qs = qs.filter(**filtro)

        if house_number:
            filtro = {'number__icontains':house_number}
            qs = qs.filter(**filtro)

        return qs


# BUSQUEDA ESCALONADA
@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class SearchManagerViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Clase encargada de manejar los filtros a través de un lookup
    apoyada por un parametro get que varia según la clase que hereda 
    """
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)
    param = None
    lookup = 'name__icontains'
    filter_by = None


    def get_queryset(self):
        qs = super().get_queryset()
        if self.filter_by:
            arg = self.kwargs.get(self.filter_by,None)
            filtro = {f'{self.filter_by}__id':arg}
            qs = qs.filter(**filtro)
        return qs
            
    def customFilter(self,qs):
        argumento = self.request.GET.get(self.param.lower(),None)
        if argumento:
            _list_word = argumento.split()
            query = Q()
            for word in _list_word:
                query = query & Q(**{self.lookup:word})
                            
            qs = qs.filter(query)
        return qs

    def list(self,request,*args,**kwargs):
        qs = self.customFilter(self.get_queryset())
        serializer = self.serializer_class(qs,many=True)
        return Response(serializer.data)

@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class SearchRegionViewSet(SearchManagerViewSet):
    """ Regiones para Factibilizador """
    serializer_class = RegionSerializer
    queryset = Region.objects.order_by('name')
    param = 'region'
    lookup = 'name__icontains'


@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class SearchCommuneViewSet(SearchManagerViewSet):
    """ Comunas para Factibilizador """
    serializer_class = CommuneSerializer
    queryset = Commune.objects.order_by('name')
    param = 'commune'
    filter_by = 'region'

@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class SearchStreetViewSet(SearchManagerViewSet):
    """ Calles para Factibilizador """
    queryset = Street.objects.order_by('name')
    serializer_class = StreetSerializer
    param = 'street'
    filter_by = 'commune'

@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class SearchStreetLocationViewSet(SearchManagerViewSet):
    """ Ubicaciones por calle para Factibilizador """
    queryset = StreetLocation.objects.order_by('id')
    serializer_class = StreetLocationSerializer
    param = 'streetlocation'
    lookup = 'street_no__istartswith'
    filter_by = 'street'

@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class SearchCompleteLocationViewSet(SearchManagerViewSet):
    """ Direcciones completas para Factibilizador """
    serializer_class = CompleteLocationSz

    def customFilter(self,qs):
        argumento = self.request.GET.get('floor_num',None)
        if argumento:
            _list_word = argumento.split()
            query = Q()
            for word in _list_word:
                query = query & Q(floor__number__istartswith=word) | Q(number__istartswith=word)
                            
            qs = qs.filter(query)


        return qs

    def get_queryset(self):
        filtro = dict(street_location__id=self.kwargs.get('streetlocation',None))
        qs = (CompleteLocation.objects
                            .filter(**filtro).order_by('floor__floor_num',
                                                        'floor__number',
                                                        'id',
                                                        ))
        return qs


# =============

from dal import autocomplete 

class NodeAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):

        qs = Node.objects.all()
        if self.q:
            qs = Node.objects.filter(code__iexact=self.q)
        
        return qs

class RegionAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):

        qs = Region.objects.none()
        if self.q:
            qs = Region.objects.filter(name__icontains=self.q)
        
        return qs

class CommuneAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):

        qs = Commune.objects.none()
        region = self.forwarded.get('region', None)
        
        if region:
            qs = Commune.objects.filter(region=region)

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        
        return qs

class StreetAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Street.objects.none()
        region = self.forwarded.get('region',None)
        commune = self.forwarded.get('commune', None)
        if commune and region:
            qs = Street.objects.filter(commune__region=region, commune=commune)

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        
        return qs

class StreetLocationAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):

        qs = StreetLocation.objects.none()
        region = self.forwarded.get('region',None)
        commune = self.forwarded.get('commune', None)
        street = self.forwarded.get('street', None)

        if street and commune and region:
            qs = StreetLocation.objects.filter(street=street,
                                                street__commune=commune,
                                                street__commune__region=region)

        if self.q:
            qs = qs.filter(street_no__icontains=self.q)
        
        return qs

class TowerAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Tower.objects.none()
        street_location = self.forwarded.get('street_location', None)
        
        if street_location:
            qs = Tower.objects.filter(street_location=street_location)
        if self.q:
            qs = qs.filter(name__icontains=self.q)
        
        return qs

class FloorAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Floor.objects.none()
        tower = self.forwarded.get('tower', None)
        if tower:
            qs = Floor.objects.filter(tower=tower)

        if self.q:
            qs = qs.filter(number__icontains=self.q)
        
        return qs

    def get_result_label(self, item):
        return item.get_floor_num

    def get_selected_result_label(self, item):
        return item.get_floor_num

class CompleteLocationAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):

        qs = CompleteLocation.objects.none()
        street_location = self.forwarded.get('street_location', None)

        if street_location:
            qs = CompleteLocation.objects.filter(street_location=street_location)

        if self.q:
            qs = (qs.filter(Q(floor__number__icontains=self.q)|
                            Q(street_location__street_no__icontains=self.q))
                    .order_by('floor__floor_num',
                            'floor__number',
                            'id',))

        return qs

    def get_result_label(self, item):
        if item.floor:
            return f"{item.departament}"
        else:
            return f"{item.street_location.get_edification_type_display().upper()} NRO. {item.number}"


    def get_selected_result_label(self, item):
        if item.floor:
            return f"{item.departament}"
        else:
            return f"{item.street_location.get_edification_type_display().upper()} NRO. {item.number}"

class OperatorAutocomplete(autocomplete.Select2QuerySetView):
    
    def get_queryset(self):
        
        qs = Operator.objects.all()

        if self.q:
            qs = (Operator.objects.filter(name__istartswith=self.q))

        return qs