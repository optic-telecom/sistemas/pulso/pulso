from places.models import StreetLocation
from dal import autocomplete


class StreetLocationAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):

        qs = StreetLocation.objects.none()
        region = self.forwarded.get('region', None)
        commune = self.forwarded.get('commune', None)
        street = self.forwarded.get('street', None)

        if street and commune and region:
            qs = StreetLocation.objects.filter(
                                            street=street,
                                            street__commune=commune,
                                            street__commune__region=region
                                        )

        if self.q:
            qs = qs.filter(street_no__icontains=self.q)
        return qs
