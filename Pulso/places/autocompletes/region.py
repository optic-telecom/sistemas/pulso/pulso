from places.models import Region
from dal import autocomplete 


class RegionAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):

        qs = Region.objects.none()
        if self.q:
            qs = Region.objects.filter(name__icontains=self.q)
        return qs
