from models.models import Operator
from dal import autocomplete


class OperatorAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Operator.objects.all()
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs
