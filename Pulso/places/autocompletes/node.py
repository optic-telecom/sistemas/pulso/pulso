from models.models import Node
from dal import autocomplete 


class NodeAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Node.objects.all()
        if self.q:
            qs = Node.objects.filter(code__iexact=self.q)
        return qs
