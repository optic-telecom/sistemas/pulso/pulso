from django.db.models import Q
from places.models import CompleteLocation
from dal import autocomplete


class CompleteLocationAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):

        qs = CompleteLocation.objects.none()
        street_location = self.forwarded.get('street_location', None)

        if street_location:
            qs = CompleteLocation.objects.filter(
                street_location=street_location
                )

        if self.q:
            qs = (qs.filter(
                        Q(floor__number__icontains=self.q) |
                        Q(street_location__street_no__icontains=self.q)
                    ).order_by(
                        'floor__floor_num',
                        'floor__number',
                        'id',)
                    )

        return qs

    def get_result_label(self, item):
        if item.floor:
            return f"{item.departament}"
        else:
            tipo_ed = item.street_location.get_edification_type_display()
            return f"{tipo_ed.upper()} NRO. {item.number}"

    def get_selected_result_label(self, item):
        return self.get_result_label(item)
