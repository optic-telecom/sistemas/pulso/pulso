from places.models import Floor
from dal import autocomplete


class FloorAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Floor.objects.none()
        tower = self.forwarded.get('tower', None)
        if tower:
            qs = Floor.objects.filter(tower=tower)

        if self.q:
            qs = qs.filter(number__icontains=self.q)

        return qs

    def get_result_label(self, item):
        return item.get_floor_num

    def get_selected_result_label(self, item):
        return item.get_floor_num
