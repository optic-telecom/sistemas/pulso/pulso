from places.models import Commune
from dal import autocomplete


class CommuneAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):

        qs = Commune.objects.none()
        region = self.forwarded.get('region', None)
        if region:
            qs = Commune.objects.filter(region=region)

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs

