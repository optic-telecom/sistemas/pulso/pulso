from places.models import Tower
from dal import autocomplete


class TowerAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Tower.objects.none()
        street_location = self.forwarded.get('street_location', None)
        if street_location:
            qs = Tower.objects.filter(street_location=street_location)
        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs
