from places.models import Street
from dal import autocomplete


class StreetAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Street.objects.none()
        region = self.forwarded.get('region', None)
        commune = self.forwarded.get('commune', None)
        if commune and region:
            qs = Street.objects.filter(
                commune__region=region,
                commune=commune
                )

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs
