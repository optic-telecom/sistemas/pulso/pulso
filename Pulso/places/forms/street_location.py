from dal import autocomplete
from django import forms
from places.forms import StreetForm
from places.models import (
    Commune,
    StreetLocation
)

from places.models.street_location import EDIFICATION_TYPE_CHOICES


class StreetLocationForm(StreetForm):
    edification_type = forms.ChoiceField(
            choices=EDIFICATION_TYPE_CHOICES,
            widget=forms.Select(attrs={
                'class': 'select2',
                "placeholder": "Tipo de Edificio"
                })
        )

    commune = forms.ModelChoiceField(
        queryset=Commune.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='commune-ac',
            forward=['region'],
            attrs={
                'data-placeholder': 'Comuna',
                'style': 'width:100%',
                'class': 'form-control'
                },
        ))

    class Meta:
        model = StreetLocation
        fields = (
            'id', 'street_no', 'street', 'commune',
            'region', 'edification_type',
            'homepassed', 'gps_latitude', 'gtd_longitude'
            )

        widgets = {
            'street': autocomplete.ModelSelect2(
                url='street-ac',
                forward=['region', 'commune'],
                attrs={
                    'data-placeholder': 'Calle',
                    'style': 'width:100%',
                    'class': 'form-control'},
            )

        }
