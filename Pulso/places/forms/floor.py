from dal import autocomplete
from django import forms
from places.forms import TowerForm
from places.models import (
    StreetLocation,
    Floor
)


class FloorForm(TowerForm):
    street_location = forms.ModelChoiceField(
        queryset=StreetLocation.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='streetlocation-ac',
            forward=['street'],
            attrs={
                'data-placeholder': 'Ubicación de Calle',
                'style': 'width:100%',
                'class': 'form-control'
                },
        ))

    class Meta:
        model = Floor
        fields = (
            'tower', 'number', 'floor_num',
            'region', 'commune', 'street',
            'street_location',
            )
        widgets = {
            'tower': autocomplete.ModelSelect2(
                        url='tower-ac',
                        forward=['street_location'],
                        attrs={
                            'data-placeholder': 'Torres',
                            'style': 'width:100%',
                            'class': 'form-control'
                            }),
        }
