from dal import autocomplete
from django import forms
from places.forms import StreetForm
from places.models import (
    Commune,
    Street,
    Tower,
)


class TowerForm(StreetForm):
    commune = forms.ModelChoiceField(
        queryset=Commune.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='commune-ac',
            forward=['region'],
            attrs={
                'data-placeholder': 'Comuna',
                'style': 'width:100%',
                'class': 'form-control'
                },
        ))

    street = forms.ModelChoiceField(
        queryset=Street.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='street-ac',
            forward=['commune'],
            attrs={
                'data-placeholder': 'Calle',
                'style': 'width:100%',
                'class': 'form-control'
                },
        ))

    class Meta:
        model = Tower
        fields = ('name', 'region', 'commune', 'street', 'street_location')
        widgets = {
            'street_location': autocomplete.ModelSelect2(
                                url='streetlocation-ac',
                                forward=['street'],
                                attrs={
                                    'data-placeholder': 'Ubicación de calle',
                                    'style': 'width:100%',
                                    'class': 'form-control'
                                    }),
            }
