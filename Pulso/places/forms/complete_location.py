from dal import autocomplete
from django import forms
from places.forms import StreetLocationForm
from places.models import (
    Commune,
    Street,
    Tower,
    CompleteLocation
)


class CompleteLocationForm(StreetLocationForm):

    commune = forms.ModelChoiceField(
        queryset=Commune.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='commune-ac',
            forward=['region'],
            attrs={
                'data-placeholder': 'Comuna',
                'style': 'width:100%',
                'class': 'form-control'
                },
        ))

    street = forms.ModelChoiceField(
        queryset=Street.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='street-ac',
            forward=['region', 'commune'],
            attrs={
                'data-placeholder': 'Calle',
                'style': 'width:100%',
                'class': 'form-control'
                },
        ))

    tower = forms.ModelChoiceField(
        queryset=Tower.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='tower-ac',
            forward=['street_location'],
            attrs={
                'data-placeholder': 'Torre Nro',
                'style': 'width:100%',
                'class': 'form-control'
                },
        ))

    class Meta:
        model = CompleteLocation
        fields = (
            'number', 'region', 'commune', 'street',
            'street_location', 'tower', 'floor'
            )

        widgets = {
            'street_location': autocomplete.ModelSelect2(
                                url='streetlocation-ac',
                                forward=['region', 'commune', 'street'],
                                attrs={
                                    'data-placeholder': 'Ubicación de calle',
                                    'style': 'width:100%',
                                    'class': 'form-control'
                                    }),
            'floor': autocomplete.ModelSelect2(
                                url='floor-ac',
                                forward=['tower'],
                                attrs={
                                    'data-placeholder': 'Piso de Apto.',
                                    'style': 'width:100%',
                                    'class': 'form-control'
                                    }),
        }