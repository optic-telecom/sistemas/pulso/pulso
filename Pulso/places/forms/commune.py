from dal import autocomplete
from django import forms
from places.models import (Commune)


class CommuneForm(forms.ModelForm):
    class Meta:
        model = Commune
        fields = ('id', 'name', 'region')
        widgets = {
            'region': autocomplete.ModelSelect2(
                url='region-ac',
                attrs={
                    'data-placeholder': 'Región',
                    'style': 'width:100%',
                    'class': 'form-control'
                    },
            )
        }

    class Media:
        css = {
            'all': []
        }

        js = ['/static/admin/js/vendor/jquery/jquery.js', ]
