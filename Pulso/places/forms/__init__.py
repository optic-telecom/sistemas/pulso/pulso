from places.serializers.complete_location_sz import CompleteLocationSz
from .commune import CommuneForm
from .street import StreetForm
from .street_location import StreetLocationForm
from .tower import TowerForm
from .floor import FloorForm
from .complete_location import CompleteLocationForm

__all__ = [
    "CommuneForm",
    "StreetForm",
    "StreetLocationForm",
    "TowerForm",
    "FloorForm",
    "CompleteLocationForm",
    ]