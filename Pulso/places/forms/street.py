from dal import autocomplete
from django import forms
from places.forms import CommuneForm
from places.models import (
    Region,
    Street,

)


class StreetForm(CommuneForm):
    region = forms.ModelChoiceField(
        queryset=Region.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='region-ac',
            attrs={
                'data-placeholder': 'Región',
                'style': 'width:100%',
                'class': 'form-control'
                },
        ))

    class Meta:
        model = Street
        fields = ('id', 'name', 'region', 'commune')
        widgets = {
            'commune': autocomplete.ModelSelect2(
                url='commune-ac',
                forward=['region'],
                attrs={
                    'data-placeholder': 'Comuna',
                    'style': 'width:100%',
                    'class': 'form-control'
                    },
            )
        }
