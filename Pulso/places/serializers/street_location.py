from rest_framework import serializers
from places.models import (StreetLocation)


class StreetLocationSerializer(serializers.ModelSerializer):
    """ Serialiazer de las ubicaciones de calles. """
    class Meta:
        model = StreetLocation
        fields = (
            'id', 'street_no', 'street', 'edification_type',
            'homepassed', 'gps_latitude', 'gtd_longitude'
            )

    def is_valid(self, *args, **kwargs):
        """ Valida que el par (latitud,longitud) sea único. """
        if not self.instance:
            gps_latitude = self.initial_data.get('gps_latitude')
            gtd_longitude = self.initial_data.get('gtd_longitude')
        else:
            lat = self.instance.gps_latitude
            long = self.instance.gtd_longitude
            gps_latitude = self.initial_data.get('gps_latitude', lat)
            gtd_longitude = self.initial_data.get('gtd_longitude', long)
        try:
            street_location = StreetLocation.objects.get(
                gps_latitude=gps_latitude, gtd_longitude=gtd_longitude)
            if self.instance and street_location.id != self.instance.id:
                msg = 'Esas coordenadas de latitud y longitud ya existen.'
                _err = {'error': msg}
                raise serializers.ValidationError(_err)
            elif not self.instance:
                msg = 'Esas coordenadas de latitud y longitud ya existen.'
                _err = {'error': msg}
                raise serializers.ValidationError(_err)
        except StreetLocation.DoesNotExist:
            pass
        return super(StreetLocationSerializer, self).is_valid(*args, **kwargs)

