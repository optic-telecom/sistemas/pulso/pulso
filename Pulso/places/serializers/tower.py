from rest_framework import serializers
from places.models import (Tower)
from places.models.street_location import (
    TYPE_BUILDING,
    TYPE_COMPANY)


class TowerSerializer(serializers.ModelSerializer):
    """ Serialiazer de las torres de edificios y empresas. """
    class Meta:
        model = Tower
        fields = ('id', 'name', 'street_location')

    def is_valid(self, *args, **kwargs):
        """ Valida que el nombre no se haya creado ya para una ubicación de una calle. """
        if not self.instance:
            name = self.initial_data.get('name')
            street_location = self.initial_data.get('street_location')
        else:
            name = self.initial_data.get('name', self.instance.name)
            street_location = self.initial_data.get(
                'street_location',
                self.instance.street_location
                )
        try:
            tower = Tower.objects.get(
                name=name, street_location=street_location)
            if self.instance and tower.id != self.instance.id:
                _err = {
                    'error': 'Esa torre ya existe para esa ubicación de calle.'
                    }
                raise serializers.ValidationError(_err)
            elif not self.instance:
                _err = {
                    'error': 'Esa torre ya existe para esa ubicación de calle.'
                    }
                raise serializers.ValidationError(_err)
        except Tower.DoesNotExist:
            pass
        return super(TowerSerializer, self).is_valid(*args, **kwargs)

    def validate(self, attrs):
        """
        Valida que edification_type de street_location
        es edificio o empresa.
        """
        listado = [TYPE_BUILDING, TYPE_COMPANY]
        if attrs['street_location'].edification_type not in listado:
            _err = {
                'error': """La ubicación de calle no coincide con un edificio o una empresa,
                            por lo que no puede tener una torre asociada."""
                }
            raise serializers.ValidationError(_err)
        return super().validate(attrs)

