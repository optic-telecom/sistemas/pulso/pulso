from rest_framework import serializers
from places.models import (Floor)


class FloorSerializer(serializers.ModelSerializer):
    """ Serialiazer de los pisos de una torre. """
    class Meta:
        model = Floor
        fields = ('id', 'tower', 'number')

    def is_valid(self, *args, **kwargs):
        """
        Valida que el número del piso no se
        haya creado ya para una torre.
        """
        if not self.instance:
            tower = self.initial_data.get('tower')
            number = self.initial_data.get('number')
        else:
            tower = self.initial_data.get('tower', self.instance.tower)
            number = self.initial_data.get('number', self.instance.number)
        try:
            floor = Floor.objects.get(
                tower=tower, number=number)
            if self.instance and floor.id != self.instance.id:
                _err = {'error': 'Ese piso ya está registrado para esa torre.'}
                raise serializers.ValidationError(_err)
            elif not self.instance:
                _err = {'error': 'Ese piso ya está registrado para esa torre.'}
                raise serializers.ValidationError(_err)
        except Floor.DoesNotExist:
            pass
        return super(FloorSerializer, self).is_valid(*args, **kwargs)

