from rest_framework import serializers
from places.models import (Country)
from places.models.country import CODE_REGEX
import re


class CountrySerializer(serializers.ModelSerializer):
    """ Serialiazer de los países. """
    class Meta:
        model = Country
        fields = ('id', 'name', 'code')

    def is_valid(self, *args, **kwargs):
        """ Valida que el nombre o código no se haya creado ya. """
        if not self.instance:
            name = self.initial_data.get('name')
            code = self.initial_data.get('code')
        else:
            name = self.initial_data.get('name', self.instance.name)
            code = self.initial_data.get('code', self.instance.code)
        try:
            country = Country.objects.get(
                name=name)
            if self.instance and country.id != self.instance.id:
                _err = {'error': 'Ese país ya existe en la base de datos.'}
                raise serializers.ValidationError(_err)
            elif not self.instance:
                _err = {'error': 'Ese país ya existe en la base de datos.'}
                raise serializers.ValidationError(_err)
        except Country.DoesNotExist:
            pass
        # Veamos que el código haga match con el regex
        rgx = re.search(CODE_REGEX, code)
        if len(code) > 3 or len(code) < 2 or not bool(rgx):
            _err = {'error': 'El código debe ser letras en mayúsculas.'}
            raise serializers.ValidationError(_err)
        try:
            country = Country.objects.get(
                code=code)
            if self.instance and country.id != self.instance.id:
                _err = {'error': 'Ese código ya existe para otro país.'}
                raise serializers.ValidationError(_err)
            elif not self.instance:
                _err = {'error': 'Ese código ya existe para otro país.'}
                raise serializers.ValidationError(_err)
        except Country.DoesNotExist:
            pass
        return super(CountrySerializer, self).is_valid(*args, **kwargs)

