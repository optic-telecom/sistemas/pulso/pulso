from rest_framework import serializers
from places.models import Street


class StreetSerializer(serializers.ModelSerializer):
    """ Serialiazer de las calles. """
    class Meta:
        model = Street
        fields = ('id', 'name', 'commune')

    def is_valid(self, *args, **kwargs):
        """ Valida que el nombre no se haya creado ya para una comuna. """
        if not self.instance:
            name = self.initial_data.get('name')
            commune = self.initial_data.get('commune')
        else:
            name = self.initial_data.get('name', self.instance.name)
            commune = self.initial_data.get('commune', self.instance.commune)
        try:
            street = Street.objects.get(
                name=name, commune=commune)
            if self.instance and street.id != self.instance.id:
                _err = {'error': 'Esa calle ya existe para esa comuna.'}
                raise serializers.ValidationError(_err)
            elif not self.instance:
                _err = {'error': 'Esa calle ya existe para esa comuna.'}
                raise serializers.ValidationError(_err)
        except Street.DoesNotExist:
            pass
        return super(StreetSerializer, self).is_valid(*args, **kwargs)
