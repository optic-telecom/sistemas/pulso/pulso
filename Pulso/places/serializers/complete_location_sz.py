from rest_framework import serializers


class CompleteLocationSz(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    country = serializers.CharField(read_only=True)
    country_id = serializers.IntegerField(read_only=True)
    region = serializers.CharField(read_only=True)
    region_id = serializers.IntegerField(read_only=True)
    commune = serializers.CharField(read_only=True)
    commune_id = serializers.IntegerField(read_only=True)
    street = serializers.CharField(read_only=True)
    street_id = serializers.IntegerField(read_only=True)
    edification_type = serializers.CharField(read_only=True)
    tower = serializers.CharField(read_only=True)
    tower_id = serializers.IntegerField(read_only=True)
    floor_num = serializers.CharField(read_only=True)
    floor_num_id = serializers.IntegerField(read_only=True)
    departament = serializers.CharField(read_only=True)
    number = serializers.CharField(read_only=True)
    street_location = serializers.CharField(read_only=True)
    street_location_pk = serializers.IntegerField(read_only=True)
