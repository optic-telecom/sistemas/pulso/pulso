from rest_framework import serializers
from places.models import Commune


class CommuneSerializer(serializers.ModelSerializer):
    """ Serialiazer de las comunas. """
    class Meta:
        model = Commune
        fields = ('id', 'name', 'region')

    def is_valid(self, *args, **kwargs):
        """ Valida que el nombre no se haya creado ya para una región. """
        if not self.instance:
            name = self.initial_data.get('name')
            region = self.initial_data.get('region')
        else:
            name = self.initial_data.get('name', self.instance.name)
            region = self.initial_data.get('region', self.instance.region)
        try:
            commune = Commune.objects.get(
                name=name, region=region)
            if self.instance and commune.id != self.instance.id:
                _err = {'error': 'Esa comuna ya existe para esa región.'}
                raise serializers.ValidationError(_err)
            elif not self.instance:
                _err = {'error': 'Esa comuna ya existe para esa región.'}
                raise serializers.ValidationError(_err)
        except Commune.DoesNotExist:
            pass
        return super(CommuneSerializer, self).is_valid(*args, **kwargs)

