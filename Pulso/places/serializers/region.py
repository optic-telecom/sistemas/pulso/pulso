from rest_framework import serializers
from places.models import (Region)


class RegionSerializer(serializers.ModelSerializer):
    """ Serialiazer de las regiones. """
    class Meta:
        model = Region
        fields = ('id', 'name', 'country')

    def is_valid(self, *args, **kwargs):
        """ Valida que el nombre no se haya creado ya para un país. """
        if not self.instance:
            name = self.initial_data.get('name')
            country = self.initial_data.get('country')
        else:
            name = self.initial_data.get('name', self.instance.name)
            country = self.initial_data.get('country', self.instance.country)
        try:
            region = Region.objects.get(
                name=name, country=country)
            if self.instance and region.id != self.instance.id:
                _err = {'error': 'Esa región ya existe para ese país.'}
                raise serializers.ValidationError(_err)
            elif not self.instance:
                _err = {'error': 'Esa región ya existe para ese país.'}
                raise serializers.ValidationError(_err)
        except Region.DoesNotExist:
            pass
        return super(RegionSerializer, self).is_valid(*args, **kwargs)

