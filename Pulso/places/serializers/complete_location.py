from rest_framework import serializers
from places.models import (CompleteLocation)
from places.models.street_location import (TYPE_CONDOMINIUM)


class CompleteLocationSerializer(serializers.ModelSerializer):
    """
    Serialiazer de los números de condominio o de
    oficina/departamento de un piso.
    """
    class Meta:
        model = CompleteLocation
        fields = ('id', 'floor', 'number', 'street_location')

    def is_valid(self, *args, **kwargs):
        """ Valida que si tiene floor es porque no tiene street_location, y que
            al menos tiene uno. Luego se hacen las validaciones unique_together
            para (floor,number) o (street_location
            (tiene que ser condominio), number).
            """
        if not self.instance:
            floor = self.initial_data.get('floor')
            number = self.initial_data.get('number')
            street_location = self.initial_data.get('street_location')
        else:
            floor = self.initial_data.get('floor', self.instance.floor)
            number = self.initial_data.get('number', self.instance.number)
            street_location = self.initial_data.get(
                'street_location', self.instance.street_location
                )
        if not floor and not street_location:
            _err = {
                'error': 'Debe especificar un piso o una ubicación de calle.'
                }
            raise serializers.ValidationError(_err)
        if floor and street_location:
            _err = {
                'error': """Si se trata de un edificio o una empresa
                        especifique un piso, si es un condominio especifique
                        la ubicación de la calle, pero no ambos."""
                    }
            raise serializers.ValidationError(_err)
        if floor:
            try:
                location = CompleteLocation.objects.get(
                    number=number, floor=floor)
                if self.instance and location.id != self.instance.id:
                    _err = {'error': 'Ese número ya existe para ese piso.'}
                    raise serializers.ValidationError(_err)
                elif not self.instance:
                    _err = {'error': 'Ese número ya existe para ese piso.'}
                    raise serializers.ValidationError(_err)
            except CompleteLocation.DoesNotExist:
                pass
        if street_location:
            try:
                location = CompleteLocation.objects.get(
                    number=number, street_location=street_location)
                if self.instance and location.id != self.instance.id:
                    _err = {
                        'error': """Ese número ya existe para
                                    esa ubicación de condominio."""
                        }
                    raise serializers.ValidationError(_err)
                elif not self.instance:
                    _err = {
                        'error': """Ese número ya existe para
                                    esa ubicación de condominio."""
                        }
                    raise serializers.ValidationError(_err)
            except CompleteLocation.DoesNotExist:
                pass
        return super(CompleteLocationSerializer, self).is_valid(*args, **kwargs)

    def validate(self, attrs):
        """ Valida que edification_type de
        street_location es un condominio. """
        if attrs['street_location'].edification_type != TYPE_CONDOMINIUM:
            _err = {
                'error': """La ubicación de calle no coincide con un condominio,
                        por lo que no puede tener un número asociado."""
                        }
            raise serializers.ValidationError(_err)
        return super().validate(attrs)


