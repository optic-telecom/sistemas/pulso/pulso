from rest_framework import routers

from places.apis import (
    CountryViewSet, RegionViewSet, CommuneViewSet,
    StreetViewSet, StreetLocationViewSet, TowerViewSet, FloorViewSet,
    CompleteLocationViewSet,
    CompleteLocationRead,
    SearchRegionViewSet, SearchCommuneViewSet, SearchStreetViewSet,
    SearchStreetLocationViewSet, SearchCompleteLocationViewSet
    )


router = routers.DefaultRouter()

router.register(r'country', CountryViewSet, 'country_api')
router.register(r'region', RegionViewSet, 'region_api')
router.register(r'commune', CommuneViewSet, 'commune_api')
router.register(r'street', StreetViewSet, 'street_api')
router.register(r'streetlocation', StreetLocationViewSet, 'streetlocation_api')
router.register(r'tower', TowerViewSet, 'tower_api')
router.register(r'floor', FloorViewSet, 'floor_api')
router.register(r'completelocation', CompleteLocationViewSet)

# vista movistar

router.register(r'complete-location', CompleteLocationRead, 'complete-loc')
router.register(r'search-location', CompleteLocationRead, 'search-loc')

# vistas factibilizador

router.register(
        r'search-region',
        SearchRegionViewSet,
        'search-region'
    )
router.register(
        r'search-commune/(?P<region>\d+)',
        SearchCommuneViewSet,
        'search-commune'
    )
router.register(
        r'search-street/(?P<commune>\d+)',
        SearchStreetViewSet,
        'search-street'
    )
router.register(
        r'search-street-location/(?P<street>\d+)',
        SearchStreetLocationViewSet,
        'search-street-location'
    )
router.register(
        r'search-complete-location/(?P<streetlocation>\d+)',
        SearchCompleteLocationViewSet,
        'search-complete-location'
    )

