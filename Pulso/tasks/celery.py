from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from django.conf import settings
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.local')

app = Celery('tasks')
app.config_from_object('django.conf:settings', namespace='CELERY')

app.conf.beat_schedule = {
    "matrix-prod-backup": {
        'task': "run_script",
        'schedule':  crontab(minute=0, hour=5),
        'args': ([1]),
    },
    "iris-prod-backup": {
        'task': "run_script",
        'schedule': crontab(minute=0, hour=6),
        'args': ([2]),
    },
}

app.autodiscover_tasks(settings.INSTALLED_APPS)