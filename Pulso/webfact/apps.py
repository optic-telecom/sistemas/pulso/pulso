from django.apps import AppConfig


class WebfactConfig(AppConfig):
    name = 'webfact'
    verbose_name = 'Factibilidad para la web'