from django.contrib import admin
from webfact.models import FactibilityIframeFull

template = 'admin/factibilizador/factibilizadorIframeAdmin.html'


@admin.register(FactibilityIframeFull)
class FactibilityIframeFullAdmin(admin.ModelAdmin):
    change_list_template = template
    list_display = ('node', )
