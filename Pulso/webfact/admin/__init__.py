from .factibility_full import FactibilityFullAdmin
from .factibility_full_iframe import FactibilityIframeFullAdmin

__all__ = [
    "FactibilityFullAdmin",
    "FactibilityIframeFullAdmin"
]