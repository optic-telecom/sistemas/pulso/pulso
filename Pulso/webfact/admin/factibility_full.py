from django.contrib import admin
from webfact.models import FactibilityFull


@admin.register(FactibilityFull)
class FactibilityFullAdmin(admin.ModelAdmin):
    change_list_template = 'admin/factibilizador/factibilizadorAdmin.html'
    list_display = ('node',)

