from rest_framework.routers import DefaultRouter
from django.urls import path
from webfact.autocompletes import AutocompleteSelect2
# from .views import (FactibilityIrisViewset,
#                     FactibilityNewViewSet,
#                     LocationIrisViewSet)


from webfact.viewsets import (
                    FactibilityIrisViewset,
                    FactibilityNewViewSet,
                    LocationIrisViewSet
                    )

from places.models import (
        Region,
        Commune,
        Street,
        StreetLocation,
        CompleteLocation
        )
router = DefaultRouter()

router.register(r'^factibility',
                FactibilityNewViewSet,
                'factibility')

router.register(r'factibility-iris',
                FactibilityIrisViewset,
                'factibility-iris')

router.register(r'locations-iris',
                LocationIrisViewSet,
                'locations-iris')


urlpatterns = [
    path(
        'region-ac/',
        AutocompleteSelect2.as_view(
                                model=Region,
                                filter_by='name__icontains'
                                ),
        name="region-ac"
        ),
    path(
        'commune-ac/<int:region>/',
        AutocompleteSelect2.as_view(
                                model=Commune,
                                filter_by='name__icontains',
                                kwargs=True,
                                key='region'
                                ),
        name="commune-ac"),
    path(
        'street-ac/<int:commune>/',
        AutocompleteSelect2.as_view(
                                model=Street,
                                filter_by='name__icontains',
                                kwargs=True,
                                key='commune'
                                ),
        name="street-ac"
        ),
    path(
        'street-location-ac/<int:street>/',
        AutocompleteSelect2.as_view(
                                model=StreetLocation,
                                filter_by='street_no__istartswith',
                                kwargs=True,
                                key='street',
                                label='street_no'
                                ),
        name="street-location-ac"
        ),
    path(
        'complete-location-ac/<int:street_location>/',
        AutocompleteSelect2.as_view(
                                model=CompleteLocation,
                                filter_by='floor__number__istartswith',
                                kwargs=True,
                                key='street_location',
                                label='departament',
                                second_filter_by={
                                    'street_location__street_no': ''
                                    }
                                ),
        name="complete-location-ac"
        ),
]
