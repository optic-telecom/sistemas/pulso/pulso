from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator

from places.models import CompleteLocation
from webfact.models import AvailabilityAddress, FutureAvailabilityAddress
from webfact.serializers import FactibilitySerializer
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.permissions import IsAuthenticated
from utils.permissions import IsSystemInternalPermission


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@method_decorator(cache_page(CACHE_TTL), name='dispatch')
class FactibilityNewViewSet(ReadOnlyModelViewSet):
    """ Endpoint para el manejo de la factibilidad """
    permission_classes = (IsSystemInternalPermission | IsAuthenticated, )

    def filterFactibility(self, *args, **kwargs):
        address = kwargs.get('address', None)
        street_location = kwargs.get('street_location', None)
        exists = True

        print(address)

        if FutureAvailabilityAddress.objects.filter(**kwargs).exists():
            obj = FutureAvailabilityAddress.objects.filter(**kwargs).last()
        elif AvailabilityAddress.objects.filter(**kwargs).exists():
            obj = AvailabilityAddress.objects.filter(**kwargs).last()
        else:
            exists = False
            if address:
                filtro_ = {'pk': address, 'street_location': street_location}
                direccion = CompleteLocation.objects.get(**filtro_)
                sl = direccion.street_location.pk
                direccion = direccion.__str__()
            else:
                filtro_ = {'street_location': street_location}
                direccion = CompleteLocation.objects.filter(**filtro_)[0]
                sl = direccion.street_location.pk
                direccion = direccion.street_location.__str__()
            obj = {
                'address': direccion,
                'street_location': sl,
                'operator': None,
                'node': None,
                'factibility': 'NO',
                'plans': None,
                }

        return exists, obj

    def get_factibilidad(self, **kwargs):
        # Busca la existencia del id en las vistas preparadas con postgres
        # recibe como parametro el id de la complete_location por el cual
        # filtra el resultado, de este se toma el último elemento de la lista,
        # para emplear el método json_to_web declarado como @property
        # en la clase

        try:
            if 'address' not in kwargs:
                filtro = {
                    'street_location': kwargs['street_location'],
                    }
            else:
                filtro = {
                        'address': kwargs['address'],
                        'street_location': kwargs['street_location']
                        }

            exists, obj = self.filterFactibility(**filtro)
            if exists:
                # el condicional expresa la existencia en cualquiera de
                # las 2 vistas
                # AvailabilityAddress ó FutureAvailabilityAddress
                obj = obj.json_to_web(kwargs['operator'])

            output = FactibilitySerializer(obj, many=False).data
        except Exception as e:
            print(str(e))

            output = FactibilitySerializer({}, many=False).data

        return output

    def list(self, request, *args, **kwargs):
        dict_ = dict()

        if 'address' in self.request.GET:
            dict_['address'] = self.request.GET.get('address')

        if 'street_location' in self.request.GET:
            dict_['street_location'] = self.request.GET.get('street_location')

        if 'operator' in self.request.GET:
            dict_['operator'] = self.request.GET.get('operator')

        if dict_['street_location'] and dict_['operator']:

            try:
                output = self.get_factibilidad(**dict_)
            except Exception as e:
                return Response({'error': str(e)})
        else:
            return Response({
                'error': """Debes ingresar el id de una ubicación de
                            calle para iniciar la búsqueda de información."""
                },
                status=400
                )

        return Response(output, status=200)

