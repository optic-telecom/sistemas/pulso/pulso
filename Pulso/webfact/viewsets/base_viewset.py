from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework import mixins
from utils.permissions import IsSystemInternalPermission


@method_decorator(csrf_exempt, name='dispatch')
class PostListMixin(mixins.CreateModelMixin, ReadOnlyModelViewSet):
    """ Mixin encargado de listar los servicios via post """
    permission_classes = (IsSystemInternalPermission | IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
