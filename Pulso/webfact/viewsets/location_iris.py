from places.models import StreetLocation
from rest_framework.response import Response
from places.serializers import CompleteLocationSz
from webfact.viewsets.base_viewset import PostListMixin


class LocationIrisViewSet(PostListMixin):
    """
        Api dirigida al sistema iris para verificar las direcciones
        existentes dentro de una localidad,
        se requieren 3 parametros:
        1. tipo, indica sobre cual modelo se va filtrar el qs
        2. location, indica el id del la localidad
        3. locations, indica una lista de complete location
        asociadas a la localidad anterior.
    """
    serializer_class = CompleteLocationSz

    def get_queryset(self):
        queryset = StreetLocation.objects.none()

        tipo = self.request.data.get('tipo', None)
        location = self.request.data.get('location', None)
        locations = self.request.data.get('locations', [])

        # from pdb import set_trace
        # set_trace()

        try:

            switch = dict(
                region=dict(
                    street__commune__region__pk=location,
                    pk__in=locations
                ),
                commune=dict(
                    street__commune__pk=location,
                    pk__in=locations
                ),
                street=dict(
                    street__pk=location,
                    pk__in=locations
                )
            )
            filtro = switch.get(tipo)
            queryset = StreetLocation.objects.filter(**filtro)
        except Exception as e:
            print(str(e))

        return queryset

    def list(self, request, *args, **kwargs):
        listado = list(map(lambda x: x.pk, self.get_queryset()))
        return Response(listado)
