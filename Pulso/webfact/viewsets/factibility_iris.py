
from webfact.serializers import (FactibilityIrisSerializer)
from rest_framework.response import Response
from rest_framework import mixins
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from rest_framework.permissions import IsAuthenticated
from utils.permissions import IsSystemInternalPermission
from places.models import StreetLocation
from webfact.viewsets.factibility_new import FactibilityNewViewSet
# Create your views here.


@method_decorator(csrf_exempt, name='dispatch')
class FactibilityIrisViewset(mixins.CreateModelMixin, FactibilityNewViewSet):
    serializer_class = FactibilityIrisSerializer
    permission_classes = (IsSystemInternalPermission | IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def get_queryset(self, street_location):
        queryset = StreetLocation.objects.get(pk=street_location)
        return queryset

    def list(self, request, *args, **kwargs):
        street_location = request.data.get('street_location', [])
        output = list()

        if street_location:
            for sl in street_location:
                obj = self.get_queryset(sl)

                output.append({
                    'street_location_id': obj.pk,
                    'street_location': obj.__str__(),
                })
            output = FactibilityIrisSerializer(output, many=True).data
            return Response(output, status=200)
        else:
            error = {
                'error': """Debes ingresar un listado de street_location id's,
                        por favor ingresa al menos solo uno."""
            }
            return Response(error, status=400)
