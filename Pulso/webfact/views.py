from django.shortcuts import render
from django.db.models import Q
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from models.models import Availability, FutureAvailability

from .serializers import (PlansToWebSerializer,
                        FactibilitySerializer,
                        FactibilityIrisSerializer)
from places.models import CompleteLocation,StreetLocation
from rest_framework.response import Response
from rest_framework import mixins
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from rest_framework.permissions import IsAuthenticated, AllowAny
from utils.permissions import IsSystemInternalPermission
# Create your views here.


from .models import AvailabilityAddress,FutureAvailabilityAddress

from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
from django.utils.decorators import method_decorator


@method_decorator(cache_page(CACHE_TTL),name='dispatch')
class FactibilityNewViewSet(ReadOnlyModelViewSet):
    """ Endpoint para el manejo de la factibilidad """
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)

    def filterFactibility(self,*args,**kwargs):
        address = kwargs.get('address',None)
        street_location = kwargs.get('street_location',None)
        exists = True

        print(address)

        if FutureAvailabilityAddress.objects.filter(**kwargs).exists():
            obj = FutureAvailabilityAddress.objects.filter(**kwargs).last()
        elif AvailabilityAddress.objects.filter(**kwargs).exists():
            obj = AvailabilityAddress.objects.filter(**kwargs).last()
        else:
            exists = False
            if address:
                filtro_ = {'pk':address,'street_location':street_location}
                direccion = CompleteLocation.objects.get(**filtro_)
                sl = direccion.street_location.pk
                direccion = direccion.__str__()
            else:
                filtro_ = {'street_location':street_location}
                direccion = CompleteLocation.objects.filter(**filtro_)[0]
                sl = direccion.street_location.pk
                direccion = direccion.street_location.__str__()
            
            obj = {
                'address':direccion,
                'street_location':sl,
                'operator':None,
                'node':None,
                'factibility':'NO',
                'plans':None,
                }

        return exists, obj

    def get_factibilidad(self,**kwargs):
        # Busca la existencia del id en las vistas preparadas con postgres
        # recibe como parametro el id de la complete_location por el cual
        # filtra el resultado, de este se toma el último elemento de la lista,
        # para emplear el método json_to_web declarado como @property en la clase

        try:
            if not 'address' in kwargs:
                filtro = {
                    'street_location':kwargs['street_location'],
                    }
            else:
                filtro = {
                        'address':kwargs['address'],
                        'street_location':kwargs['street_location']
                        }

            exists, obj = self.filterFactibility(**filtro)
            
            if exists:
                # el condicional expresa la existencia en cualquiera de las 2 vistas
                # AvailabilityAddress ó FutureAvailabilityAddress
                obj = obj.json_to_web(kwargs['operator'])

            output = FactibilitySerializer(obj,many=False).data
        except Exception as e:
            print(str(e))

            output = FactibilitySerializer({},many=False).data

        return output

    def list(self,request,*args,**kwargs):
        dict_ = dict()

        if 'address' in self.request.GET:
            dict_['address'] = self.request.GET.get('address')

        if 'street_location' in self.request.GET:
            dict_['street_location'] = self.request.GET.get('street_location')

        if 'operator' in self.request.GET:
            dict_['operator'] = self.request.GET.get('operator')
        
        if dict_['street_location'] and dict_['operator']:

            try:
                output = self.get_factibilidad(**dict_)
            except Exception as e:
                return Response({'error':str(e)})
        else:
            return Response({'error':'Debes ingresar el id de una ubicación de calle para iniciar la búsqueda de información.'},status=400)
        
        return Response(output,status=200)


# ====================== LISTADO VIA POST ======================== #
# ====================== LISTADO VIA POST ======================== #


@method_decorator(csrf_exempt,name='dispatch')
class PostListMixin(mixins.CreateModelMixin,ReadOnlyModelViewSet):
    """ Mixin encargado de listar los servicios via post """
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)

    def create(self,request,*args,**kwargs):
        return self.list(request,*args,**kwargs)

# ====================== LISTADO VIA POST ======================== #
# ====================== LISTADO VIA POST ======================== #

@method_decorator(csrf_exempt,name='dispatch')
class FactibilityIrisViewset(mixins.CreateModelMixin,FactibilityNewViewSet):
    serializer_class = FactibilityIrisSerializer
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)


    def create(self,request,*args,**kwargs):
        return self.list(request,*args,**kwargs)

    def get_queryset(self,street_location):
        queryset = StreetLocation.objects.get(pk=street_location)
        return queryset

    def list(self,request,*args,**kwargs):
        street_location = request.data.get('street_location',[])
        output = list()
        
        if street_location:
            for sl in street_location:
                obj = self.get_queryset(sl)
            
                output.append({
                    'street_location_id':obj.pk,
                    'street_location':obj.__str__(),
                })
            output = FactibilityIrisSerializer(output,many=True).data
            return Response(output,status=200)
        else:
            error = {
                'error':"Debes ingresar un listado de street_location id's, por favor ingresa al menos solo uno."
            }
            return Response(error,status=400)
        


from places.serializers import CompleteLocationSz
# @method_decorator(csrf_exempt,name='dispatch')
class LocationIrisViewSet(PostListMixin):
# class LocationIrisViewSet(mixins.CreateModelMixin,ReadOnlyModelViewSet):
    """ 
        Api dirigida al sistema iris para verificar las direcciones existentes dentro de una localidad,
        se requieren 3 parametros: 
        1. tipo, indica sobre cual modelo se va filtrar el qs
        2. location, indica el id del la localidad
        3. locations, indica una lista de complete location asociadas a la localidad anterior.
    """
    serializer_class = CompleteLocationSz

    def get_queryset(self):
        queryset = StreetLocation.objects.none()

        tipo = self.request.data.get('tipo',None)
        location = self.request.data.get('location',None)
        locations = self.request.data.get('locations',[])

        # from pdb import set_trace
        # set_trace()

        try:

            switch = dict(
                region = dict(
                    street__commune__region__pk=location,
                    pk__in=locations 
                ),
                commune = dict(
                    street__commune__pk=location,
                    pk__in=locations 
                ),
                street = dict(
                    street__pk=location,
                    pk__in=locations 
                )
            )

            

            filtro = switch.get(tipo)
            
            queryset = StreetLocation.objects.filter(**filtro)
        except Exception as e:
            print(str(e))

        return queryset

    def list(self,request,*args,**kwargs):
        listado = list(map(lambda x:x.pk, self.get_queryset()))
        return Response(listado)
