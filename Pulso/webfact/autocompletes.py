from dal import autocomplete
from django.db.models import Q
from django.contrib.auth import get_user_model
from rest_framework_jwt.settings import api_settings
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
User = get_user_model()


class AutocompleteSelect2(autocomplete.Select2QuerySetView):
    """
        Filtrado Select2 Génerico recibe 2 atributos obligatorios
        model el cual indica el modelo sobre el cual se va realizar
        la selección y filter_by que indica el nombre del campo
        incluido también el lookup_field que se va usar y por último
        un second_filter_by que es opcional y recibe un diccionario
        ej.

        model=Voucher
        filter_by = "code__istarstwith"
        second_filter_by = {} #opcional
    """

    model = None
    filter_by = None
    second_filter_by = {}
    kwargs = False
    key = None
    value = None
    label = None

    def get_queryset(self):
        qs = self.model.objects.all()
        id = self.request.GET.get('id', None)

        if id:
            qs = self.model.objects.filter(id__in=[id])

        if self.kwargs:
            filtro = {self.key: self.kwargs[self.key]}
            qs = self.model.objects.filter(**filtro)

        if self.second_filter_by:
            qs = qs.filter(
                Q(street_location=self.kwargs[self.key]) &
                Q(street_location__street_no__icontains=self.q) |
                Q(floor__number__icontains=self.q)
                )

        else:
            if self.filter_by and self.q:
                filtro = {self.filter_by: self.q}
                if self.kwargs:
                    qs = qs.filter(**filtro)
                else:
                    qs = self.model.objects.filter(**filtro)

        return qs

    def get_result_label(self, item):
        if self.label:
            label = getattr(item, self.label)
            return label
        else:
            return super().get_result_label(item)

    def get_selected_result_label(self, item):
        if self.label:
            label = getattr(item, self.label)
            return label
        else:
            return super().get_result_label(item)


class ListadoAutocomplete(autocomplete.Select2ListView):
    listado = list()

    def get_list(self):
        return self.listado
