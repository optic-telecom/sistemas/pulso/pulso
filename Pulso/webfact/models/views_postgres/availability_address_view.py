from django.db import models
from models.models import Availability, Operator
from webfact.serializers import PlansToWebSerializer
from places.models import CompleteLocation


class AvailabilityAddress(models.Model):
    id = models.IntegerField(primary_key=True)
    address = models.IntegerField()
    street_location = models.IntegerField()
    availability = models.IntegerField()

    def __str__(self):
        return f'{self.id}'

    def json_to_web(self, operator=2):
        availability = Availability.objects.get(pk=self.availability)
        complete_location = (CompleteLocation.objects.get(
                                    pk=self.address,
                                    street_location__pk=self.street_location
                                    )
                                )
        operador = Operator.objects.get(pk=operator)
        planes = availability.node.plans.filter(
                    operator=operador
                ).order_by('apparence_order', '-price')
        factibility = 'SI' if planes.count() > 0 else 'NO'
        planes = PlansToWebSerializer(planes, many=True).data
        # print("Node", str(availability.node.pk))
        output = dict(
            availability=self.availability,
            address=complete_location,
            address_id=complete_location.pk,
            street_location=complete_location.street_location,
            street_location_id=complete_location.street_location.pk,
            node=availability.node,
            operator=operador.name,
            factibility=factibility,
            plans=planes if factibility == 'SI' else list()
        )
        return output

    class Meta:
        managed = False
        db_table = 'public\".\"models_availability_view'
