from django.db import models
from models.models import FutureAvailability, Operator
from webfact.serializers import PlansToWebSerializer
from places.models import CompleteLocation


class FutureAvailabilityAddress(models.Model):
    id = models.IntegerField(primary_key=True)
    address = models.IntegerField()
    street_location = models.IntegerField()
    future_availability = models.IntegerField()

    def __str__(self):
        return f'{self.id}'

    def json_to_web(self, operator=2):
        f_availability = FutureAvailability.objects.get(
            pk=self.future_availability
            )
        complete_location = (CompleteLocation.objects.get(
                                pk=self.address,
                                street_location__pk=self.street_location
                                )
                            )
        operador = Operator.objects.get(pk=operator)
        planes = f_availability.plans.filter(
                    operator=operador
                    ).order_by('apparence_order', '-price')
        factibility = 'PROXIMAMENTE' if planes.count() > 0 else 'NO'
        planes = PlansToWebSerializer(planes, many=True).data

        output = dict(
            availability=self.future_availability,
            address=f'{complete_location}',
            address_id=complete_location.pk,
            operator=operador.name,
            node=None,
            factibility=factibility,
            plans=planes if factibility == 'PROXIMAMENTE' else list()
        )
        return output

    class Meta:
        managed = False
        db_table = 'public\".\"models_futureavailability_view'
