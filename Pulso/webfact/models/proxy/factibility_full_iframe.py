from models.models.availability import Availability


class FactibilityIframeFull(Availability):

    class Meta:
        proxy = True
        verbose_name = 'Factibilidad Iframe'
        verbose_name_plural = 'Factibilidades Iframe'

    def __str__(self):
        return f'{self.node.code}'
