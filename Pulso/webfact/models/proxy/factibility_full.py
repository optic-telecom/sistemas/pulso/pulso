from models.models import Availability


class FactibilityFull(Availability):

    class Meta:
        proxy = True
        verbose_name = 'Factibilidad'
        verbose_name_plural = 'Factibilidades'

    def __str__(self):
        return f'{self.node.code}'
