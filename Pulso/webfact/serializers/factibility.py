from rest_framework import serializers


class FactibilitySerializer(serializers.Serializer):
    availability = serializers.IntegerField(read_only=True)
    address = serializers.CharField(read_only=True)
    address_id = serializers.IntegerField(read_only=True)
    street_location = serializers.CharField(read_only=True)
    street_location_id = serializers.IntegerField(read_only=True)
    operator = serializers.CharField(read_only=True)
    node = serializers.CharField(read_only=True)
    factibility = serializers.CharField(read_only=True)
    availability_on = serializers.DateTimeField(
        read_only=True, format="%d-%m-%Y"
        )
    plans = serializers.JSONField(read_only=True)
