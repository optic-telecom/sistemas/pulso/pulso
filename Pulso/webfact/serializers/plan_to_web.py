from rest_framework import serializers
from models.models import Plan


class PlansToWebSerializer(serializers.ModelSerializer):
    price = serializers.IntegerField(read_only=True)
    promotional_price = serializers.IntegerField(read_only=True)
    availability_price = serializers.IntegerField(read_only=True)
    promotional_availability_price = serializers.IntegerField(read_only=True)

    class Meta:
        model = Plan
        fields = [
            'pk', 'name', 'tradename',
            'price', 'promotional_price',
            'get_operator',
            'get_category',
            'active', 'uf',
            'download_speed',
            'upload_speed',
            'availability_price',
            'promotional_availability_price']
