from rest_framework import serializers


class FactibilityIrisSerializer(serializers.Serializer):
    street_location_id = serializers.IntegerField(read_only=True)
    street_location = serializers.CharField(read_only=True)
