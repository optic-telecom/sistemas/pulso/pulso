from django.urls import path, re_path
from rest_framework import routers
from backup.views import (
    BackupSystemList, BackupSystemDetail,
    BackupReceptorList, BackupReceptorDetail
    )

router = routers.DefaultRouter()

urlpatterns = [
    re_path(
        r'^api/backup',
        BackupSystemList.as_view()
        ),
    path(
        r'api/detail_backup/<int:pk>/',
        BackupSystemDetail.as_view()
        ),
    re_path(
        r'^api/copy_backup',
        BackupReceptorList.as_view()
        ),
    path(
        r'api/detail_receptor/<int:pk>/',
        BackupReceptorDetail.as_view()
        ),
]
