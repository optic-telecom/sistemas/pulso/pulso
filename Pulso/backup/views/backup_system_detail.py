from backup.models import BackupSystem
from backup.serializers import BackupSystemSerializer

from django.http import Http404

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django_celery_beat.models import PeriodicTask, IntervalSchedule


class BackupSystemDetail(APIView):
    """
    Retrieve, update or delete a BackupSystem instance.
    """
    def get_object(self, pk):
        try:
            return BackupSystem.objects.get(id_system=pk)
        except BackupSystem.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        system = self.get_object(pk)
        serializer = BackupSystemSerializer(system)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        system = self.get_object(pk)
        serializer = BackupSystemSerializer(system, data=request.data)
        if serializer.is_valid():
            serializer.save()
            pe = PeriodicTask.objects.filter(name=str(system.id_system))
            sis = BackupSystem.objects.get(id_system=system.id_system)
            if sis.type_frequency == "days":
                schedule, created = IntervalSchedule.objects.get_or_create(
                    every=sis.frequency,
                    period=IntervalSchedule.DAYS)
            elif sis.type_frequency == "hours":
                schedule, created = IntervalSchedule.objects.get_or_create(
                    every=sis.frequency,
                    period=IntervalSchedule.HOURS)
            elif sis.type_frequency == "minutes":
                schedule, created = IntervalSchedule.objects.get_or_create(
                    every=sis.frequency,
                    period=IntervalSchedule.MINUTES)
            elif sis.type_frequency == "seconds":
                schedule, created = IntervalSchedule.objects.get_or_create(
                    every=sis.frequency,
                    period=IntervalSchedule.SECONDS
                )
            for i in pe:
                i.interval = schedule
                i.enabled = True
                i.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        system = self.get_object(pk)
        pe = PeriodicTask.objects.filter(name=str(system.id_system))
        for i in pe:
            i.enabled = False
            i.save()
        system.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

