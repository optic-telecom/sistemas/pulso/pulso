# Create your views here.
from backup.models import BackupReceptor
from backup.serializers import BackupReceptorSerializer

from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class BackupReceptorDetail(APIView):
    """
    Retrieve, update or delete a BackupReceptor instance.
    """
    def get_object(self, pk):
        try:
            return BackupReceptor.objects.get(id_receptor=pk)
        except BackupReceptor.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        respaldo = self.get_object(pk)
        serializer = BackupReceptorSerializer(respaldo)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        respaldo = self.get_object(pk)
        serializer = BackupReceptorSerializer(respaldo, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        respaldo = self.get_object(pk)
        respaldo.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
