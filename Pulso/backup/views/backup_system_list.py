import json
from backup.models import BackupSystem
from backup.serializers import BackupSystemSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django_celery_beat.models import PeriodicTask, IntervalSchedule


class BackupSystemList(APIView):
    """
    Lista todos los BackupSystem y permite crear uno nuevo.
    """
    def get(self, request, format=None):
        system = BackupSystem.objects.all()
        serializer = BackupSystemSerializer(system, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = BackupSystemSerializer(data=request.data)
        if serializer.is_valid():
            d = serializer.save()
            sis = BackupSystem.objects.get(id_system=d.id_system)
            if sis.type_frequency == "days":
                schedule, created = IntervalSchedule.objects.get_or_create(
                    every=sis.frequency,
                    period=IntervalSchedule.DAYS
                    )
            elif sis.type_frequency == "hours":
                schedule, created = IntervalSchedule.objects.get_or_create(
                    every=sis.frequency,
                    period=IntervalSchedule.HOURS
                    )
            elif sis.type_frequency == "minutes":
                schedule, created = IntervalSchedule.objects.get_or_create(
                    every=sis.frequency,
                    period=IntervalSchedule.MINUTES
                    )
            elif sis.type_frequency == "seconds":
                schedule, created = IntervalSchedule.objects.get_or_create(
                    every=sis.frequency,
                    period=IntervalSchedule.SECONDS
                )

            pe = PeriodicTask.objects.create(
                interval=schedule,
                name=str(sis.id_system),
                task='run_script',
                args=json.dumps([sis.id_system])
                )
            pe.enabled = True
            pe.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
