from backup.models import BackupReceptor
from backup.serializers import BackupReceptorSerializer

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class BackupReceptorList(APIView):
    """
    Lista todos los servidores asociados a un BackupSystem,
    donde se hace copia del respaldo.
    Permite crear dicha relación.
    """
    def get(self, request, format=None):
        respaldo = BackupReceptor.objects.all()
        serializer = BackupReceptorSerializer(respaldo, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = BackupReceptorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


