import subprocess
import datetime
import shlex
from tasks.celery import app as celery_app
from backup.models import BackupSystem, BackupReceptor
from config.settings.base import BASE_DIR
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from os import remove


@celery_app.task(name="run_script")
def run_script(pk):
    # Obtiene los objectos que activaron la tarea.
    systems = BackupSystem.objects.get(id_system=int(pk))
    respados = BackupReceptor.objects.filter(id_system=systems)

    # Obtiene la ruta del archivo.
    p = BASE_DIR.split("/")
    path = ""
    for j in p[:len(p)-1]:
        path = path + str(j)+"/"

    # Creando el nombre del archivo backup.
    name = str(systems.system).lower().split(" ")
    name_backup = ""
    for i in name:
        name_backup = name_backup+i

    datetime_object = datetime.datetime.now()
    date = str(datetime_object).split(" ")

    # Se concatenan los strings para obtener el nombre del archivo.
    env = str(systems.type_entorno)
    name_backup = f"{name_backup}_{date[0]}_{date[1]}_{env}"

    # Ejecuta el script de crear los backups.
    subprocess.call(shlex.split('./scripts/create_backup.sh '+ str(systems.server_username)+ ' '+ str(systems.server_ip)+ ' '+ str(systems.server_password)+ ' '+ str(systems.server_route_save)+ ' '+ str(systems.bd_password)+ ' '+ str(systems.bd_name)+ ' '+ str(systems.bd_user)+' '+ str(name_backup)+' '+str(path)+' '+str(systems.bd_type)+' '+str(systems.bd_port)+' '+str(systems.type_command)))

    # Para todos los receptores, ejecuta el respectivo script.
    for i in respados:
        if (not systems.copy_to_server) and i.receptor_type == "server":
            pass
        elif (not systems.copy_to_drive) and i.receptor_type == "drive":
            pass
        else:
            subprocess.call(shlex.split('./scripts/move_backup.sh '+ str(systems.server_username)+ ' '+ str(systems.server_ip)+ ' '+ str(systems.server_password)+ ' '+ str(systems.server_route_save)+ ' '+ str(i.receptor_password)+ ' '+ str(name_backup)+ ' '+ str(i.receptor_server_username)+ ' '+ str(i.receptor_server_ip)+ ' '+ str(i.receptor_route_save)+' '+str(path)))

    # Si hace la copia en drive.
    if systems.copy_to_drive:

        # Accede al drive
        g_login = GoogleAuth()

        # Leyendo las credenciales
        mensaje = systems.credentials
        if mensaje is not None:
            f = open('credential.txt', 'w')
            f.write(mensaje)
            f.close()

        # Carga las credenciales desde el archivo.
        g_login.LoadCredentialsFile("credential.txt")
        if g_login.credentials is None:
            g_login.LocalWebserverAuth()
        elif g_login.access_token_expired:
            g_login.Refresh()
        else:
            g_login.Authorize()

        # Guarda las credenciales en el archivo.
        g_login.SaveCredentialsFile("credential.txt")

        # Guardando las credenciales en la tabla.
        f = open("credential.txt", 'r')
        mensaje = f.read()
        systems.credentials = mensaje
        systems.save()
        f.close()

        # Limpiando el contenido del archivo temporal credential.
        f = open("credential.txt", 'w')
        f.write("")
        f.close()

        drive = GoogleDrive(g_login)
        # Mueve el archivo

        # Create GoogleDriveFile instance with title 'Hello.txt'.
        file1 = drive.CreateFile({'title': str(name_backup)+".sql.gz"})
        # Set content of the file from given string.
        file1.SetContentFile(str(path) + str(name_backup)+".sql.gz")
        file1.Upload()

    # Elimina los archivos que se trajeron,
    # si es que hace respaldo en un servidor.
    try:
        remove(str(path)+str(name_backup)+".sql.gz")
    except Exception:
        pass

    return True
