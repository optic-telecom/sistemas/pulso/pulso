from rest_framework import serializers
from backup.models import BackupSystem


class BackupSystemSerializer(serializers.ModelSerializer):
    """ Serialiazer de los sitemas que se le copiaran el backup. """
    class Meta:
        model = BackupSystem
        fields = (
            'id_system',
            'system',
            'bd_name',
            'bd_user',
            'bd_password',
            'bd_type',
            'bd_port',
            'type_entorno',
            'type_command',
            'frequency',
            'type_frequency',
            'server_ip',
            'server_username',
            'server_password',
            'server_route_save',
            'copy_to_server',
            'copy_to_drive'
        )

