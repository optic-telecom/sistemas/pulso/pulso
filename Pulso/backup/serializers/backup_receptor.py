from rest_framework import serializers
from backup.models import BackupReceptor


class BackupReceptorSerializer(serializers.ModelSerializer):
    """ Serialiazer de los servidores que recibiran los respaldos. """
    class Meta:
        model = BackupReceptor
        fields = (
            'id_receptor',
            'id_system',
            'receptor_server_username',
            'receptor_password',
            'receptor_server_ip',
            'receptor_route_save',
            'receptor_type'
            )

