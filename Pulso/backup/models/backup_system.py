from django.db import models
from django.core.validators import MinValueValidator
import re
from django.core.validators import RegexValidator
from webhosts.models import System


class BackupSystem(models.Model):
    TYPE_CHOICES = (
            ('mysql', 'Mysql'),
            ('postgresql', 'PostgreSQL')
        )
    TYPE_ENTORNO = (
            ('produccion', 'Produccion'),
            ('devel', 'Devel'),
            ('stage', 'Stage')
        )
    TYPE_COMMAND = (
            ('basic', 'Basico'),
            ('-C', 'Con opcion -C'),
            ('-h', 'Con opcion -h'),
            ('-h and -C', 'Con opcion -h y -C')
        )
    FREQUENCY_CHOICES = (
            ('days', 'Days'),
            ('hours', 'Hours'),
            ('minutes', 'Minutes'),
            ('seconds', 'Seconds')
        )
    id_system = models.AutoField(primary_key=True)
    system = models.ForeignKey(System, on_delete=models.CASCADE)
    bd_name = models.CharField(max_length=200)
    bd_user = models.CharField(max_length=200, blank=True)
    bd_password = models.CharField(max_length=200, blank=True)
    bd_type = models.CharField(
        max_length=11,
        choices=TYPE_CHOICES
        )
    bd_port = models.IntegerField(
        validators=[MinValueValidator(1)], null=True, blank=True
        )
    type_entorno = models.CharField(
        max_length=11, choices=TYPE_ENTORNO, default='devel'
        )
    type_command = models.CharField(
        max_length=20, choices=TYPE_COMMAND, default='basic'
        )
    frequency = models.IntegerField(validators=[MinValueValidator(1)])
    type_frequency = models.CharField(max_length=11, choices=FREQUENCY_CHOICES)
    server_ip = models.CharField(
        max_length=20,validators=[RegexValidator((re.compile('([0-9]{1,3}.?){4}')), ('ip incorrecta'), 'invalid')]
        )
    server_username = models.CharField(max_length=200)
    server_password = models.CharField(max_length=200)
    server_route_save = models.CharField(max_length=500, blank=True, null=True)
    copy_to_server = models.BooleanField(blank=True, default=False)
    copy_to_drive = models.BooleanField(blank=True, default=False)
    credentials = models.CharField(max_length=2000, blank=True)

    def __str__(self):
        '''Devuelve el modelo en tipo String'''
        return str(self.system.name)