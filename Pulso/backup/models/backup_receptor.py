from django.db import models
import re
from django.core.validators import RegexValidator


class BackupReceptor(models.Model):
    """ Información de los receptores de los backups o respaldos. """
    TYPE_CHOICES = (
            ('drive', 'Drive'),
            ('server', 'Server')
        )
    id_receptor = models.AutoField(primary_key=True)
    id_system = models.ForeignKey("BackupSystem", on_delete=models.CASCADE)
    receptor_server_username = models.CharField(max_length=200)
    receptor_password = models.CharField(max_length=200)
    receptor_server_ip = models.CharField(
        max_length=20, validators=[RegexValidator((re.compile('([0-9]{1,3}.?){4}')), ('ip incorrecta'), 'invalid')]
        )
    receptor_route_save = models.CharField(max_length=500)
    receptor_type = models.CharField(
        max_length=11, choices=TYPE_CHOICES, default="server"
        )

    def __str__(self):
        '''Devuelve el modelo en tipo String'''
        return str(self.id_receptor)