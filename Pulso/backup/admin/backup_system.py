from django.contrib import admin
from backup.models import BackupSystem
# Register your models here.


@admin.register(BackupSystem)
class BackupSystemAdmin(admin.ModelAdmin):
    list_display = [
        'id_system',
        'system',
        'bd_name',
        'bd_user',
        'bd_type',
        'bd_port',
        'type_entorno',
        'frequency',
        'type_frequency',
        'server_ip',
        'server_username',
        'server_route_save',
        'copy_to_server',
        'copy_to_drive'
        ]
