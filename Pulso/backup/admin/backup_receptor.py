from django.contrib import admin
from backup.models import BackupReceptor


@admin.register(BackupReceptor)
class BackupReceptorAdmin(admin.ModelAdmin):
    list_display = [
        'id_receptor',
        'id_system',
        'receptor_server_username',
        'receptor_password', 'receptor_server_ip',
        'receptor_route_save'
        ]
