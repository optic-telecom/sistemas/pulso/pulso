# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from simple_history.models import HistoricalRecords

class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add = True, editable=True)
    modified = models.DateTimeField(auto_now = True)
    status_field = models.BooleanField(default = True)
    id_data = models.UUIDField(db_index = True,
                               null = True, blank = True,
                               editable = settings.DEBUG)
    history = HistoricalRecords(inherit = True)

    class Meta:
        abstract = True