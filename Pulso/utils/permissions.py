from rest_framework import permissions

class IsSystemInternalPermission(permissions.BasePermission):
    """ Permiso temporal para enviar información de réplica de los sistemas
        Usado en webhook transaction """
    message = 'No tiene permiso'

    def has_permission(self, request, view):
        token = request.META.get('HTTP_AUTHORIZATION',None)
        if token:
            return token == 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
        return False