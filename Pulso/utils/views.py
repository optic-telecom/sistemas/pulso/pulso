import requests

from django.urls import reverse
from django.conf import settings
from django.shortcuts import render
from django.views.decorators.clickjacking import xframe_options_exempt

from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.renderers import CoreJSONRenderer
from rest_framework.schemas import SchemaGenerator
from rest_framework_swagger import renderers
from rest_framework.response import Response

from enterprise.models import Enterprise
from models.models import Operator

from dynamic_preferences.registries import global_preferences_registry
global_preferences = global_preferences_registry.manager()


class MySchemaGenerator(SchemaGenerator):
    title = 'REST API Index'

    def get_link(self, path, method, view):
        link = super(MySchemaGenerator, self).get_link(path, method, view)
        link._fields += self.get_core_fields(view)
        return link

    def get_core_fields(self, view):
        if hasattr(view, "get_core_fields"):
            return getattr(view, 'get_core_fields')(coreapi, coreschema)
        return ()


class SwaggerSchemaView(APIView):
    _ignore_model_permissions = True
    exclude_from_schema = True
    permission_classes = [AllowAny]
    renderer_classes = [
        CoreJSONRenderer,
        renderers.OpenAPIRenderer,
        renderers.SwaggerUIRenderer
    ]

    def get(self, request):
        generator = MySchemaGenerator(
            title="Sentinel API",
            # url=url,
            # patterns=patterns,
            # urlconf=urlconf
        )
        schema = generator.get_schema(request=request)

        if not schema:
            raise exceptions.ValidationError(
                'The schema generator did not return a schema Document'
            )

        return Response(schema)


def send_webhook_transaction(model, action, data, system="pulso"):
    """ Función que envía información de pulso al endpoint 
        de WebHookTransaction. """
    transaction = {
        "system": system,
        "action": action,
        "model": model,
        "data": data
    }
    url = settings.HOST + \
        reverse('webhook_transaction_api-list', kwargs={'version': 'v1'})
    response = requests.post(url=url, json=transaction,
                             headers={
                                 'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'},
                             verify=False)
    # print(response.text)
    return response


def home(request):
    return render(request, "home.html")



@xframe_options_exempt
def factibilizador_matrix(request):
    return render(request, "admin/factibilizador/factibilizadorIframeAdmin.html")


def replicador_operador(instance, method):
    url_matrix = global_preferences['matrix_url'] + 'api/v1/operators/'
    url_sentinel = global_preferences['sentinel_url'] + 'api/v1/operator/'
    url_iris = global_preferences['iris_url'] + 'api/v1/operators/'

    urls = [url_matrix, url_sentinel, url_iris]

    for url in urls:
        if url == url_iris:
            data = {
                'name': instance.name,
                'company': instance.enterprise_set.first()
            }
        else:
            data = {
                'name': instance.name,
                'code': instance.code
            }
        for url in urls:
            if method == 'create':
                res = requests.post(url, data=data, verify=False)
            if method == 'update':
                res = requests.patch(url, data=data, verify=False)
            if method == 'delete':
                res = requests.delete(url, data=data, verify=False)
    
    return res


def replicador_enterprise(instance, method):

    url_matrix = global_preferences['matrix_url'] + 'api/v1/company/'
    url_sentinel = global_preferences['sentinel_url'] + 'api/v1/enterprise/'
    url_iris = global_preferences['iris_url'] + 'api/v1/company/'

    urls = [url_matrix, url_sentinel, url_iris]

    for url in urls:
        if url == url_iris:
            data = {
                'name': instance.name,
            }
        else:
            data = {
                'name': instance.name,
                'country_id': instance.country.id
            }
        if method == 'create':
            res = requests.post(url, data=data, verify=False)
        if method == 'update':
            res = requests.patch(url, data=data, verify=False)
        if method == 'delete':
            res = requests.delete(url, data=data, verify=False)
    return res
