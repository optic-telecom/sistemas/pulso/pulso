App.setPageTitle('Pulso Admin | Direcciones');
App.restartGlobalFunction();

var conf = {
        headers:{'Authorization':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'},
    }

getRegister=function(street_location){
    
    axios.get(`/api/v1/search-complete-location/${street_location}/`, conf).then((response)=>{
        if(response.status==200){
            var $data=response.data;
            var table = $("#complete_location_table").DataTable({
                responsive: true,
                data:$data,
                destroy:true,
                language:{
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ direcciones",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando direcciones del _START_ al _END_ de un total de _TOTAL_ direcciones",
                    "sInfoEmpty":      "Mostrando direcciones del 0 al 0 de un total de 0 direcciones",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ direcciones)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                columns:[                 
                    {data:'edification_type'},
                    {data:'street_location'},
                    {data:'departament'},
                    {
                    data:null,
                    orderable:false,
                    searchable:false,
                    render: function(obj){ 
                        
                        return `<button 
                                    class="btn btn-sm btn-inverse" 
                                    onclick="loadModal('${obj.country}',
                                                        '${obj.region}',
                                                        '${obj.commune}',
                                                        '${obj.street}',
                                                        '${obj.edification_type}',
                                                        '${obj.number}',
                                                        '${obj.departament}',
                                                        '${obj.street_location}')"
                                    title="Detalle de la dirección">
                                        <i class="fas fa-eye"></i>
                                </button>
                                `; 
                        }
                    },
                ],
                columnDefs: [],

            });

        }
    })

}

loadModal = function(country,region,comuna,calle,e_t,number,departament,street_location){
    var obj = {
        country:country,
        region:region,
        commune:comuna,
        street:calle,
        edification_type:e_t,
        number:number,
        departament:departament,
        street_location:street_location,
    }

    var content = tableDetail(obj);
    var footer = modalFooter()
    var modal = modalBody('Detalle de dirección',content,footer)
    $("#modal_create").empty().append(modal).modal({'show':true})

}

getSelect2=function(path,idInput,placeholder="Región"){
    axios.get(path,conf).then((response)=>{
        if(response.status==200)
        {
            var data = response.data;
            data = data.map((obj)=>{
                return {id:obj.id,text:obj.name || obj.street_no}
            });

            $(idInput).empty().html(`<option value="">${placeholder}</option>`)
            
            $(idInput).select2({
                width:'100%',
                placeholder:placeholder,
                data:data,
            })

        }
    })
}

var path_region = '/api/v1/search-region/';
var path_commune = null;
var path_street = null;
var path_street_location = null;

$("#region,#comuna,#calle,#ubicacion").select2();

getSelect2(path_region,"#region","Región")
$("#region").on('change',function(){
    var region_id = $(this).val();
    if (region_id != ""){
        path_commune = `/api/v1/search-commune/${region_id}`; 
        getSelect2(path_commune,"#comuna","Comuna");
        $("#comuna").on('change',function(){
            var comuna_id = $(this).val();
            if(comuna_id != ''){

                path_street = `/api/v1/search-street/${comuna_id}`; 
                getSelect2(path_street,"#calle","Calle");
                $("#calle").on('change',function(){
                    var street_id = $(this).val();
                    if(street_id != ''){
                        path_street_location = `/api/v1/search-street-location/${street_id}`; 
                        getSelect2(path_street_location,"#ubicacion","Nro");
                        $("#ubicacion").on('change',function(){
                            var u_id = $(this).val();
                            if(u_id != ""){
                                getRegister($(this).val())
                            }    
                        });
                    }
                });

            }


        });

    }

});

reset=function(){
    $("#region,#comuna,#calle,#ubicacion").val('').trigger('change')
    $("#complete_location_table tbody").empty();
}

modalFooter=function(){
    return (`<div class="modal-footer">
            <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Cerrar</a>
            </div>`)
}
modalBody=function(title,content="",footer=null){
    return (`
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title">${title}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
            
            ${content}

            </div>
            ${footer?footer:''}
        </div>
    </div>`)
}

tableDetail=function(obj){
    return (`<table class="table table-stripped" style="width:100%">
                <tr>
                    <th>País</th><td>${obj.country}</td>
                </tr>
                <tr>
                    <th>Región</th><td>${obj.region}</td>
                </tr>
                <tr>
                    <th>Comuna</th><td>${obj.commune}</td>
                </tr>
                <tr>
                    <th>Calle</th><td>${obj.street}</td>
                </tr>
                <tr>
                    <th>Tipo de edificación</th><td>${obj.edification_type}</td>
                </tr>
                <tr>
                    <th>Número de edificación</th><td>${obj.number}</td>
                </tr>
                <tr>
                    <th>Departamento</th><td>${obj.departament?obj.departament:obj.number}</td>
                </tr>
                <tr>
                    <th>Dirección completa</th><td>${obj.street_location}</td>
                </tr>

            </table>`)
}


