

// Carga de Js para Selectores



var selects = new SPA();


var content = selects.Select('Operador','operator')
content += selects.Select('Region','region')
content += selects.Select('Comuna','commune')
content += selects.Select('Calle','street')
content += selects.Select('Nro de Calle','street_location')
content += selects.Select('Nro de Casa o Dpto.','complete_location')


$("#selectores").empty().append(content);
$("#operator,#region,#commune,#street,#street_location,#complete_location").select2({width:'100%'})


selects.Select2AjaxWithoutModal('/operator-ac/','operator','Operador',-1);

url_region = `/region-ac/`;
selects.Select2AjaxWithoutModal(url_region,'region','Region');

$("#region").on('change',function(){
    var region = $("#region").val();
    url_commune = `/commune-ac/${region}/`;
    selects.Select2AjaxWithoutModal(url_commune,'commune','Comuna');

    $("#commune").on('change',function(){
        var commune = $("#commune").val();
        url_street = `/street-ac/${commune}/`;
        selects.Select2AjaxWithoutModal(url_street,'street','Calle');

        $("#street").on('change',function(){
            var street = $("#street").val();
            url_street_location = `/street-location-ac/${street}/`;
            selects.Select2AjaxWithoutModal(url_street_location,'street_location','Nro de calle');

            $("#street_location").on('change',function(){
                var street_location = $("#street_location").val();
                url_complete_location = `/complete-location-ac/${street_location}/`;
                selects.Select2AjaxWithoutModal(url_complete_location,'complete_location','Nro Casa o Dpto', -1);

                $("#complete_location").on('change',function(){

                    var complete_location = $(this).val();

                })
            })
        })
    })

})

function reset_select(){
    $("#operator,#region,#commune,#street,#street_location,#complete_location")
    .val('')
    .trigger('change');
}

function SeparateNumber(val, param = 'entero') {
    // Separa con puntos y como los números en el datatable
    if (param != 'entero') {
        decimal = 2
    } else {
        decimal = 0
    }
    val = val.toFixed(decimal).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }
    return val
}

function tmp_planes(obj){
    // Para limpiar datos nulos aplico los siguientes condcionales a la tabla
    obj.price = obj.price ? SeparateNumber(obj.price) : 0
    obj.promotional_price = obj.promotional_price ? SeparateNumber(obj.promotional_price) : 0
    obj.availability_price = obj.availability_price ? SeparateNumber(obj.availability_price) : 0
    obj.promotional_availability_price = obj.promotional_availability_price ? SeparateNumber(obj.promotional_availability_price) : 0
    obj.upload_speed = obj.upload_speed ? obj.upload_speed : 0
    obj.download_speed = obj.download_speed ? obj.download_speed : 0

    var tmp = `<tr>
                <td>${obj.name}</td> 
                <td class="text-center">${obj.tradename}</td> 
                <td class="text-center">$${obj.price}</td>
                <td class="text-center">$${obj.promotional_price}</td>
                <td class="text-center">$${obj.availability_price}</td>
                <td class="text-center">$${obj.promotional_availability_price}</td>
                <td class="text-center">${obj.upload_speed} Megas</td>
                <td class="text-center">${obj.download_speed} Megas</td>
            </tr>`;

    return tmp;
}


function config(params){
    return {
        headers:{
            'Authorization':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
        },
        params:params,
    }
}

get_factibility = () => {
    var headers = {'Authorization':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    var address = $("#complete_location").val();
    var street_location = $("#street_location").val();
    var operator = $("#operator").val();

    if(operator==''||operator==null){
        swal('Error','Debe seleccionar un operador','error');
        return false;
    }
    else if (street_location=='')
    {
        swal('Error','Debe seleccionar al menos un Nro de calle','error');
        return false;
    }
    else if(address=='')
    {

        var params = {
            street_location:street_location,
            operator:operator
        }
        
    }else
    {
        var params = {
            address:address,
            street_location:street_location,
            operator:operator
        }
    }

    if (params){

        var conf = config(params);
        axios.get('/api/v1/factibility/',conf).then((response)=>{
            if(response.status == 200){
                var $data = response.data;

            

                // $("#factibility").text($data.factibility);
                $("#address").text($data.address);
                $("#node").text($data.node);
                $("#plans").empty();
                

                if(($data.factibility == 'SI')){
                    var $planes = $data.plans;

					$("#factibility").text($data.factibility).removeClass('text-info text-danger text-success').addClass('text-success');
					swal("Excelente","Si se cuenta con factibilidad en esta dirección","success")
                
                    for(i=0;i<$planes.length;i++){
                        $("#plans").append(tmp_planes($planes[i]));
                    }
					

                }else if(($data.factibility=='PROXIMAMENTE')){
					$("#factibility").text($data.factibility).removeClass('text-primary text-danger text-success').addClass('text-primary');
					
					swal("Proximamente","Se va contar con factibilidad en esta dirección.","info")
                    var $planes = $data.plans;
                
                    for(i=0;i<$planes.length;i++){
                        $("#plans").append(tmp_planes($planes[i]));
                    }

                }else{
                    reset_select();
					$("#factibility").text($data.factibility).removeClass('text-primary text-info text-success').addClass('text-danger');
                    $("#plans").empty();
                    swal("No, Lo sentimos","No se cuenta con factibilidad en esta dirección","error")
                }
                
            }
        })
        .catch((error)=>{
            console.log(error)
        })
    }

}

