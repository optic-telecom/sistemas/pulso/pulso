# Generated by Django 2.2.3 on 2019-09-04 11:48

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import simple_history.models
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('places', '0008_auto_20190902_1502'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('taggit', '0002_auto_20150616_2121'),
        ('models', '0007_auto_20190904_1111'),
    ]

    operations = [
        migrations.CreateModel(
            name='Technician',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('status_field', models.BooleanField(default=True)),
                ('id_data', models.UUIDField(blank=True, db_index=True, null=True)),
                ('name', models.CharField(max_length=255)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ['-name'],
            },
        ),
        migrations.RemoveField(
            model_name='node',
            name='plan_order_kinds',
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('status_field', models.BooleanField(default=True)),
                ('id_data', models.UUIDField(blank=True, db_index=True, null=True)),
                ('number', models.PositiveIntegerField(unique=True)),
                ('document_type', models.PositiveSmallIntegerField(choices=[(1, 'boleta'), (2, 'factura'), (3, 'nota de venta')], default=1)),
                ('price_override', models.DecimalField(blank=True, decimal_places=2, max_digits=12, null=True)),
                ('due_day', models.PositiveSmallIntegerField(default=5)),
                ('activated_on', models.DateTimeField(blank=True, null=True)),
                ('installed_on', models.DateTimeField(blank=True, null=True)),
                ('uninstalled_on', models.DateTimeField(blank=True, null=True)),
                ('activation_fee', models.DecimalField(decimal_places=2, default=0, max_digits=12)),
                ('expired_on', models.DateTimeField(blank=True, null=True)),
                ('status', models.PositiveSmallIntegerField(choices=[(1, 'active'), (2, 'pending'), (3, 'inactive'), (4, 'not installed'), (5, 'exchange'), (6, 'install rejected'), (7, 'off-plan sale'), (8, 'delinquent'), (9, 'to reschedule'), (10, 'holder change'), (11, 'discarded'), (12, 'on hold'), (13, 'lost, could not be retired')], default=4)),
                ('network_mismatch', models.BooleanField(default=False)),
                ('notes', models.TextField(blank=True, null=True)),
                ('billing_notes', models.TextField(blank=True, null=True)),
                ('network_notes', models.TextField(blank=True, null=True)),
                ('internal_notes', models.TextField(blank=True, null=True)),
                ('ssid', models.CharField(blank=True, max_length=255, null=True)),
                ('ssid_5g', models.CharField(blank=True, max_length=255, null=True)),
                ('password', models.CharField(blank=True, max_length=255, null=True)),
                ('technology_kind', models.PositiveSmallIntegerField(choices=[(1, 'UTP'), (2, 'GPON'), (3, 'GEPON'), (4, 'antenna')], default=1)),
                ('allow_auto_cut', models.BooleanField(default=True)),
                ('seen_connected', models.NullBooleanField(default=None)),
                ('mac_onu', models.CharField(blank=True, max_length=17, null=True)),
                ('complete_direction', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='places.CompleteDirection')),
                ('connection_node', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='connection_node', to='models.Node')),
                ('customer', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='models.Customer')),
                ('node_mismatch', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='node_mismatch', to='models.Node')),
                ('operator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='models.Operator')),
                ('original_node', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='original_node', to='models.Node')),
                ('plan', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='models.Plan')),
                ('promos', models.ManyToManyField(blank=True, to='models.Promo')),
                ('seller', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='models.Seller')),
                ('street_direction', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='places.StreetDirection')),
                ('tags', taggit.managers.TaggableManager(blank=True, help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags')),
                ('technician', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='models.Technician')),
            ],
            options={
                'ordering': ['-number'],
            },
        ),
        migrations.CreateModel(
            name='HistoricalTechnician',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('created', models.DateTimeField(blank=True, editable=False)),
                ('modified', models.DateTimeField(blank=True, editable=False)),
                ('status_field', models.BooleanField(default=True)),
                ('id_data', models.UUIDField(blank=True, db_index=True, null=True)),
                ('name', models.CharField(max_length=255)),
                ('active', models.BooleanField(default=True)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical technician',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalService',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('created', models.DateTimeField(blank=True, editable=False)),
                ('modified', models.DateTimeField(blank=True, editable=False)),
                ('status_field', models.BooleanField(default=True)),
                ('id_data', models.UUIDField(blank=True, db_index=True, null=True)),
                ('number', models.PositiveIntegerField(db_index=True)),
                ('document_type', models.PositiveSmallIntegerField(choices=[(1, 'boleta'), (2, 'factura'), (3, 'nota de venta')], default=1)),
                ('price_override', models.DecimalField(blank=True, decimal_places=2, max_digits=12, null=True)),
                ('due_day', models.PositiveSmallIntegerField(default=5)),
                ('activated_on', models.DateTimeField(blank=True, null=True)),
                ('installed_on', models.DateTimeField(blank=True, null=True)),
                ('uninstalled_on', models.DateTimeField(blank=True, null=True)),
                ('activation_fee', models.DecimalField(decimal_places=2, default=0, max_digits=12)),
                ('expired_on', models.DateTimeField(blank=True, null=True)),
                ('status', models.PositiveSmallIntegerField(choices=[(1, 'active'), (2, 'pending'), (3, 'inactive'), (4, 'not installed'), (5, 'exchange'), (6, 'install rejected'), (7, 'off-plan sale'), (8, 'delinquent'), (9, 'to reschedule'), (10, 'holder change'), (11, 'discarded'), (12, 'on hold'), (13, 'lost, could not be retired')], default=4)),
                ('network_mismatch', models.BooleanField(default=False)),
                ('notes', models.TextField(blank=True, null=True)),
                ('billing_notes', models.TextField(blank=True, null=True)),
                ('network_notes', models.TextField(blank=True, null=True)),
                ('internal_notes', models.TextField(blank=True, null=True)),
                ('ssid', models.CharField(blank=True, max_length=255, null=True)),
                ('ssid_5g', models.CharField(blank=True, max_length=255, null=True)),
                ('password', models.CharField(blank=True, max_length=255, null=True)),
                ('technology_kind', models.PositiveSmallIntegerField(choices=[(1, 'UTP'), (2, 'GPON'), (3, 'GEPON'), (4, 'antenna')], default=1)),
                ('allow_auto_cut', models.BooleanField(default=True)),
                ('seen_connected', models.NullBooleanField(default=None)),
                ('mac_onu', models.CharField(blank=True, max_length=17, null=True)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('complete_direction', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='places.CompleteDirection')),
                ('connection_node', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='models.Node')),
                ('customer', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='models.Customer')),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('node_mismatch', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='models.Node')),
                ('operator', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='models.Operator')),
                ('original_node', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='models.Node')),
                ('plan', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='models.Plan')),
                ('seller', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='models.Seller')),
                ('street_direction', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='places.StreetDirection')),
                ('technician', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='models.Technician')),
            ],
            options={
                'verbose_name': 'historical service',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
    ]
