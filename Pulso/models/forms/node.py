from django import forms
from models.models import Node
from places.models import (
    Region,
    Commune,
    Street
)
from dal import autocomplete


class NodeForm(forms.ModelForm):

    region = forms.ModelChoiceField(
        required=False,
        queryset=Region.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='region-ac',
            attrs={
                'data-placeholder': 'Región',
                'style': 'width:100%',
                'class': 'form-control form-control-sm'
                },
        ))
    commune = forms.ModelChoiceField(
        required=False,
        queryset=Commune.objects.all(),
        widget=autocomplete.ModelSelect2(
            forward=['region'],
            url='commune-ac',
            attrs={
                'data-placeholder': 'Comuna',
                'style': 'width:100%',
                'class': 'form-control form-control-sm'
                },
        ))
    street = forms.ModelChoiceField(
        required=False,
        queryset=Street.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='street-ac',
            forward=['region', 'commune'],
            attrs={
                'data-placeholder': 'Calle',
                'style': 'width:100%',
                'class': 'form-control form-control-sm'
                },
        ))

    class Meta:
        model = Node
        fields = [
            'id', 'code', 'status',
            'region', 'commune', 'street',
            'street_location',
            'complete_location', 'phone', 'parent',
            'default_connection_node',
            # 'olt_config',
            'is_optical_fiber',
            'gpon', 'gepon', 'pon_port',
            # 'expected_power',
            'fiber_notes',
            'mikrotik_ip', 'mikrotik_username', 'mikrotik_password',
            'notes', 'antennas_left',
            # 'ip_range',
            'no_address_binding', 'plans',
            # 'plan_order_kinds',
            # 'devices',
            # 'devices_cpes'
        ]

        widgets = {
            'street_location': autocomplete.ModelSelect2(
                url='streetlocation-ac',
                forward=['region', 'commune', 'street'],
                attrs={
                    'class': 'form-control',
                    'style': 'width:100%',
                    'data-placeholder': 'Ubicación de calle',
                }
            ),
            'complete_location': autocomplete.ModelSelect2(
                url='completelocation-ac',
                forward=['street_location'],
                attrs={
                    'class': 'form-control',
                    'style': 'width:100%',
                    'data-placeholder': 'Dirección completa',
                }
            ),
        }

    class Media:
        css = {
            'all': []
        }

        js = ['/static/admin/js/vendor/jquery/jquery.js', ]
