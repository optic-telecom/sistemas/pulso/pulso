from django import forms
from models.models import FutureAvailability
from places.models import (
    Region,
    Commune,
    Street,
    StreetLocation
)
from dal import autocomplete


class FutureAvailabilityForm(forms.ModelForm):

    region = forms.ModelChoiceField(
        required=False,
        queryset=Region.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='region-ac',
            attrs={
                'data-placeholder': 'Región',
                'style': 'width:100%',
                'class': 'form-control form-control-sm'
                },
        ))

    commune = forms.ModelChoiceField(
        required=False,
        queryset=Commune.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='commune-ac',
            forward=['region'],
            attrs={
                'data-placeholder': 'Comuna',
                'style': 'width:100%',
                'class': 'form-control form-control-sm'
                },
        ))

    street = forms.ModelChoiceField(
        required=False,
        queryset=Street.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='street-ac',
            forward=['region', 'commune'],
            attrs={
                'data-placeholder': 'Calle',
                'style': 'width:100%',
                'class': 'form-control form-control-sm'
                },
        ))

    street_location = forms.ModelChoiceField(
        required=False,
        queryset=StreetLocation.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='streetlocation-ac',
            forward=['region', 'commune', 'street'],
            attrs={
                'data-placeholder': 'Ubicación de calle',
                'style': 'width:100%',
                'class': 'form-control form-control-sm'
                },
        ))

    class Meta:
        model = FutureAvailability
        fields = (
            'id',
            'region',
            'commune',
            'street',
            'street_location',
            'address',
            'availability_on'
            )
        widgets = {
            'address': autocomplete.ModelSelect2Multiple(
                url='completelocation-ac',
                forward=['street_location'],
                attrs={
                    'class': 'form-control',
                    'style': 'width:100%',
                    'data-placeholder': 'Dirección completa',
                    }),
        }

    class Media:
        css = {
            'all': []
        }

        js = ['/static/admin/js/vendor/jquery/jquery.js', ]
