from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from models.serializers import (TechnicianSerializer)
from models.models import Technician
from places.apis.internals.base_viewset import BaseViewSet

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


class TechnicianViewSet(BaseViewSet):
    """
    create:
        End point for Technician
    retrieve:
        End point for Technician
    update:
        End point for Technician
    delete:
        End point for Technician
    """
    queryset = Technician.objects.all()
    serializer_class = TechnicianSerializer
    model = 'technician'


