from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from models.serializers import (AvailabilitySerializer)
from models.models import Availability
from utils.permissions import IsSystemInternalPermission

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@method_decorator(cache_page(CACHE_TTL), name='dispatch')
class AvailabilityViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Availability
    retrieve:
        End point for Availability
    update:
        End point for Availability
    delete:
        End point for Availability
    """
    queryset = Availability.objects.all()
    serializer_class = AvailabilitySerializer
    permission_classes = (IsSystemInternalPermission | IsAuthenticated,)
