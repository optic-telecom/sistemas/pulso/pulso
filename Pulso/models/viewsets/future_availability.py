from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from models.serializers import (FutureAvailabilitySerializer)
from models.models import FutureAvailability
from utils.permissions import IsSystemInternalPermission

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@method_decorator(cache_page(CACHE_TTL), name='dispatch')
class FutureAvailabilityViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for FutureAvailability
    retrieve:
        End point for FutureAvailability
    update:
        End point for FutureAvailability
    delete:
        End point for FutureAvailability
    """
    queryset = FutureAvailability.objects.all()
    serializer_class = FutureAvailabilitySerializer
    permission_classes = (IsSystemInternalPermission | IsAuthenticated,)
