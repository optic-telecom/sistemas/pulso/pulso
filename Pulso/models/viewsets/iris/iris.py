from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from django.http.request import QueryDict
from multifiberpy.permissions import IsSystemInternalPermission

from models.models import Technician

from models.serializers import TechnicianSerializer


class IrisModelViewSet(ViewSet):

    permission_classes = (IsSystemInternalPermission | IsAuthenticated,)

    def service_orders(
            self,
            request: QueryDict,
            *args: list,
            **kwargs: dict
            ) -> Response:
        data = request.POST
        try:
            data["street_location"]
        except KeyError:
            return Response({
                "sError": "street_location es requerido."
            }, status=400)
        else:
            street_location = data["street_location"]

        try:
            tech = Technician.objects.get(
                node__street_location__pk=street_location
            )
        except Technician.DoesNotExist:
            serializer = {
                'sError': "No existe un Técnico para esa dirección."
            }
            status = 404
        except Technician.MultipleObjectsReturned:
            serializer = {
                "sError": "Existen múltiples técnicos para esa dirección."
            }
            status = 400
        else:
            status = 200
            serializer = TechnicianSerializer(
                tech, many=False
            ).data
        finally:
            return Response(serializer, status=status)