from .iris import IrisModelViewSet

__all__ = [
    "IrisModelViewSet",
]