from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from models.serializers import (ServiceOrderSerializer)
from models.models import ServiceOrder
from places.apis.internals.base_viewset import BaseViewSet

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


class ServiceOrderViewSet(BaseViewSet):
    """
    create:
        End point for ServiceOrder
    retrieve:
        End point for ServiceOrder
    update:
        End point for ServiceOrder
    delete:
        End point for ServiceOrder
    """
    queryset = ServiceOrder.objects.all()
    serializer_class = ServiceOrderSerializer
    model = 'serviceorder'


