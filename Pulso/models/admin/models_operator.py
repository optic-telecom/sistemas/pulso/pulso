from django.contrib import admin
from utils.admin import BaseAdmin
from models.models import Operator
from utils.views import replicador_operador


@admin.register(Operator)
class OperatorAdmin(BaseAdmin):
    list_display = ('id', 'name', 'code')
    search_fields = ('name', 'code',)

    # Al guardar un registro
    def save_model(self, request, obj, form, change):
        # Ejecutamos el replicador
        print('Replicador')
        if change:
            res = replicador_operador(obj, 'update')
        else:
            res = replicador_operador(obj, 'create')
        print(res)
        super().save_model(request, obj, form, change)

    # Al eliminar un registro:
    def delete_model(self, request, obj):
        # Ejecutamos el replicador
        print('Replicador')
        res = replicador_operador(obj, 'delete')
        print(res)
        super().delete_model(request, obj)

    # Al eliminar varios registros:
    def delete_queryset(self, request, queryset):
        # Ejecutamos el replicador
        print('Replicador')
        for obj in queryset:
            res = replicador_operador(obj, 'delete')
            print(res)
        super().delete_queryset(request, queryset)
