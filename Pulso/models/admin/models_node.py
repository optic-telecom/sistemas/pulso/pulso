from django.contrib import admin
from utils.admin import BaseAdmin
from models.models import Node
from models.forms import NodeForm
from dynamic_preferences.registries import global_preferences_registry
global_preferences = global_preferences_registry.manager()


@admin.register(Node)
class NodeAdmin(BaseAdmin):
    form = NodeForm
    list_display = (
        'id',
        'code',
        'status',
        'complete_location',
        # 'phone', 'parent','default_connection_node',
        # 'olt_config',
        'is_optical_fiber',
        'gpon',
        'gepon',
        # 'pon_port', 'expected_power',
        'fiber_notes',
        'mikrotik_ip',
        'mikrotik_username',
        'mikrotik_password',
        # 'notes',
        'antennas_left',
        # 'ip_range',
        'no_address_binding',
        # 'plans', 'plan_order_kinds',
        # 'devices',
        # 'devices_cpes'
        )
    list_filter = (
        'complete_location__street_location__street__commune__region',
        )
    search_fields = (
        'code',
        'status',
        'complete_location__street_location__street__name',
        'complete_location__street_location__street__commune__name',
        'complete_location__street_location__street__commune__region__name',
                    )
    # list_filter = ('code', 'status')
