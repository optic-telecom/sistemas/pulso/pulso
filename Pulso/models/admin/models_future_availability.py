from django.contrib import admin
from utils.admin import BaseAdmin
from django.db.models import Q
from django.contrib import messages
from models.models import FutureAvailability, Availability
from models.forms import FutureAvailabilityForm
from dynamic_preferences.registries import global_preferences_registry
global_preferences = global_preferences_registry.manager()


def send_to_availability(modeladmin, request, queryset):
    queryset = queryset.select_related(
        "node"
    ).prefetch_related(
        "plans", "address"
    ).exclude(
        Q(node__isnull=True)
    )
    print(queryset.count())
    # recorremos los elementos seleccionados
    for qs in queryset:
        # OPCIONES
        # llamamos al existente
        # creamos el habilitado si no existe, o
        # si hay varios, garantizamos que sea el mas antiguo
        try:
            availability, created = Availability.objects.get_or_create(
                node=qs.node)
        except Availability.MultipleObjectsReturned:
            availability = Availability.objects.filter(
                node=qs.node
                ).order_by(
                    "-id"
                ).last()
        except Exception as e:
            print(str(e))

        for q in qs.address.all():
            # agregamos direcciones al habilitado
            availability.address.add(q)
        availability.save()
        # eliminamos el registro de los habilitados
        qs.delete()
    message = """
        El o los registro(s) seleccionado(s)
        fueron enviados a la sección de habilitados.
            """
    messages.add_message(request, messages.SUCCESS, message)



send_to_availability.short_description = "Mover a Factibilidad"


@admin.register(FutureAvailability)
class FutureAvailabilityAdmin(BaseAdmin):
    form = FutureAvailabilityForm
    list_display = (
        'id', 'node', 'availability_on',
        'get_address_count', 'get_planes_count'
        )
    readonly_fields = ('id',)
    actions = ['habilitar_direcciones', send_to_availability]
    search_fields = (
                    'address__street_location__street__name',
                    'address__street_location__street__commune__name',
                    'address__street_location__street__commune__region__name',
                    )
    fieldsets = (
        ('Datos Generales', {
            # 'classes': ('collapse',),
            'fields': ('id',)
        }),
        ('Nodo', {
            'classes': ('collapse',),
            'fields': ('node',)
        }),
        ('Planes', {
            'classes': ('collapse',),
            'fields': ('plans',)
        }),
        ('Ubicación', {
            'classes': ('collapse',),
            'fields': (
                'region', 'commune', 'street', 'street_location', 'address'
                )
        }),
        ('Activación', {
            'classes': ('collapse',),
            'fields': ('availability_on',)
        }),

    )

    def get_address_count(self, obj):
        return f'Direcciones por habilitar. {obj.address.count()}'
    get_address_count.short_description = 'Direcciones'

    def get_planes_count(self, obj):
        return f'Nro de planes. {obj.plans.count()}'
    get_planes_count.short_description = 'Planes'
