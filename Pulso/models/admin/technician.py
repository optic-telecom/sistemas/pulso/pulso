from django.contrib import admin
from utils.admin import BaseAdmin
from models.models import Technician


@admin.register(Technician)
class TechnicianAdmin(BaseAdmin):
    list_display = ("name", "node")