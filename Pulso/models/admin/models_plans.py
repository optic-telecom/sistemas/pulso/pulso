from django.contrib import admin
from utils.admin import BaseAdmin
from models.models import Plan


@admin.register(Plan)
class PlanAdmin(BaseAdmin):
    list_display = [
        'name',
        'tradename',
        'price',
        'promotional_price',
        'availability_price',
        'promotional_availability_price',
        'apparence_order',
        'get_operator',
        'get_category',
        'active',
        'uf',
        'download_speed',
        'upload_speed',
        ]
    search_fields = (
        'name',
        'tradename',
        )
    list_filter = (
        'operator',
        'category',
        )
    list_editable = (
        'tradename',
        'apparence_order',
        'price',
        'promotional_price',
        'availability_price',
        'promotional_availability_price',
        'download_speed',
        'upload_speed'
        )
