from django.contrib import admin
from utils.admin import BaseAdmin
from django.contrib import messages
from models.models import Availability, FutureAvailability
from models.forms import AvailabilityForm

from dynamic_preferences.registries import global_preferences_registry
global_preferences = global_preferences_registry.manager()


def send_to_future_availability(modeladmin, request, queryset):
    queryset = queryset.select_related(
        "node"
    ).prefetch_related(
        "address"
    )
    # recorremos los elementos seleccionados
    for qs in queryset:
        # OPCIONES
        # llamamos al existente
        # creamos el habilitado si no existe, o
        # si hay varios, garantizamos que sea el mas antiguo

        try:
            f_availability, created = FutureAvailability.objects.get_or_create(
                node=qs.node)
        except FutureAvailability.MultipleObjectsReturned:
            f_availability = FutureAvailability.objects.filter(
                node=qs.node
                ).order_by(
                    "-id"
                ).last()
        except Exception as e:
            print(str(e))

        for q in qs.address.all():
            # agregamos direcciones al futuro habilitado
            f_availability.address.add(q)
        f_availability.save()
        # eliminamos el registro de los futuros habilitados
        qs.delete()
    message = """
        El o los registro(s) seleccionado(s)
        fueron enviados a la sección de futuros habilitados.
            """
    messages.add_message(request, messages.SUCCESS, message)


send_to_future_availability.short_description = "Mover a Futura Factibilidad"


@admin.register(Availability)
class AvailabilityAdmin(BaseAdmin):
    form = AvailabilityForm
    list_display = (
        'node', 'node_address', 'get_address_count',
        'get_planes_count'
        )
    list_filter = (
        'node__complete_location__street_location__street__commune__region',
        )
    actions = [send_to_future_availability, ]
    search_fields = (
        'node__code',
        'address__street_location__street__name',
        'address__street_location__street__commune__name',
        'address__street_location__street__commune__region__name',
                    )
    readonly_fields = (
        'id', 'created', 'plans'
        )

    fieldsets = (
        ('Datos Generales', {
            'fields': ('id',)
        }),
        ('Nodo', {
            'classes': ('collapse',),
            'fields': ('node',)
        }),
        ('Direcciones', {
            'classes': ('collapse',),
            'fields': (
                'region', 'commune', 'street', 'street_location', 'address'
                )
        }),
        ('Planes Asociados', {
            'classes': ('collapse',),
            'fields': ('plans',)
        }),
        ('Activación', {
            'classes': ('collapse',),
            'fields': ('created',)
        }),

    )

    def get_address_count(self, obj):
        return f'Direcciones por habilitar. {obj.address.count()}'
    get_address_count.short_description = 'Direcciones'

    def get_planes_count(self, obj):
        return f'Nro de planes. {obj.node.plans.count()}'
    get_planes_count.short_description = 'Planes'
