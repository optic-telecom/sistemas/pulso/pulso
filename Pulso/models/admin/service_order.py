from django.contrib import admin
from utils.admin import BaseAdmin
from models.models import ServiceOrder


@admin.register(ServiceOrder)
class ServiceOrderAdmin(BaseAdmin):
    list_display = ("name", "time_range")