from django.urls import path
from models.viewsets import IrisModelViewSet

app_name = 'apis_to_iris'

urlpatterns = [
        path(
            "iris-service-orders/",
            IrisModelViewSet.as_view({'post': 'service_orders'}),
            name="service_orders"
        )
]