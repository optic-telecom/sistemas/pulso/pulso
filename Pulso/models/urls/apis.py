from rest_framework import routers

from models.viewsets import (
    OperatorViewSet,
    PlanViewSet,
    NodeViewSet,
    AvailabilityViewSet,
    FutureAvailabilityViewSet,
    TechnicianViewSet,
    ServiceOrderViewSet
    )

router = routers.DefaultRouter()

router.register(r'operator', OperatorViewSet, 'operator_api')
router.register(r'plan', PlanViewSet, 'plan')
router.register(r'node', NodeViewSet, 'node_api')
router.register(r'availability', AvailabilityViewSet, 'availability_api')
router.register(
    r'future-availability',
    FutureAvailabilityViewSet,
    'future_availability_api'
    )

router.register("technicians", TechnicianViewSet, "technicians")
router.register("services-orders", ServiceOrderViewSet, "services_orders")

# for u in router.urls:
#     print(u)
