from django.urls import path, include
from models.urls.apis import router

app_name = 'models'

urlpatterns = [
    path("api/v1/", include('models.urls.iris_viewset')),
]