from django.db import models
from multifiberpy.models import BaseModel
from models.models.node import Node


class TechnicianManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related(
            "node", "service_order"
        )


class Technician(BaseModel):
    name: str = models.CharField(
            max_length=100,
            verbose_name="Tecnico",
            help_text="varchar(100), required*"
        )
    node: int = models.ForeignKey(
            Node,
            blank=True,
            null=True,
            on_delete=models.PROTECT,
            verbose_name="Nodo",
            help_text="int (FK) models.Node"
        )
    service_order: int = models.ForeignKey(
            "models.ServiceOrder",
            on_delete=models.PROTECT,
            blank=True,
            null=True,
            verbose_name="Orden de Servicio",
            help_text="int"
        )
    objects = TechnicianManager()

    class Meta:
        ordering = ["-id"]
        verbose_name = "Técnico"
        verbose_name_plural = "Técnicos"

    def __str__(self):
        return f"{self.name}"

# =========================================================================== #
