from django.db import models
from utils.models import BaseModel
from django.core.validators import MaxValueValidator
from models.models.operator import Operator
from models.helpers import get_plan

INTERNET_HOGAR = 1
INTERNET_EMPRESAS = 2
TELEFONIA_EMPRESAS = 3
RED_EMPRESAS = 4
TELEVISION_HOGAR = 5
CATEGORY_CHOICES = (
    (INTERNET_HOGAR, 'Internet hogar'),
    (INTERNET_EMPRESAS, 'Internet empresas'),
    (TELEFONIA_EMPRESAS, 'Telefonía empresas'),
    (RED_EMPRESAS, 'Red empresas'),
    (TELEVISION_HOGAR, 'Televisión Hogar')
)


class PlanManager(models.Manager):

    def get_queryset(self):
        qs = super().get_queryset().select_related(
            "operator"
        )
        return qs


class Plan(BaseModel):
    """ Información de planes """
    name = models.CharField(max_length=75, unique=True)
    tradename = models.CharField(
        max_length=75, verbose_name="Nombre comercial", null=True, blank=True
        )
    # No requerido por sentinel
    price = models.DecimalField(
        max_digits=12, decimal_places=2, null=True, blank=True
        )
    # No requerido por matrix
    promotional_price = models.DecimalField(
        max_digits=12, decimal_places=2, null=True,
        blank=True, verbose_name="Precio promocial"
        )
    # No requerido por sentinel
    availability_price = models.DecimalField(
        max_digits=12, decimal_places=2, null=True, blank=True
        )
    # No requerido por sentinel
    promotional_availability_price = models.DecimalField(
        max_digits=12, decimal_places=2, null=True, blank=True
        )
    category = models.PositiveSmallIntegerField(
        default=INTERNET_HOGAR, choices=CATEGORY_CHOICES
        )
    uf = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    operator = models.ForeignKey(
        Operator, on_delete=models.SET_NULL, null=True
        )
    apparence_order = models.IntegerField(
        default=1, verbose_name="Orden de aparición"
        )
    # Sentinel
    download_speed = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(4096)], null=True, blank=True
        )
    upload_speed = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(4096)], null=True, blank=True
        )
    aggregation_rate = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(4096)], null=True, blank=True
        )
    id_matrix = models.PositiveIntegerField()
    _plan = None
    # Manager del modelo
    objects = PlanManager()

    def _get_plan(self):
        if(not self._plan):
            res = get_plan(self.id_matrix)
            # print('---------------')
            # pprint.pprint(res)
            if res['ok']:
                self._plan = res['data']
                return True
            return False

        return True

    def __str__(self):
        return ' - '.join([self.name, str(self.price)])

    @property
    def get_operator(self):
        return self.operator.name

    @property
    def get_category(self):
        return self.get_category_display()

    class Meta:
        ordering = ['-id']
