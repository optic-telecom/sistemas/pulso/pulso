from django.db import models
from multifiberpy.models import BaseModel
from models.models.node import Node
from places.models import CompleteLocation


class AvailabilityManager(models.Manager):

    def get_queryset(self):
        qs = super().get_queryset().select_related(
            "node"
            ).prefetch_related(
                "address"
            )
        return qs


class Availability(BaseModel):
    node = models.ForeignKey(Node, on_delete=models.CASCADE)
    # Para edificio/condominio/empresa
    address = models.ManyToManyField(
        CompleteLocation, verbose_name="Dirección completa"
        )
    # Manager del modelo
    objects = AvailabilityManager()

    def __str__(self):
        return f'{self.node.complete_location}'

    def get_node(self):
        return f'{self.node.code}'

    def get_address(self):
        address = self.address.all()
        listado = list()
        for direccion in address:
            if direccion.floor:
                listado.append(
                    {'complete_location': direccion.floor.__str__()}
                    )
            else:
                listado.append(
                    {'complete_location': direccion.street_location.__str__()}
                    )
        return listado

    @property
    def node_address(self):
        node_sl = self.node.street_location
        node_cl = self.node.complete_location
        return node_sl if node_sl else node_cl

    @property
    def plans(self):
        """ Planes relacionados al nodo """
        planes = self.node.plans.order_by('apparence_order')
        return [f'{plan.name} - ${plan.price}' for plan in planes]

    class Meta:
        ordering = ['-id']
