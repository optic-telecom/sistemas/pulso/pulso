from django.db import models
from multifiberpy.models import BaseModel
from models.models.node import Node
from models.models.plan import Plan
from places.models import CompleteLocation


class FutureAvailabilityManager(models.Manager):

    def get_queryset(self):
        qs = super().get_queryset().select_related(
            "node"
            ).prefetch_related(
                "address", "plans"
            )
        return qs


class FutureAvailability(BaseModel):
    """ Edificios proximamente habilitados """
    node = models.ForeignKey(
        Node, blank=True, null=True, on_delete=models.PROTECT,
        verbose_name="Nodo", help_text="Foreign Key (opcional)"
        )
    address = models.ManyToManyField(
        CompleteLocation, verbose_name="Dirección completa"
        )
    availability_on = models.DateTimeField(blank=True, null=True)
    plans = models.ManyToManyField(Plan, blank=True)
    # Manager del modelo
    objects = FutureAvailabilityManager()

    def __str__(self):
        return f"{self.availability_on}"

    def get_plans(self):
        planes = self.plans.all()
        listado = list()
        for plan in planes:
            listado.append({'name': plan.name, 'price': plan.price})
        return listado

    def get_address(self):
        address = self.address.all()
        listado = list()
        for direccion in address:
            if direccion.floor:
                listado.append({
                    'complete_location': direccion.floor.__str__()
                    })
            else:
                listado.append({
                    'complete_location': direccion.street_location.__str__()
                    })
        return listado


    class Meta:
        ordering = ['-id']
