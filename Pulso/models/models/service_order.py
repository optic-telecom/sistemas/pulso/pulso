from django.db import models
from multifiberpy.models import BaseModel


class ServiceOrder(BaseModel):
    name: str = models.CharField(
            max_length=100,
            verbose_name="Tecnico",
            help_text="varchar(100), required*"
        )
    time_range: int = models.SmallIntegerField(
            default=0,
            verbose_name="Tiempo de atención(minutos)",
            help_text="int"
        )

    class Meta:
        ordering = ["-id"]
        verbose_name = "Orden de servicio"
        verbose_name_plural = "Ordenes de servicio"

    def __str__(self):
        return f"{self.name}"

# =========================================================================== #
