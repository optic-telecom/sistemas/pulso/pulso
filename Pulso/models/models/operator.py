from django.db import models
from utils.models import BaseModel
from django.db.models.signals import pre_delete, post_delete
from django.dispatch.dispatcher import receiver
from utils.views import replicador_operador


class Operator(BaseModel):
    """ Información de operadores """
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=3)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-name']


@receiver(post_delete, sender=Operator)
def _operator_delete(sender, instance, **kwargs):
    # Ejecutamos el replicador luego de que se elimine el registro
    print("Replicador")
    res = replicador_operador(instance, 'delete')
    print(res)
