from django.db import models
from multifiberpy.models import BaseModel
from models.models.plan import Plan
from places.models import StreetLocation, CompleteLocation
from models.helpers import get_node


WIRED = 1
PENDING = 2
IN_PROGRESS = 3
NO_LABEL = 4
DISCARDED = 5

STATUS_CHOICES = (
    (WIRED, 'wired'),
    (PENDING, 'pending'),
    (IN_PROGRESS, 'in progress'),
    (NO_LABEL, 'no label'),
    (DISCARDED, 'discarded')
)


class NodeManager(models.Manager):

    def get_queryset(self):
        qs = super().get_queryset().select_related(
            "street_location", "complete_location"
            ).prefetch_related(
                "plans"
            )
        return qs


class Node(BaseModel):
    """ Información de nodos """
    # Matrix
    code = models.CharField(max_length=40)
    status = models.PositiveSmallIntegerField(
        default=WIRED, choices=STATUS_CHOICES
        )
    # Para casa/terreno
    street_location = models.ForeignKey(
        StreetLocation, on_delete=models.SET_NULL, null=True, blank=True
        )
    # Para edificio/condominio/empresa
    complete_location = models.ForeignKey(
        CompleteLocation, on_delete=models.SET_NULL, null=True, blank=True
        )
    phone = models.CharField(max_length=16, blank=True, null=True)
    parent = models.ForeignKey(
        'self', null=True, blank=True, on_delete=models.CASCADE
        )
    default_connection_node = models.ForeignKey(
        'self', null=True, blank=True,
        on_delete=models.CASCADE, related_name='+'
        )
    # olt_config = models.TextField(null=True, blank=True)
    is_optical_fiber = models.BooleanField(default=False)
    gpon = models.BooleanField(default=False)
    gepon = models.BooleanField(default=False)
    pon_port = models.CharField(max_length=255, blank=True, null=True)
    # xpectede_power = models.CharField(max_length=255, blank=True, null=True)
    fiber_notes = models.TextField(blank=True, null=True)
    mikrotik_ip = models.GenericIPAddressField(protocol='IPv4')
    mikrotik_username = models.CharField(max_length=255)
    mikrotik_password = models.CharField(max_length=255)
    notes = models.TextField(blank=True, null=True)
    antennas_left = models.PositiveSmallIntegerField(default=0)
    # ip_range = models.GenericIPAddressField(
    #   protocol='IPv4', unique=True, blank=True, null=True)
    no_address_binding = models.BooleanField(default=False)
    plans = models.ManyToManyField(Plan)

    # Sentinel
    # devices = models.PositiveIntegerField(default=0, blank=True, null=True)
    # devices_cpes = models.PositiveIntegerField(
    #   default=0, blank=True, null=True
    # )
    # id_sentinel = models.PositiveIntegerField(db_index=True,
    #                         default=0,
    #                         editable=settings.DEBUG)

    _node = None
    # Manager del modelo
    objects = NodeManager()

    class Meta:
        ordering = ['code']

    def _get_node(self):
        if(not self._node):
            res = get_node(self.id_sentinel)
            if res['ok']:
                self._node = res['data']
                return True
            return False

        return True

    def __str__(self):
        return self.code
