from models.helpers.request_api import request_api


def get_node(id):
    """ Pendiente con esta url en donde usemos la funcion get_node """

    url = f'https://localhost:8000/api/v1/node/{id}/'
    status_code, node = request_api(id, url)

    if status_code == 200:
        res = {
            'ok': True,
            'data': {
                'id': node['id'],
                'name': node['name'],
                'alias': node['alias'],
                'cpeutps': node['cpeutps'],
                'devices': node['devices'],
                'cpeutp_active': node['cpeutp_active'],
                'expected_power': node['expected_power'],
                'status': node['status'],
                'parent': node['parent'],
                'type': node['type'],
            }
        }

        return res

    else:
        return {
            'ok': True,
            'detail': f'error {status_code}',
            'data': {
                'id': '-',
                'name': '-',
                'alias': '-',
                'cpeutps': '-',
                'devices': '-',
                'cpeutp_active': '-',
                'expected_power': '-',
                'status': '-',
                'parent': '-',
                'type': '-'
            }
        }

