from models.helpers.request_api import request_api


def get_plan(id):
    url = f'https://localhost:8001/api/v1/plans/{id}/'
    status_code, node = request_api(id, url)

    if status_code == 200:
        res = {
            'ok': True,
            'data': {
                'id': node['id'],
                'name': node['name'],
                'price': node['price'],
                'active': node['active'],
                'operator': node['operator']
            }
        }

        return res

    else:
        return {
            'ok': True,
            'detail': f'error {status_code}',
            'data': {
                'id': '-',
                'name': '-',
                'price': '-',
                'active': '-',
                'operator': '-'
            }
        }
