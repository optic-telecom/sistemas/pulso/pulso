import requests


def request_api(id, api_url):
    # api_url = f'http://127.0.0.1:9001/api/v1/complete-location/{id}/'
    r = requests.get(api_url)
    # print(r.text)
    status_code = r.status_code
    object_ = r.json()
    return status_code, object_
    # return 400, {}
