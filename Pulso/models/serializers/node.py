from rest_framework import serializers
from models.models import Node

from places.models.street_location import TYPE_HOUSE, TYPE_TERRAIN


class NodeSerializer(serializers.ModelSerializer):
    """ Serializer de nodos """

    id = serializers.IntegerField(required=False, min_value=1)

    class Meta:
        model = Node
        fields = (
            'id', 'code', 'status', 'street_location',
            'complete_location', 'phone', 'parent',
            'default_connection_node',
            # 'olt_config',
            'is_optical_fiber',
            'gpon', 'gepon', 'pon_port',
            # 'expected_power',
            'fiber_notes',
            'mikrotik_ip', 'mikrotik_username', 'mikrotik_password',
            'notes', 'antennas_left',
            # 'ip_range',
            'no_address_binding',
            # 'plans', 'plan_order_kinds', 'devices', 'devices_cpes')
            'plans',
            # 'devices',
            # 'devices_cpes',
            # 'id_sentinel'
            )

    def is_valid(self, *args, **kwargs):
        """ Valida que si tiene street_location no tiene complete_location, y que tiene
            exactamente uno. """
        if not self.instance:
            complete_location = self.initial_data.get('complete_location')
            street_location = self.initial_data.get('street_location')
        else:
            complete_location = self.initial_data.get(
                'complete_location', self.instance.floor
                )
            street_location = self.initial_data.get(
                'street_location', self.instance.street_location
                )
        if not complete_location and not street_location:
            _err = {
                'error': """Debe especificar una dirección
                            completa o una dirección de calle."""
                }
            raise serializers.ValidationError(_err)
        if complete_location and street_location:
            _err = {
                'error': """
                Si se trata de un edificio, una empresa o un condominio
                especifique la dirección completa,
                si es una casa o un terreno especifique la dirección
                de la calle, pero no ambos.
                """
                }
            raise serializers.ValidationError(_err)
        return super(NodeSerializer, self).is_valid(*args, **kwargs)

    def validate(self, attrs):
        """
        Valida que edification_type de street_location es casa o terreno.
        """
        street_location = attrs.get('street_location', None)
        if street_location:
            sl_type = street_location.edification_type
            if sl_type not in [TYPE_HOUSE, TYPE_TERRAIN]:
                _err = {
                    'error': """
                                La dirección de calle no coincide
                                con una casa o un terreno.
                            """
                    }
                raise serializers.ValidationError(_err)
        return super().validate(attrs)

