from rest_framework import serializers
from models.models import ServiceOrder


class ServiceOrderSerializer(serializers.ModelSerializer):
    """ Serializer de ServiceOrder """
    class Meta:
        model = ServiceOrder
        fields = (
            'id',
            'name',
            'time_range',
            )


