from rest_framework import serializers
from models.models import Plan


class PlanSerializer(serializers.ModelSerializer):
    """ Serializer de planes """

    id = serializers.IntegerField(required=False, min_value=1)

    class Meta:
        model = Plan
        fields = (
            'id', 'name', 'price', 'category', 'uf', 'active', 'operator',
            'download_speed', 'upload_speed', 'aggregation_rate',
            'id_matrix'
            )