from rest_framework import serializers


class FutureAvailabilityListSerializer(serializers.Serializer):
    plans = serializers.JSONField(source="get_plans")
    address = serializers.JSONField(source='get_address')
