from rest_framework import serializers
from models.models import Availability


class AvailabilitySerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        address = validated_data.pop('address', [])
        availability = Availability.objects.create(**validated_data)

        if address:
            for direccion in address:
                availability.address.add(direccion)
            availability.save()

        return availability

    def update(self, instance, validated_data):
        address = validated_data.pop('address', instance.address)

        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()

        if address:
            for direccion in address:
                if direccion not in instance.address.all():
                    instance.address.add(direccion)
                else:
                    instance.address.remove(direccion)

        instance.save()
        return instance

    class Meta:
        model = Availability
        fields = ('id', 'node', 'address')
