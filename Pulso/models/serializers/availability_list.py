from rest_framework import serializers


class AvailabilityListSerializer(serializers.Serializer):
    node = serializers.CharField(source="get_node")
    address = serializers.JSONField(source='get_address')
