from rest_framework import serializers
from models.models import FutureAvailability


class FutureAvailabilitySerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        plans = validated_data.pop('plans', [])
        address = validated_data.pop('address', [])

        f_avialability = FutureAvailability(**validated_data).save()

        for plan in plans:
            f_avialability.plans.add(plan)

        for direccion in address:
            f_avialability.address.add(direccion)
        f_avialability.save()

        return f_avialability

    def update(self, instance, validated_data):
        plans = validated_data.pop('plans', instance.plans)
        address = validated_data.pop('address', instance.address)

        for attr, value in validated_data.items():
            setattr(instance, attr, value)

        if plans:
            for plan in plans:
                if plan not in instance.plans.all():
                    instance.plans.add(plan)
                else:
                    instance.plans.remove(plan)

        instance.save()

        if address:
            for direccion in address:
                if direccion not in instance.address.all():
                    instance.address.add(direccion)
                else:
                    instance.address.remove(direccion)

        instance.save()
        return instance

    class Meta:
        model = FutureAvailability
        fields = ('id', 'address', 'availability_on', 'plans')
