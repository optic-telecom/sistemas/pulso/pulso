from rest_framework import serializers
from models.models import Operator
from utils.views import replicador_operador


class OperatorSerializer(serializers.ModelSerializer):
    """ Serializer de operadores """
    def create(self, validated_data):
        new_obj = super().create(validated_data)
        # Ejecutamos el replicador
        res = replicador_operador(new_obj, 'create')
        print(res)
        # Devolvemos la instancia ya creada
        return new_obj

    def update(self, instance, validated_data):
        updated_obj = super().update(instance, validated_data)
        # Ejecutamos el replicador
        res = replicador_operador(updated_obj, 'update')
        print(res)
        # Devolvemos la instancia ya creada
        return updated_obj
    
    class Meta:
        model = Operator
        fields = ('id', 'name', 'code')
