from rest_framework import serializers
from models.models import Technician, Node, ServiceOrder


class NodeField(serializers.RelatedField):

    def to_representation(self, obj):
        if obj:
            return {
                "id": obj.pk,
                "code": obj.code,
                "street_location": obj.street_location.pk,
                "street_location_address": obj.street_location.__str__(),
            }
        else:
            return "---"

    def to_internal_value(self, data):
        print("internal value")
        print(data)
        try:
            try:
                return Node.objects.get(code=data)
            except KeyError:
                raise serializers.ValidationError(
                    'id is a required field.'
                )
            except ValueError:
                raise serializers.ValidationError(
                    'id must be an integer.'
                )
        except Node.DoesNotExist:
            raise serializers.ValidationError(
                'Node does not exist.'
            )


class ServiceOrderField(serializers.RelatedField):

    def to_representation(self, obj):
        if obj:
            return {
                "id": obj.pk,
                "name": obj.name,
                "time_range": obj.time_range,
            }
        else:
            return "---"

    def to_internal_value(self, data):
        try:
            try:
                return ServiceOrder.objects.get(code=data)
            except KeyError:
                raise serializers.ValidationError(
                    'id is a required field.'
                )
            except ValueError:
                raise serializers.ValidationError(
                    'id must be an integer.'
                )
        except ServiceOrder.DoesNotExist:
            raise serializers.ValidationError(
                'ServiceOrder does not exist.'
            )


class TechnicianSerializer(serializers.ModelSerializer):
    """ Serializer de Technician """
    node = NodeField(queryset=Node.objects.all())
    service_order = ServiceOrderField(queryset=ServiceOrder.objects.all())
    class Meta:
        model = Technician
        fields = (
            'id',
            'name',
            'node',
            'service_order',
            )


