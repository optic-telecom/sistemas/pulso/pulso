from rest_framework.viewsets import ModelViewSet
from qualification.models import Qualification
from qualification.serializers import QualificationSerializer
from rest_framework.permissions import IsAuthenticated
from multifiberpy.permissions import IsSystemInternalPermission


class QualificationViewSet(ModelViewSet):
    """ GET, POST, PUT, PATCH Y DELETE de calificaciones """
    queryset = Qualification.objects.all()
    serializer_class = QualificationSerializer
    permission_classes = [IsSystemInternalPermission | IsAuthenticated, ]