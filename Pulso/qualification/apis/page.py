from rest_framework.viewsets import ModelViewSet
from qualification.models import Page
from qualification.serializers import PageSerializer
from rest_framework.permissions import IsAuthenticated
from multifiberpy.permissions import IsSystemInternalPermission


class PageViewSet(ModelViewSet):
    """ GET, POST, PUT, PATCH, DELETE de Páginas"""
    queryset = Page.objects.all()
    serializer_class = PageSerializer
    permission_classes = [IsSystemInternalPermission | IsAuthenticated, ]