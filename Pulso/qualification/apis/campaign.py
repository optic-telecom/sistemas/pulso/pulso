from rest_framework.viewsets import ModelViewSet
from qualification.models import Campaign
from qualification.serializers import CampaignSerializer
from rest_framework.permissions import IsAuthenticated
from multifiberpy.permissions import IsSystemInternalPermission


class CampaignViewSet(ModelViewSet):
    """
    Estadísticas por Campaña
    """
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer
    permission_classes = [IsSystemInternalPermission | IsAuthenticated, ]