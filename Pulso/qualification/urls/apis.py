from rest_framework.routers import DefaultRouter
from qualification.apis import (
    PageViewSet,
    QualificationViewSet,
    CampaignViewSet
)
router = DefaultRouter()

router.register("pages", PageViewSet, "page")
router.register("qualifications", QualificationViewSet, "qualification")
router.register("campaigns", CampaignViewSet, "campaigns")
