from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db.models import Avg, FloatField, Sum, Count, QuerySet
from django.contrib.auth.models import User
from multifiberpy.models import BaseModel
# Create your models here.


class QualificationManager(models.Manager):
    def get_queryset(self) -> QuerySet:
        return super().get_queryset().select_related(
            "page", "campaign", "created_by"
        )

    def general_numbers(self) -> QuerySet:
        """
        Estádisticas generales de la calificación
        """
        consulta = self.only(
                    "id", "page__page", "start"
                ).values(
                    "page__page", "campaign__name"
                ).annotate(
                    total_estrellas=Sum('start'),
                    total_votos=Count("start"),
                    promedio_votos=Avg("start", output_field=FloatField()),
                ).order_by(
                    "-promedio_votos", "-total_estrellas", "page__page"
                )
        # print(consulta.query)
        return consulta

    def page_numbers(self, page: str) -> QuerySet:
        """
        Estádisticas de la calificación por página
        """
        return self.page_statistics().filter(page__page=page)


class Qualification(BaseModel):
    page: QuerySet = models.ForeignKey(
        "qualification.Page",
        on_delete=models.PROTECT,
        verbose_name="Página *",
        help_text="int required"
    )
    campaign: QuerySet = models.ForeignKey(
        "qualification.Campaign",
        on_delete=models.PROTECT,
        verbose_name="Campaña *",
        help_text="int required",
        blank=True,
        null=True,
    )
    start: int = models.IntegerField(
        default=1,
        verbose_name="Estrella *",
        help_text="int",
        validators=[
            MinValueValidator(1, "Mínimo valor permitido: 1"),
            MaxValueValidator(5, "Máximo valor Permitido: 5")
            ]
    )
    comment: str = models.TextField(
        verbose_name="Comentario",
        help_text="TEXT no required",
        blank=True,
        null=True
    )
    created_by: QuerySet = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        verbose_name="Creado Por",
        help_text="int"
    )

    objects = QualificationManager()

    class Meta:
        ordering = ["-id"]
        verbose_name = "Calificación"
        verbose_name_plural = "Calificaciones"

    def __str__(self):
        return f"{self.page}"
