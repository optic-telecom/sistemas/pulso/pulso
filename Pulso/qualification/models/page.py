from django.db import models
from django.contrib.auth.models import User
from multifiberpy.models import BaseModel
# Create your models here.


class PageManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related(
            "created_by"
        )


class Page(BaseModel):
    page: str = models.URLField(
        verbose_name="Página",
        help_text="varchar(100)",
        blank=True,
        null=True
    )
    description: str = models.TextField(
        verbose_name="Descripción",
        help_text="TEXT no required",
        blank=True,
        null=True
    )
    created_by: models.QuerySet = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        verbose_name="Creado Por *",
        help_text="int"
    )
    objects = PageManager()

    class Meta:
        ordering = ["-id"]
        verbose_name = "Página"
        verbose_name_plural = "Páginas"

    def __str__(self):
        return f"{self.page}"
