from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from multifiberpy.models import BaseModel

# Create your models here.
PERIOD = (
    (0, "I"),
    (1, "II"),
    (2, "III"),
    (3, "IV")
)


class Campaign(BaseModel):
    name: str = models.CharField(
        max_length=50,
        verbose_name="Campaña",
        help_text="varchar(100)",
    )
    period = models.SmallIntegerField(
        choices=PERIOD,
        default=PERIOD[0][0],
        verbose_name="Periodo",
        help_text="int, required"
    )
    description: str = models.TextField(
        verbose_name="Descripción",
        help_text="TEXT no required",
        blank=True,
        null=True
    )
    created_by: models.QuerySet = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        verbose_name="Creado Por *",
        help_text="int"
    )

    class Meta:
        ordering = ["-id"]
        verbose_name = "Campaña"
        verbose_name_plural = "Campañas"

    def save(self, *args, **kwargs):
        date = timezone.now()
        today = date.strftime("%Y")
        if not self.pk:
            period = PERIOD[self.period][1]
            self.name = f"{self.name.upper()}_{period}_{today}"
        else:
            self.name = self.name.upper()
        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.name}"