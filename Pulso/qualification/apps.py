from django.apps import AppConfig


class QualificationConfig(AppConfig):
    name = 'qualification'
    verbose_name = "Calificación a Páginas"
