from django.contrib import admin
from qualification.models import Qualification


@admin.register(Qualification)
class QualificationAdmin(admin.ModelAdmin):
    list_display = ("id", "page", "campaign", "start", "created_by")
    search_fields = ("page", "campaign")
    list_filter = ("campaign", "page")
