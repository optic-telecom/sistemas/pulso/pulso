from django.contrib import admin
from qualification.models import Page


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ("id", "page", "created_by")
    search_fields = ("page", )
