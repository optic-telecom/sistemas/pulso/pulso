from django.contrib import admin
from qualification.models import Campaign


@admin.register(Campaign)
class CampaignAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "created_by")
    search_fields = ("name", )
