from rest_framework import serializers
from qualification.models import Qualification


class QualificationSerializer(serializers.ModelSerializer):

    def validate_start(self, value):
        errors = {}
        if value < 1:
            errors["start"] = "Mínimo valor permitido: 1"
        elif value > 5:
            errors["start"] = "Máximo valor permitido: 5"

        if errors:
            raise serializers.ValidationError(errors["start"])

        return value

    def validate_created_by(self, value):
        if not value:
            raise serializers.ValidationError(
                "El usuario es un campo requerido"
                )
        return value

    def validate_comment(self, value):
        if not value or value == '':
            raise serializers.ValidationError(
                "El comentario es un campo requerido"
                )
        return value

    def validate_page(self, value):
        if not value:
            raise serializers.ValidationError(
                "La página es un campo requerido"
                )
        return value

    class Meta:
        model = Qualification
        fields = (
            "id",
            "page",
            "start",
            "comment",
            "created_by",
        )