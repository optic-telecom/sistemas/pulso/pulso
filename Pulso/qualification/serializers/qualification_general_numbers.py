from rest_framework import serializers


class GeneralNumbersSerializer(serializers.Serializer):
    page = serializers.SerializerMethodField()
    campaign = serializers.SerializerMethodField()
    total_starts = serializers.SerializerMethodField()
    total_votes = serializers.SerializerMethodField()
    mean_votes = serializers.SerializerMethodField()

    def get_page(self, obj):
        return obj["page__page"]

    def get_campaign(self, obj):
        return obj["campaign__name"]

    def get_total_starts(self, obj):
        return obj["total_estrellas"]

    def get_total_votes(self, obj):
        return obj["total_votos"]

    def get_mean_votes(self, obj):
        return obj["promedio_votos"]