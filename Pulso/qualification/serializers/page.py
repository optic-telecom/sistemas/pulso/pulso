from rest_framework import serializers
from qualification.models import Page


class PageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = (
            "id",
            "page",
            "description",
        )