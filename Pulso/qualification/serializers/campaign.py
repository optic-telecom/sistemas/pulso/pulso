from rest_framework import serializers
from qualification.serializers import GeneralNumbersSerializer
from qualification.models import Campaign


class CampaignSerializer(serializers.ModelSerializer):
    period = serializers.SerializerMethodField()
    statistic = serializers.SerializerMethodField()
    owner = serializers.SerializerMethodField()

    def get_period(self, obj):
        return obj.get_period_display()

    def get_statistic(self, obj):
        qs = obj.qualification_set.general_numbers()
        return GeneralNumbersSerializer(qs, many=True).data

    def get_owner(self, obj):
        return obj.created_by.email

    class Meta:
        model = Campaign
        fields = (
            "id",
            "name",
            "period",
            "description",
            "owner",
            "statistic",

        )