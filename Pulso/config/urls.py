import os

from django.contrib import admin
from django.urls import include, path, re_path


from backup.urls import router as router_backups
from backup.urls import urlpatterns as urlpatterns_backup
from models.urls import router as router_models
from places.urls import router as router_places
from users.urls import router as router_users
from users.urls import urlpatterns as urlpatterns_user
from utils.views import SwaggerSchemaView, factibilizador_matrix, home
from webfact.urls import router as router_webfact
from webfact.urls import urlpatterns as urlpatterns_webfact
from webhosts.urls import router as router_webhosts
from qualification.urls.apis import router as qualification_router

# PERSONALIZACIÓN GLOBAL DEL ADMIN 
admin.site.title_header = 'Pulso Admin | Multifiber'
admin.site.site_title = 'Pulso'
admin.site.site_header = 'Pulso Admin | Multifiber'
admin.site.index_title = 'Pulso Admin'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home),
    path('factibilizador_matrix', factibilizador_matrix),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_users.urls)),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_webhosts.urls)),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_places.urls)),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_models.urls)),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_backups.urls)),

    re_path(r'^api/(?P<version>[-\w]+)/', include(router_webfact.urls)),
    re_path(r'^api/(?P<version>[-\w]+)/', include(qualification_router.urls)),
    path('api/docs', SwaggerSchemaView.as_view()),
] + urlpatterns_user + urlpatterns_backup + urlpatterns_webfact

urlpatterns += [
    path("enterprise/", include("enterprise.urls")),
    path("qualification/", include("qualification.urls")),
    path("models/", include("models.urls")),
]
