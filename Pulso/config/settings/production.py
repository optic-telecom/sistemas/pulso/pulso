from .base import *
from os.path import join
from sentry_sdk.integrations.django import DjangoIntegration
import sentry_sdk
from multifiberpy.sentry_multifiber import send_slack_event
from . import get_env_variable

ALLOWED_HOSTS = ['*']


sentry_sdk.init(
    dsn=get_env_variable('SENTRY_DSN'),
    integrations=[DjangoIntegration()],
    before_send=send_slack_event
)


CELERY_BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = CELERY_BROKER_URL
CELERY_BROKER_TRANSPORT = 'redis'

STATIC_URL = '/static/'
STATIC_ROOT = join(BASE_DIR, 'static')
STATICFILES_DIRS = (
    join(BASE_DIR, 'staticfiles'),
)
