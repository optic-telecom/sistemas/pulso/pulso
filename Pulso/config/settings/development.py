from .base import *
from os.path import join
from sentry_sdk.integrations.django import DjangoIntegration
import sentry_sdk

DEBUG = True

ALLOWED_HOSTS = ['*']


STATIC_URL = '/static/'
STATIC_ROOT = join(BASE_DIR, 'static')
STATICFILES_DIRS = (
    join(BASE_DIR, 'staticfiles'),
)