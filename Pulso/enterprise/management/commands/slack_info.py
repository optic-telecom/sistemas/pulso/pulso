import os
import logging
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from enterprise.models import CanalSlack, Enterprise, SpaceSlack, AppSlack
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
logging.basicConfig(level=logging.DEBUG)
User = get_user_model()


class Command(BaseCommand):
    help = ("Comando para la carga de información del space\
            Multifiber en el sistema pulso")

    def add_arguments(self, parser):
        parser.add_argument(
            'app',
            type=str,
            help='Indicates the name app boot token'
            )

    def handle(self, *args, **kwargs):
        app = kwargs["app"]
        if app == "multifiber":
            token = "MULTIFIBER_SLACK_BOT_TOKEN"
        elif app == "banda_ancha":
            token = "BANDA_ANCHA_SLACK_BOT_TOKEN"
        client = WebClient(token=os.environ.get(token))
        try:
            self.fetch_conversations(client, app)
        except Exception as e:
            print(str(e))

    def fetch_conversations(self, cliente, app):
        try:
            # Call the conversations.list method using the WebClient
            result = cliente.conversations_list()

            self.save_conversations(result["channels"], app)

        except SlackApiError as e:
            logging.error("Error fetching conversations: {}".format(e))

    def save_conversations(self, conversations, app):
        try:
            empresa = Enterprise.objects.get(
                name__icontains="Multifiber"
            )
        except Enterprise.DoesNotExist:
            empresa = None
        except Enterprise.MultipleObjectsReturned:
            empresa = None

        for conversation in conversations:
            canal, created = CanalSlack.objects.get_or_create(
                name=conversation["name"],
                canal_slack_id=conversation["id"],
            )
            if created:
                canal.enterprise.add(empresa)
                canal.save()

        if app == "banda_ancha":
            space = "Banda Ancha"
        elif app == "multifiber":
            space = "Multifiber"

        spaces, createds = SpaceSlack.objects.get_or_create(
            name=space.upper(),
            description=space
        )

        admin = User.objects.get(pk=1)

        apps, createdss =  AppSlack.objects.get_or_create(
            user=admin,
            name=space.upper(),
            space=spaces
        )
        if createdss:
            apps.enterprise.add(empresa)