from django.core.management.base import BaseCommand
from enterprise.models import GroupSlack


class Command(BaseCommand):
    help = 'Crea grupos en GroupSlack'

    def add_arguments(self, parser):
        parser.add_argument(
                'name',
                type=str,
                help='Indica el nombre del grupo slack a ser creado'
            )

    def handle(self, *args, **kwargs):

        name = kwargs["name"]

        grupo, create = GroupSlack.objects.get_or_create(
            name=name.strip(),
            description=name.strip().upper()
        )