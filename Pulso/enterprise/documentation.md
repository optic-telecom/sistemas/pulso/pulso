# MÓDULO ENTERPRISE

El módulo tiene 2 enfoques el primero va dirigido a tratar todo lo
referente a las empresas, el segundo va dirigido a tratar los temas
de Slack

# Empresas:
nota importante:
    A). Matrix sigue manejando todo los clientes
    B). Usuarios y Clientes deben ser filtrados tanto por operadores como
        por empresas (ambos), sujeto de acción inmediata el usuario.
    C). Usuarios separados por empresa, la respuesta de pulso será:
            El usuario X está relacionado a esta(s) empresa(s) y operador(es)

    MODELO DE PERFILUSUARIO - APP(users)
        user OneToOne
        empresa m2m
        operador m2m

    MODELO EMPRESA - APP(enterprise)
        country FK
        nombre
        telefono
        email
        direccion
        direccion_facturacion
        identificacion
        gonfiguraciones - JSONField() parametrizar ciertas cosas acá
        grupo_slack M2M
        aplicación_slack M2M

# Slack

    CANALSLACK - APP(enterprise)
        id
        nombre
        identificador_canal
        grupo_slack M2M
        canal_usuario (BOOLEAN O CHOICE)
        webhost

    GRUPOS-SLACK - APP(enterprise)
        name
        .
        .
        .

    APLICACIONSLACK - APP(enterprise)
        credentials
        .
        .
        .

# managment command
Para la app enterprise se decide generar un comando que se encarga de guardar los canales
pertencientes a las aplicaciones Multifiber y Banda Ancha este comando maneja un argumento
app que recibe el nombre de la aplicación a la cual queremos listar y guardar en la bd
en el caso de Multifiber el argumento será multifiber y en el caso de Banda Ancha pues banda_ancha

ej:

python3 manage.py slack_info banda_ancha
ó
python3 manage.py slack_info multifiber