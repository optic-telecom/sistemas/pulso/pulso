from rest_framework.viewsets import ModelViewSet
from enterprise.models import SpaceSlack
from enterprise.serializers import (
    SpaceSlackSerializer
)


class SpaceSlackViewSet(ModelViewSet):
    queryset = SpaceSlack.objects.all()
    serializer_class = SpaceSlackSerializer
