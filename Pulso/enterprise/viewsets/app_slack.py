from rest_framework.viewsets import ModelViewSet
from enterprise.models import AppSlack
from enterprise.serializers import (
    AppSlackReadSerializer,
    AppSlackCreateSerializer
)


class AppSlackViewSet(ModelViewSet):
    queryset = AppSlack.objects.all()

    def get_serializer_class(self):
        if self.request.method.lower() in ['post', 'put', "patch"]:
            return AppSlackCreateSerializer
        else:
            return AppSlackReadSerializer

