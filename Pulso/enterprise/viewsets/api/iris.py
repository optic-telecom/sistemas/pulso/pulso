import operator
from functools import reduce

from django.db.models import Q
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from enterprise.models import CanalSlack, GroupSlack
from enterprise.serializers import (
    CanalSlackReadSerializer,
    ChannelToGroupSerializer
)
from multifiberpy.permissions import IsSystemInternalPermission


@method_decorator(csrf_exempt, name="dispatch")
class IrisViewSet(ViewSet):
    """
    Maneja el conglomerado de apis para iris
    """
    permission_classes = (IsSystemInternalPermission | IsAuthenticated,)

    def channels(self, request, *args, **kwargs) -> Response:
        """
        Este método retorna un listado de canales de slack
        """
        data = request.POST
        try:
            canales = data.getlist("channels", [])
            if canales:
                canales = list(map(lambda x: x, canales))
                qs = CanalSlack.objects.filter(
                    reduce(
                            operator.or_,
                            (
                                Q(name__iexact=x) for x in canales
                            )
                        )
                )
                serializer = CanalSlackReadSerializer(
                    qs, many=True,
                    context={'request': request}
                    ).data
                status = 200
            else:
                serializer = {
                    "error": ("Se necesita al menos un nombre de canal\
                                en la lista para realizar la búsqueda")
                }
                status = 400
        except Exception as e:
            print(str(e))
            serializer = []
            status = 400
        return Response(serializer, status=status)

    def channels_to_groups(self, request, *args, **kwargs) -> Response:
        groups = self.request.POST.getlist("group", [])
        if groups:
            groups = list(map(lambda x: x.strip(), groups))
            qs = GroupSlack.objects.filter(
                reduce(
                        operator.or_,
                        (
                            Q(name__icontains=x) for x in groups
                        )
                    )
                )
            serializer = ChannelToGroupSerializer(
                qs, many=True,
                context={'request': request}
                ).data
            status = 200
        else:
            serializer = {
                "error": ("Se necesita al menos un nombre de grupo\
                            en la lista para realizar la búsqueda")
            }
            status = 400

        return Response(serializer, status=status)
