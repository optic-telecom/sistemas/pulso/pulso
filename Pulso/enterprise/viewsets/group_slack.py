from rest_framework.viewsets import ModelViewSet
from enterprise.models import GroupSlack
from enterprise.serializers import (
    GroupSlackSerializer
)


class GroupSlackViewSet(ModelViewSet):
    queryset = GroupSlack.objects.all()
    serializer_class = GroupSlackSerializer
