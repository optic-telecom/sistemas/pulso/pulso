from rest_framework.viewsets import ModelViewSet
from enterprise.models import Enterprise
from enterprise.serializers import (
    EnterpriseReadSerializer,
    EnterpriseCreateSerializer
)


class EnterpriseViewSet(ModelViewSet):
    queryset = Enterprise.objects.all()

    def get_serializer_class(self):
        if self.request.method.lower() in ['post', 'put', "patch"]:
            return EnterpriseCreateSerializer
        else:
            return EnterpriseReadSerializer
