from collections import OrderedDict

from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from enterprise.models import CanalSlack
from enterprise.serializers import (CanalSlackCreateSerializer,
                                    CanalSlackReadSerializer)
from multifiberpy.permissions import IsSystemInternalPermission


class P(PageNumberPagination):
    def get_paginated_response(self, data):
        try:
            page = self.page.next_page_number() - 1
        except Exception as e:
            page = self.page.previous_page_number() + 1
        return Response(OrderedDict([
            ('page', page),
            ('count', self.page.paginator.count),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))
class CanalSlackViewSet(ModelViewSet):
    permission_classes = (IsSystemInternalPermission | IsAuthenticated,)
    queryset = CanalSlack.objects.all()
    pagination_class = P

    def get_serializer_class(self):
        if self.request.method.lower() in ['post', 'put', "patch"]:
            return CanalSlackCreateSerializer
        else:
            return CanalSlackReadSerializer

    def list(self, request, *args, **kwargs):
        self.paginator.page_size = 10
        return super().list(request, *args, **kwargs)