from multifiberpy.admin import BaseAdmin
from django.contrib import admin
from enterprise.models import GroupSlack


@admin.register(GroupSlack)
class GroupSlackAdmin(BaseAdmin):
    list_display = (
        "id",
        "name",
        "description"
    )