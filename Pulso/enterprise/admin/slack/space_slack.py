from multifiberpy.admin import BaseAdmin
from django.contrib import admin
from enterprise.models import SpaceSlack


@admin.register(SpaceSlack)
class SpaceSlackAdmin(BaseAdmin):
    list_display = (
        "id",
        "name",
        "description",
    )