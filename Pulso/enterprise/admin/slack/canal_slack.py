from multifiberpy.admin import BaseAdmin
from django.contrib import admin
from enterprise.models import CanalSlack


@admin.register(CanalSlack)
class CanalSlackAdmin(BaseAdmin):
    list_display = (
        "id",
        "name",
        "name_format",
        "get_usage_title",
        "webhost",
    )
    search_fields = ("name",)
    list_filter = ("usage",)

    def get_usage_title(self, obj):
        return obj.get_usage_display()
    get_usage_title.short_description = 'Tipo de uso'
