from multifiberpy.admin import BaseAdmin
from django.contrib import admin
from enterprise.models import AppSlack


@admin.register(AppSlack)
class AppSlackAdmin(BaseAdmin):
    list_display = (
        "id",
        "name",
        "space",
        "get_enviroment_title",
        "user"
    )

    def get_enviroment_title(self, obj):
        return obj.get_enviroment_display()
    get_enviroment_title.short_description = 'Entorno'
