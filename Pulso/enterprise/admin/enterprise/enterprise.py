from multifiberpy.admin import BaseAdmin
from django.contrib import admin
from enterprise.models import Enterprise
from utils.views import replicador_enterprise


@admin.register(Enterprise)
class EnterpriseAdmin(BaseAdmin):
    list_display = (
        "id",
        "country",
        "name",
        "identification",
        "email",
    )

    # Al crear un registro
    def save_model(self, request, obj, form, change):
        # Ejecutamos el replicador
        print('Replicador')
        if change:
            res = replicador_enterprise(obj, 'update')
        else:
            res = replicador_enterprise(obj, 'create')
        print(res)
        super().save_model(request, obj, form, change)
    
    # Al eliminar un registro:
    def delete_model(self, request, obj):
        # Ejecutamos el replicador
        print('Replicador')
        res = replicador_enterprise(obj, 'delete')
        print(res)
        super().delete_model(request, obj)

    # Al eliminar varios registros:
    def delete_queryset(self, request, queryset):
        # Ejecutamos el replicador
        print('Replicador')
        for obj in queryset:
            res = replicador_enterprise(obj, 'delete')
            print(res)
        super().delete_queryset(request, queryset)