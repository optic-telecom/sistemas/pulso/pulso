from .app import (
    AppSlackCreateSerializer,
    AppSlackReadSerializer
)

from .canal import (
    CanalSlackCreateSerializer,
    CanalSlackReadSerializer
)

from .group import (
    GroupSlackSerializer
)

from .space import (
    SpaceSlackSerializer
)

from .channels_to_group import (
    ChannelToGroupSerializer
)