from rest_framework import serializers
from enterprise.models import SpaceSlack


class SpaceSlackSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(
                        view_name='enterprise:spaceslack-detail',
                        lookup_field="pk",
                        read_only=True
                    )

    class Meta:
        model = SpaceSlack
        fields = (
            "id",
            "name",
            "description",
            "url",
        )