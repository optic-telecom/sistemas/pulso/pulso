from rest_framework import serializers
from enterprise.models import CanalSlack


class CanalSlackCreateSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(
                        view_name='enterprise:canalslack-detail',
                        lookup_field="pk",
                        read_only=True
                    )

    class Meta:
        model = CanalSlack
        fields = (
            "name",
            "canal_slack_id",
            "usage",
            "webhost",
            "enterprise",
            "group_slack",
            "url"
        )


class CanalSlackReadSerializer(CanalSlackCreateSerializer):
    class Meta(CanalSlackCreateSerializer.Meta):
        pass

    def to_representation(self, instance):
        rep = super().to_representation(instance)
        if instance.enterprise:
            enterprise = instance.enterprise.all()
            rep["enterprise"] = list(
                map(
                    lambda x: {
                        "id": x.id,
                        "name": x.name,
                        "identification": x.identification
                    },
                    enterprise
                )
            )
        if instance.group_slack:
            group_slack = instance.group_slack.all()
            rep["group_slack"] = list(
                map(
                    lambda x: {
                        "id": x.id,
                        "name": x.name,
                    },
                    group_slack
                )
            )
        rep["usage"] = instance.get_usage_display()
        return rep
