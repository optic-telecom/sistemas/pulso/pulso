from rest_framework import serializers
from enterprise.models import GroupSlack


class GroupSlackSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(
                        view_name='enterprise:groupslack-detail',
                        lookup_field="pk",
                        read_only=True
                    )

    class Meta:
        model = GroupSlack
        fields = (
            "id",
            "name",
            "description",
            "url"
        )