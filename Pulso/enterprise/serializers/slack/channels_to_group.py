from rest_framework import serializers

from enterprise.models import GroupSlack, CanalSlack


class CanalInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = CanalSlack
        fields = (
            "name",
            "webhost"
        )


class ChannelToGroupSerializer(serializers.ModelSerializer):
    get_channels = CanalInfoSerializer(read_only=True, many=True)

    class Meta:
        model = GroupSlack
        fields = (
            "name",
            "get_channels"
        )