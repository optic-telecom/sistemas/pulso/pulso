from django.urls import reverse
from rest_framework import serializers
from enterprise.models import AppSlack


class AppSlackCreateSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(
                        view_name='enterprise:appslack-detail',
                        lookup_field="pk",
                        read_only=True
                    )

    class Meta:
        model = AppSlack
        fields = (
            "user",
            "name",
            "space",
            "enviroment",
            "credentials",
            "enterprise",
            "url"
        )


class AppSlackReadSerializer(AppSlackCreateSerializer):
    class Meta(AppSlackCreateSerializer.Meta):
        pass

    def to_representation(self, instance):
        rep = super().to_representation(instance)
        if instance.user:
            user = instance.user
            rep["user"] = {
                "id": user.pk,
                "username": user.username,
                "email": user.email
            }
        if instance.space:
            space = instance.space
            rep["space"] = {
                "id": space.pk,
                "name": space.name,
            }
        if instance.enterprise:
            enterprise = instance.enterprise.all()
            rep["enterprise"] = list(
                map(
                    lambda x: {
                        "id": x.id,
                        "name": x.name,
                        "identification": x.identification,
                    },
                    enterprise
                )
            )
        rep["enviroment"] = instance.get_enviroment_display()
        return rep
