from rest_framework import serializers
from enterprise.models import Enterprise
from utils.views import replicador_enterprise


class EnterpriseCreateSerializer(serializers.ModelSerializer):
    """ Serializer para crear objetos """
    url = serializers.HyperlinkedIdentityField(
        view_name='enterprise:enterprise-detail',
        lookup_field="pk",
        read_only=True)
    
    def create(self, validated_data):
        new_obj = super().create(validated_data)
        # Ejecutamos el replicador
        print('Acá debería estar el replicador')
        res = replicador_enterprise(new_obj, 'create')
        print(res)
        # Devolvemos la instancia ya creada
        return new_obj

    def update(self, instance, validated_data):
        updated_obj = super().update(instance, validated_data)
        # Ejecutamos el replicador
        print('Acá debería estar el replicador')
        res = replicador_enterprise(updated_obj, 'update')
        print(res)
        # Devolvemos la instancia ya creada
        return updated_obj

    class Meta:
        model = Enterprise
        fields = (
            "country",
            "name",
            "identification",
            "email",
            "phone",
            "address",
            "address_invoice",
            "settings",
            "operators",
            "group_slack",
            "url"
        )


class EnterpriseReadSerializer(EnterpriseCreateSerializer):
    """ Serializer para leer objetos """
    
    def to_representation(self, instance):
        rep = super().to_representation(instance)

        if instance.country:
            country = instance.country
            rep["country"] = {
                "id": country.pk,
                "country": country.name,
                "code": country.code
            }

        if instance.operators:
            operators = instance.operators.all()
            operators_list = list(
                map(
                    lambda x: {
                        "id": x.pk,
                        "name": x.name,
                        "code": x.code
                    },
                    operators
                )
            )
            rep["operators"] = operators_list

        if instance.group_slack:
            group_slack = instance.group_slack.all()
            group_slack_list = list(
                map(
                    lambda x: {"id": x.pk, "name": x.name},
                    group_slack
                )
            )
            rep["group_slack"] = group_slack_list
        return rep
