from django.db import models
from multifiberpy.models import BaseModel
from enterprise.utils import normalize_text


class SpaceSlack(BaseModel):
    name = models.CharField(
        max_length=255,
        verbose_name="Space *",
        help_text="varchar(255) required"
    )
    description = models.TextField(
        blank=True,
        null=True,
        verbose_name="Descripción",
        help_text="TextField"
    )

    def __str__(self):
        return f"{self.name}"

    def save(self, *args, **kwargs):
        self.name = normalize_text(
            self.name
        ).upper()
        super().save(*args, **kwargs)

    class Meta:
        ordering = ["-id"]
        verbose_name = "Espacio de Slack"
        verbose_name_plural = "Espacios de Slack"

