from django.db import models
from multifiberpy.models import BaseModel
from enterprise.utils import normalize_text


class GroupSlack(BaseModel):
    name = models.CharField(
        max_length=255,
        verbose_name="Grupo *",
        help_text="varchar(255) required"
    )
    description = models.TextField(
        blank=True,
        null=True,
        verbose_name="Descripción",
        help_text="TextField"
    )

    def __str__(self):
        return f"{self.name}"

    def clean(self):
        self.name = normalize_text(
            self.name
        ).upper()

    class Meta:
        ordering = ["-id"]
        verbose_name = "Grupo de Slack"
        verbose_name_plural = "Grupos de Slack"

    @property
    def get_channels(self):
        return self.canalslack_set.all()

