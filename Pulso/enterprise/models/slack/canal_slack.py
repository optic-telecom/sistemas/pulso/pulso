from django.db import models
from multifiberpy.models import BaseModel
from enterprise.utils import normalize_text


class CanalSlackManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().prefetch_related(
            "enterprise", "group_slack"
        )


class CanalSlack(BaseModel):
    name = models.CharField(
        max_length=255,
        verbose_name="Canal de Slack *",
        help_text="varchar(255) required"
    )
    name_format = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name="Canal de Slack format",
        help_text="varchar(255) no required"
    )
    canal_slack_id = models.CharField(
        max_length=255,
        verbose_name="Identificador Slack",
        help_text="varchar(255) required"
    )
    usage = models.SmallIntegerField(
        choices=((0, "Empresarial"), (1, "Personal")),
        default=0,
        verbose_name="Tipo de Canal",
        help_text="Integer, Indica si el canal es de uso\
                    personal o empresarial"
    )
    webhost = models.URLField(
        blank=True,
        null=True,
        verbose_name="Url WebHost",
        help_text="Text, indica el webhost con el cual interactua el registro"
    )
    enterprise = models.ManyToManyField(
        "enterprise.Enterprise",
        blank=True,
        verbose_name="Empresas *",
        help_text="FK, enterprise.Enterprise (required)"
    )
    group_slack = models.ManyToManyField(
        "enterprise.GroupSlack",
        blank=True,
        verbose_name="Grupos Slack",
        help_text="M2M, relacion al modelo enterprise.GroupSlack"
    )

    def __str__(self):
        return f"{self.name}"

    def save(self, *args, **kwargs):
        # if not self.pk:
        self.name_format = normalize_text(
            self.name
        ).upper()

        super().save(*args, **kwargs)

    class Meta:
        ordering = ["-id"]
        verbose_name = "Canal de Slack"
        verbose_name_plural = "Canales de Slack"

