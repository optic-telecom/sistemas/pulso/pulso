from django.db import models
from django.contrib.postgres.fields import JSONField
from multifiberpy.models import BaseModel
from django.contrib.auth import get_user_model
from enterprise.utils import normalize_text

User = get_user_model()

class AppSlackManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related(
            "user",
            "space",
        ).prefetch_related(
            "enterprise"
        )


class AppSlack(BaseModel):
    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        verbose_name="Creado por*",
        related_name="apps_slack_users",
        help_text="FK, relación AUTH.User"
    )
    name = models.CharField(
        max_length=100,
        verbose_name="Aplicación*",
        help_text="varchar(100)"
    )
    space = models.ForeignKey(
        "enterprise.SpaceSlack",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        verbose_name="Espacio Slack",
        related_name="spaces_apps_slack",
        help_text="FK, relación con el modelo\
                    enterprise.SpaceSlack"
    )
    enviroment = models.SmallIntegerField(
        choices=((1, "Development"), (2, "Stage"), (3, "Production")),
        default=0,
        verbose_name="Entorno",
        help_text=("Integer")
    )
    credentials = JSONField(
        default=dict,
        blank=True,
        null=True,
        verbose_name="Credenciales",
        help_text="Json con las credenciales de authenticación"
    )
    enterprise = models.ManyToManyField(
        "enterprise.Enterprise",
        blank=True,
        verbose_name="Empresa *",
        help_text="FK, enterprise.Enterprise (required)"
    )

    objects = AppSlackManager()

    def __str__(self):
        return f"{self.name}"

    def save(self, *args, **kwargs):
        self.name = normalize_text(
            self.name
        ).upper()
        super().save(*args, **kwargs)

    class Meta:
        ordering = ["-id"]
        verbose_name = "Aplicación de Slack"
        verbose_name_plural = "Aplicaciones de Slack"

