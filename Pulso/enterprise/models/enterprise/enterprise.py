from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import JSONField
from multifiberpy.models import BaseModel
from enterprise.utils import normalize_text
from django.db.models.signals import pre_delete, post_delete
from django.dispatch.dispatcher import receiver
from utils.views import replicador_enterprise

User = get_user_model()


class EnterpriseManager(models.Manager):

    def get_queryset(self):
        return (super().get_queryset()
                .select_related(
                    "country"
                ).prefetch_related(
                    "operators",
                    "group_slack"
                )
            )


class Enterprise(BaseModel):
    country = models.ForeignKey(
        "places.Country",
        on_delete=models.PROTECT,
        verbose_name="País *",
        help_text="FK, relación modelo places.Country (required)"
    )
    name = models.CharField(
        max_length=255,
        verbose_name="Empresa *",
        help_text="varchar(255) required"
    )
    name_format = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name="Empresa *",
        db_index=True,
        help_text="varchar(255) no required"
    )
    identification = models.CharField(
        max_length=255,
        verbose_name="Identificación *",
        help_text="varchar(255) required"
    )
    email = models.EmailField(
        verbose_name="Email",
        blank=True,
        null=True,
        help_text="varchar(255) no required"
    )
    phone = models.CharField(
        max_length=30,
        blank=True,
        null=True,
        verbose_name="Telefono",
        help_text="varchar no required"
    )
    address = models.TextField(
        verbose_name="Dirección *",
        help_text="Text required"
    )
    address_invoice = models.TextField(
        verbose_name="Dirección de Facturación",
        blank=True,
        null=True,
        help_text="Text required"
    )
    settings = JSONField(
        default=dict,
        blank=True,
        null=True,
        verbose_name="Configuraciones",
        help_text="JSON, no required"
    )
    operators = models.ManyToManyField(
        "models.Operator",
        blank=True,
        verbose_name="Operadores",
        help_text="M2M relacion al modelo models.Operator"
    )
    group_slack = models.ManyToManyField(
        "enterprise.GroupSlack",
        blank=True,
        verbose_name="Grupos Slack",
        help_text="M2M, relacion al modelo enterprise.GroupSlack"
    )

    objects = EnterpriseManager()

    def __str__(self):
        return f"{self.name}"

    def clean(self):
        self.name_format = normalize_text(
            self.name
        )

    class Meta:
        ordering = ["-id"]
        unique_together = ["identification", "email"]
        verbose_name = "Empresa"
        verbose_name_plural = "Empresas"


@receiver(post_delete, sender=Enterprise)
def _enterprise_delete(sender, instance, **kwargs):
    # Ejecutamos el replicador luego de que se elimine el registro
    print("Replicador")
    res = replicador_enterprise(instance, 'delete')
    print(res)
