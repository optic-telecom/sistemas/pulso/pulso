# from django.test import TestCase
import os
import logging
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
logging.basicConfig(level=logging.DEBUG)
client = WebClient(token=os.environ.get("SLACK_BOT_TOKEN"))
# You probably want to use a database to store any conversations information ;)
conversations_store = {}


def fetch_conversations():
    try:
        # Call the conversations.list method using the WebClient
        result = client.conversations_list()
        save_conversations(result["channels"])

    except SlackApiError as e:
        logging.error("Error fetching conversations: {}".format(e))


# Put conversations into the JavaScript object
def save_conversations(conversations):
    conversation_name = ""
    for conversation in conversations:
        # Key conversation info on its unique ID
        conversation_name = conversation["name"]
        print(conversation_name)

        # Store the entire conversation object
        # (you may not need all of the info)
        conversations_store[conversation_name] = conversation


fetch_conversations()
