from rest_framework.routers import DefaultRouter
from enterprise.viewsets import (
    EnterpriseViewSet,
    AppSlackViewSet,
    CanalSlackViewSet,
    GroupSlackViewSet,
    SpaceSlackViewSet
)

router = DefaultRouter()

router.register("enterprise", EnterpriseViewSet)
router.register("app-slack", AppSlackViewSet)
router.register("canal-slack", CanalSlackViewSet)
router.register("group-slack", GroupSlackViewSet)
router.register("spaces-slack", SpaceSlackViewSet)
