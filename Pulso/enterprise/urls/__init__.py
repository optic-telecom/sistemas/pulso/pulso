from django.urls import path, include
from enterprise.urls.routers import router

app_name = "enterprise"

urlpatterns = [
    path(
        "api/v1/",
        include(router.urls)
    ),
    path(
        "api/v1/",
        include("enterprise.urls.apis_to_iris")
    )
]