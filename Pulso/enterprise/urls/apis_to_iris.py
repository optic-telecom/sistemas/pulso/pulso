from django.urls import path

from enterprise.viewsets import IrisViewSet

app_name = "apis_to_iris"

urlpatterns = [
    path(
            "channels-slack-iris/",
            IrisViewSet.as_view({"post": "channels"}),
            name="channels_slack"
        ),
    path(
            "channels-to-groups-slack/",
            IrisViewSet.as_view({"post": "channels_to_groups"}),
            name="channels_to_groups"
        ),

]
