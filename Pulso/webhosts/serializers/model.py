from rest_framework import serializers
from webhosts.models import Model


class ModelSerializer(serializers.ModelSerializer):
    """ Serialiazer de modelos de la base de datos. """
    class Meta:
        model = Model
        fields = ('id', 'name')

    def is_valid(self, *args, **kwargs):
        """ Valida que el modelo no se haya creado ya. """
        if not self.instance:
            name = self.initial_data.get('name')
            if name:
                model = Model.objects.filter(
                    name=name).exists()
                if model:
                    _err = {
                        'error': 'Ese modelo ya existe en la base de datos.'
                        }
                    raise serializers.ValidationError(_err)
        return super(ModelSerializer, self).is_valid(*args, **kwargs)
