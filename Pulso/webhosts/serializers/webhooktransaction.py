from rest_framework import serializers
from webhosts.models import WebHookTransaction


class WebHookTransactionSerializer(serializers.ModelSerializer):
    """ Serialiazer de transacciones de webhooks. """
    class Meta:
        model = WebHookTransaction
        fields = (
            'id',
            'system',
            'action',
            'status',
            'data',
            'user')

    def is_valid(self, *args, **kwargs):
        """ Valida que data no esté vacío. """
        if not self.instance:
            data = self.initial_data.get('data')
            if not data:
                _err = {
                    'error': 'Debe especificar la data para la transacción.'}
                raise serializers.ValidationError(_err)
        return super(WebHookTransactionSerializer, self).is_valid(*args, **kwargs)


