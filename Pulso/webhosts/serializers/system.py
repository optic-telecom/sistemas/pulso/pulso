from rest_framework import serializers
from webhosts.models import System


class SystemSerializer(serializers.ModelSerializer):
    """ Serialiazer de sistemas suscritos. """
    class Meta:
        model = System
        fields = ('id', 'name')

    def is_valid(self, *args, **kwargs):
        """ Valida que el modelo no se haya creado ya. """
        if not self.instance:
            name = self.initial_data.get('name')
            if name:
                system = System.objects.filter(
                    name=name).exists()
                if system:
                    _err = {
                        'error': 'Ese sistema ya está registrado.'
                        }
                    raise serializers.ValidationError(_err)
        return super(SystemSerializer, self).is_valid(*args, **kwargs)

