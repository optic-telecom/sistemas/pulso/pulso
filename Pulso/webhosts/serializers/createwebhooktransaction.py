from rest_framework import serializers
from webhosts.models import WebHookTransaction, Model, System
from webhosts.models.action import ACTION_CHOICES


class CreateWebHookTransactionSerializer(serializers.ModelSerializer):
    """ Serialiazer para crear transacciones de webhooks. """
    system = serializers.CharField(max_length=50, write_only=True)
    model = serializers.CharField(max_length=50, write_only=True)
    action = serializers.ChoiceField(write_only=True, choices=ACTION_CHOICES)

    class Meta:
        model = WebHookTransaction
        fields = ('data', 'action', 'model', 'system')

    def is_valid(self, *args, **kwargs):
        """ Valida que la data, el modelo y el sistema existan. """
        if not self.instance:
            data = self.initial_data.get('data')
            if not data:
                _err = {
                    'error': 'Debe especificar la data para la transacción.'
                    }
                raise serializers.ValidationError(_err)
            model = self.initial_data.get('model')
            if model:
                model_exists = Model.objects.filter(
                    name=model).exists()
                if not model_exists:
                    error = 'Debe especificar un modelo válido para la transacción.'
                    _err = {
                        'error': error
                        }
                    raise serializers.ValidationError(_err)
            system = self.initial_data.get('system')
            if system:
                system_exists = System.objects.filter(
                    name=system).exists()
                if not system_exists:
                    _err = {
                        'error': 'Debe especificar un nombre válido del sistema del cual envía la transacción.'
                        }
                    raise serializers.ValidationError(_err)
        return super(CreateWebHookTransactionSerializer, self).is_valid(*args, **kwargs)


