from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework.utils import model_meta

from webhosts.models import TransactionReplication


class TransactionReplicationSerializer(serializers.ModelSerializer):
    """
    Serialiazer de replicación de transacciones de un
    webhook sobre otros webhooks suscritos al evento
    """
    class Meta:
        model = TransactionReplication
        fields = (
            'id',
            'transaction',
            'destination',
            'status_code'
            )