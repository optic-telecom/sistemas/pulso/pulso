from rest_framework import serializers
from webhosts.models import WebHook


class WebHookSerializer(serializers.ModelSerializer):
    """ Serialiazer de webhooks. """
    class Meta:
        model = WebHook
        fields = ('id', 'system', 'url', 'active', 'actions')

    def is_valid(self, *args, **kwargs):
        """ Valida que el webhook no se haya creado ya. """
        if not self.instance:
            url = self.initial_data.get('url')
            if url:
                webhook = WebHook.objects.filter(
                    url=url).exists()
                if webhook:
                    _err = {
                        'error': 'Esa url ya existe en la base de datos.'}
                    raise serializers.ValidationError(_err)
            actions = self.initial_data.get('actions')
            if not actions:
                _err = {
                    'error': 'Se debe especificar al menos una acción para el webhook.'
                    }
                raise serializers.ValidationError(_err)
        return super(WebHookSerializer, self).is_valid(*args, **kwargs)
