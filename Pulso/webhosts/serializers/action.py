from rest_framework import serializers
from webhosts.models import Action


class ActionSerializer(serializers.ModelSerializer):
    """ Serialiazer de acciones. """
    class Meta:
        model = Action
        fields = ('id', 'model', 'action')

    def is_valid(self, *args, **kwargs):
        """ Valida que la acción para ese modelo no se haya creado ya. """
        if not self.instance:
            model = self.initial_data.get('model')
            action = self.initial_data.get('action')
            if model and action:
                new_action = Action.objects.filter(
                    model=model,
                    action=action).exists()
                if new_action:
                    _err = {
                        'error': 'Esa acción ya existe para ese modelo.'
                        }
                    raise serializers.ValidationError(_err)
        return super(ActionSerializer, self).is_valid(*args, **kwargs)

