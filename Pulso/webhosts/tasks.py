from __future__ import absolute_import, unicode_literals
from tasks.celery import app as celery_app
import requests
from django.conf import settings

from webhosts.models.webhooktransaction import PROCESSED, ERROR

from webhosts.models import (
    System,
    WebHook,
    WebHookTransaction,
    TransactionReplication
    )


def get_user_token(url):
    """ Obtiene el token de autorización para el header """
    data = {
        'username': settings.TASK_USERNAME,
        'password': settings.TASK_PASSWORD
    }
    protocol = 'http://'
    if protocol not in url:
        url = protocol + url
    auth_response = requests.post(url, data=data)
    token = auth_response.json()['token']
    headers = {'Authorization': 'jwt ' + token}
    return headers


def send_request(url, action, headers, data):
    """ Envía un request a la url con data. """
    # Send request based on action
    if action == 'create':
        response = requests.post(url, data=data, headers=headers)
    elif action == 'update':
        # id en la data para el url
        url = url + data['id'] + '/'
        response = requests.patch(url, data=data, headers=headers)
    elif action == 'delete':
        url = url + data['id'] + '/'
        # header['replicate'] = True # Header para evitar la replica
        response = requests.delete(url, data=data, headers=headers)
    # Return status code
    return response.status_code


# VER QUE OTRAS COSAS SE NECESITAN
@celery_app.task(name='webhosts.replicate', max_retries=5)
def replicate(transaction_pk):
    """ Replica la transacción enviada a pulso a los webhooks que lo requieran.
        Asume que ya la acción se realizó en el sistema de origen. """
    transaction = WebHookTransaction.objects.get(id=transaction_pk)
    try:
        # Sistemas que no son el de origen
        systems = System.objects.exclude(id=transaction.system.id)
        for system in systems:
            # Webhooks activos y que permitan el evento
            webhooks = WebHook.objects.filter(
                system=system,
                active=True,
                actions__id=transaction.action.id
                )
            # Request a cada url
            for webhook in webhooks:
                action = transaction.action.get_action_display()
                headers = {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
                status_code = send_request(
                        url=webhook.url,
                        action=action,
                        headers=headers,
                        data=transaction.data
                    )
                # Crear una transaction replication
                replication = TransactionReplication.objects.create(
                    transaction=transaction,
                    destination=system,
                    status_code=status_code
                )
                replication.save()
        # Marcar como procesado si todo salió bien
        transaction.status = PROCESSED
    except Exception as e:
        print(str(e))
        transaction.status = ERROR
    transaction.save()
    print("Done")
