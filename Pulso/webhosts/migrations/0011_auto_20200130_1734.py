# Generated by Django 2.2.3 on 2020-01-30 17:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webhosts', '0010_auto_20200127_2039'),
    ]

    operations = [
        migrations.AlterField(
            model_name='action',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='historicalaction',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='historicalmodel',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='historicalsystem',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='historicaltransactionreplication',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='historicalwebhook',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='historicalwebhooktransaction',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='model',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='system',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='transactionreplication',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='webhook',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='webhooktransaction',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, null=True),
        ),
    ]
