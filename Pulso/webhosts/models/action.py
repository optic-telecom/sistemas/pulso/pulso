from django.db import models
from utils.models import BaseModel

TYPE_CREATE = 1
TYPE_UPDATE = 2
TYPE_DELETE = 3
ACTION_CHOICES = (
    (TYPE_CREATE, "create"),
    (TYPE_UPDATE, "update"),
    (TYPE_DELETE, "delete"),
)


class Action(BaseModel):
    """ Combinación de modelos con acciones permitidas para el modelo """
    model = models.ForeignKey("Model", on_delete=models.CASCADE)
    action = models.PositiveSmallIntegerField(choices=ACTION_CHOICES)

    def __str__(self):
        return f"{self.model}_{self.get_action_display()}"

    class Meta:
        ordering = ['-id']
        unique_together = ['model', 'action']
