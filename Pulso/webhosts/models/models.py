from django.db import models
from utils.models import BaseModel


class Model(BaseModel):
    """ Modelo asociado a acciones de webhooks """
    name = models.CharField(max_length=50)

    class Meta:
        ordering = ['-name']

    def __str__(self):
        return self.name

