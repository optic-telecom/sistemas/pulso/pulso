from django.db import models
from utils.models import BaseModel
from django.contrib.postgres.fields import JSONField
from django.contrib.auth import get_user_model
User = get_user_model()

UNPROCESSED = 1
PROCESSED = 2
ERROR = 3

STATUS_CHOICES = (
    (UNPROCESSED, 'Sin procesar'),
    (PROCESSED, 'Procesado'),
    (ERROR, 'Error'),
)


class WebHookTransaction(BaseModel):
    """ Información de las transacciones sobre webhooks"""
    system = models.ForeignKey("System", on_delete=models.SET_NULL, null=True)
    action = models.ForeignKey("Action", on_delete=models.SET_NULL, null=True)
    # from_pulso = models.BooleanField(default=False)
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES)
    data = JSONField()
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return f"{self.system.name} \n {self.data}"

