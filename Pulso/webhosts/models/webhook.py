from django.db import models
from utils.models import BaseModel


class WebHook(BaseModel):
    """ Información de los webhooks.
        Las acciones se asocian a la url directamente. """
    system = models.ForeignKey("System", on_delete=models.CASCADE)
    url = models.URLField(unique=True)
    active = models.BooleanField(default=True)
    actions = models.ManyToManyField("Action", related_name='actions')

    class Meta:
        ordering = ['-system', '-url']

    def __str__(self):
        return self.url