from django.db import models
from utils.models import BaseModel


class System(BaseModel):
    """ Sistema al que pertenecen los webhooks """
    name = models.CharField(max_length=50, unique=True)

    class Meta:
        ordering = ['-name']

    def __str__(self):
        return self.name
