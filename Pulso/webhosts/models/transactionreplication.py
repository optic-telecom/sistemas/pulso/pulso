from django.db import models
from utils.models import BaseModel
from django.core.validators import MaxValueValidator, MinValueValidator


class TransactionReplication(BaseModel):
    """ Información de la replicación de transacciones de un
    webhook sobre otros webhooks suscritos al evento """
    transaction = models.ForeignKey(
        "WebHookTransaction",
        on_delete=models.SET_NULL,
        null=True
        )
    destination = models.ForeignKey(
        "System",
        on_delete=models.SET_NULL,
        null=True
        )
    status_code = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(100), MaxValueValidator(505)],
        null=True, blank=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return f"Transacción: {self.transaction}\
                    Enviada a {self.destination} con\
                        status code {self.status_code}"
