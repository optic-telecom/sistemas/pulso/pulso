from rest_framework import viewsets
from webhosts.serializers import ModelSerializer

from webhosts.models import Model


class ModelViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Model
    retrieve:
        End point for Model
    update:
        End point for Model
    delete:
        End point for Model
    """
    queryset = Model.objects.all()
    serializer_class = ModelSerializer
