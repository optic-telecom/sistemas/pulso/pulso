from rest_framework import viewsets
from webhosts.serializers import SystemSerializer
from webhosts.models import System


class SystemViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for System
    retrieve:
        End point for System
    update:
        End point for System
    delete:
        End point for System
    """
    queryset = System.objects.all()
    serializer_class = SystemSerializer

