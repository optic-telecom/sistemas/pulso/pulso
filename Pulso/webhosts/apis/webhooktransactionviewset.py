from rest_framework import status
from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from utils.permissions import IsSystemInternalPermission

from webhosts.serializers import (
    WebHookTransactionSerializer,
    CreateWebHookTransactionSerializer
    )

from webhosts.models import Model, Action, WebHookTransaction, System
from webhosts.tasks import replicate
from django.contrib.auth import get_user_model
User = get_user_model()


class WebHookTransactionViewSet(
                        mixins.ListModelMixin,
                        mixins.CreateModelMixin,
                        mixins.RetrieveModelMixin,
                        viewsets.GenericViewSet):
    """
    create:
        End point for WebHookTransaction
    retrieve:
        End point for WebHookTransaction
    """
    queryset = WebHookTransaction.objects.all()
    serializer_class = WebHookTransactionSerializer
    permission_classes = (IsSystemInternalPermission | IsAuthenticated,)

    def get_serializer_class(self):
        serializer_class = self.serializer_class
        if self.request.method in ['POST']:
            serializer_class = CreateWebHookTransactionSerializer
        return serializer_class

    def perform_create(self, serializer):
        return serializer.save()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # Create WebHookTransactionSerializer and perform_create of that one
        system = System.objects.get(
            name=serializer.validated_data['system']
            )
        model_pk = Model.objects.get(
            name=serializer.validated_data['model']
            ).id
        action_pk = Action.objects.get(
            model=model_pk, action=serializer.validated_data['action']
            ).id
        user_pk = request.user.id
        serializer_data = serializer.validated_data['data']
        serializer_data['replicate'] = True
        data = {
            'system': system.id,
            'action': action_pk,
            'status': 1,
            'data': serializer_data,
            'user': user_pk,
        }
        create_serializer = WebHookTransactionSerializer(data=data)
        create_serializer.is_valid(raise_exception=True)
        #
        transaction = self.perform_create(create_serializer)
        headers = self.get_success_headers(serializer.data)
        # Replicate action
        replicate.delay(
            transaction_pk=transaction.id
            )
        return Response(
            create_serializer.data,
            status=status.HTTP_201_CREATED,
            headers=headers
            )