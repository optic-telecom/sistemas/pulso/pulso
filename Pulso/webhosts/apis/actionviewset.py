from rest_framework import viewsets
from webhosts.serializers import ActionSerializer
from webhosts.models import Action


class ActionViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Action
    retrieve:
        End point for Action
    update:
        End point for Action
    delete:
        End point for Action
    """
    queryset = Action.objects.all()
    serializer_class = ActionSerializer
