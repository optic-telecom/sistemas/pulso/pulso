from rest_framework import viewsets
from webhosts.serializers import WebHookSerializer
from webhosts.models import WebHook


class WebHookViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for WebHook
    retrieve:
        End point for WebHook
    update:
        End point for WebHook
    delete:
        End point for WebHook
    """
    queryset = WebHook.objects.all()
    serializer_class = WebHookSerializer
