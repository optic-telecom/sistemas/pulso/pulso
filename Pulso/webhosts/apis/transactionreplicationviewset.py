from rest_framework import viewsets, mixins
from webhosts.serializers import TransactionReplicationSerializer
from webhosts.models import TransactionReplication


class TransactionReplicationViewSet(
                        mixins.ListModelMixin,
                        mixins.RetrieveModelMixin,
                        viewsets.GenericViewSet):
    """
    retrieve:
        End point for TransactionReplication
    """
    queryset = TransactionReplication.objects.all()
    serializer_class = TransactionReplicationSerializer
