from rest_framework import routers
from webhosts.apis import (
        ModelViewSet, ActionViewSet, WebHookViewSet,
        WebHookTransactionViewSet, TransactionReplicationViewSet,
        SystemViewSet
    )

router = routers.DefaultRouter()
router.register(
        r'model',
        ModelViewSet,
        'model_api'
    )
router.register(
        r'action',
        ActionViewSet,
        'action_api'
    )
router.register(
        r'system',
        SystemViewSet,
        'system_api'
    )
router.register(
        r'webhook',
        WebHookViewSet,
        'webhook_api'
    )
router.register(
        r'transaction',
        WebHookTransactionViewSet,
        'webhook_transaction_api'
    )
router.register(
        r'replication',
        TransactionReplicationViewSet,
        'transaction_replication_api'
    )