from django.contrib import admin
from utils.admin import BaseAdmin

from webhosts.models import WebHookTransaction


@admin.register(WebHookTransaction)
class WebHookTransactionAdmin(BaseAdmin):
    list_display = ('id', 'system', 'action', 'status', 'data', 'user')

