from django.contrib import admin
from utils.admin import BaseAdmin
from webhosts.models import Model


@admin.register(Model)
class ModelAdmin(BaseAdmin):
    list_display = ('id', 'name')

