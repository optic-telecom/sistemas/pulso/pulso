from django.contrib import admin
from utils.admin import BaseAdmin
from webhosts.models import Action


@admin.register(Action)
class ActionAdmin(BaseAdmin):
    list_display = ('id', 'model', 'action')
