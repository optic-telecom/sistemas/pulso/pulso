from django.contrib import admin
from utils.admin import BaseAdmin
from webhosts.models import WebHook


@admin.register(WebHook)
class WebHookAdmin(BaseAdmin):
    list_display = ('id', 'system', 'url', 'active')

