from django.contrib import admin
from utils.admin import BaseAdmin

from webhosts.models import TransactionReplication


@admin.register(TransactionReplication)
class TransactionReplicationAdmin(BaseAdmin):
    list_display = ('id', 'transaction', 'destination', 'status_code')
