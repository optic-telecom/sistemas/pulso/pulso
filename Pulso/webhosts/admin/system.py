from django.contrib import admin
from utils.admin import BaseAdmin

from webhosts.models import System


@admin.register(System)
class SystemAdmin(BaseAdmin):
    list_display = ('id', 'name')