from django.apps import AppConfig


class WebhostsConfig(AppConfig):
    name = 'webhosts'
