#!/bin/bash

if [ ${10} == "postgresql" ]
then
    if [ ${11} == "None" ]
    then
        if [ ${12} == "basic" ]
        then
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 pg_dump -U $7 -d $6"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4 
            gzip -q $9$8.sql
        elif [ ${12} == "-C" ]
        then
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 pg_dump -U $7 -d $6 -C"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4            
            gzip -q $9$8.sql
        elif [ ${12} == "-h" ]
        then
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 pg_dump -U $7 -d $6 -h localhost"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4 
            gzip -q $9$8.sql
        elif [ ${12} == "-h and -C" ]
        then
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 pg_dump -U $7 -d $6 -h localhost -C"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4 
            gzip -q $9$8.sql
        fi
    else
        if [ ${12} == "basic" ]
        then
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 pg_dump -U $7 -d $6 -p ${11}"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4 
            gzip -q $9$8.sql
        elif [ ${12} == "-C" ]
        then
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 pg_dump -U $7 -d $6 -p ${11} -C"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4 
            gzip -q $9$8.sql
        elif [ ${12} == "-h" ]
        then
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 pg_dump -U $7 -d $6 -h localhost -p ${11}"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4 
            gzip -q $9$8.sql
        elif [ ${12} == "-h and -C" ]
        then
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 pg_dump -U $7 -d $6 -h localhost -p ${11} -C"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4 
            gzip -q $9$8.sql
        fi
    fi
else 
    if [ ${11} == "None" ]
    then
        if [ ${12} == "basic" ]
        then
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 mysqldump -u $7 $6"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4 
            gzip -q $9$8.sql
        elif [ ${12} == "-C" ]
        then
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 mysqldump -C -u $7 $6"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4 
            gzip -q $9$8.sql
        elif [ ${12} == "-h" ]
        then
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 mysqldump -h localhost -u $7 $6"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4 
            gzip -q $9$8.sql
        elif [ ${12} == "-h and -C" ]
        then
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 mysqldump -h localhost -C -u $7 $6"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4 
            gzip -q $9$8.sql
        fi
    else
        if [ ${12} == "-C" ]
        then
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 mysqldump -P ${11} -C -u $7 $6"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4 
            gzip -q $9$8.sql
        elif [ ${12} == "-h" ]
        then
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 mysqldump -h localhost -P ${11} -u $7 $6"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4 
            gzip -q $9$8.sql
        elif [ ${12} == "-h and -C" ]
        then
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 mysqldump -h localhost -P ${11} -C -u $7 $6"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4 
            gzip -q $9$8.sql
        else
            sshpass -p $3 ssh $1@$2 "sshpass -p $5 mysqldump -P ${11} -u $7 $6"  >> $9$8.sql
            sshpass -p $3 rsync -zvh $9$8.sql  $1@$2:$4 
            gzip -q $9$8.sql
        fi
    fi
fi

