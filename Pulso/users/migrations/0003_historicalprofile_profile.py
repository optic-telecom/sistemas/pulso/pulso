# Generated by Django 2.2.3 on 2021-03-05 05:44

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import simple_history.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('models', '0028_auto_20210113_1949'),
        ('enterprise', '0001_initial'),
        ('users', '0002_auto_20190901_0228'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('status_field', models.BooleanField(default=True)),
                ('id_data', models.UUIDField(blank=True, db_index=True, null=True)),
                ('enterprise', models.ManyToManyField(blank=True, help_text='M2M, relación con el modelo enterpise.Enterprise', to='enterprise.Enterprise', verbose_name='Empresas')),
                ('operator', models.ManyToManyField(blank=True, help_text='M2M, relación con el modelo models.Operator', to='models.Operator', verbose_name='Operadores')),
                ('user', models.OneToOneField(help_text='OneToOne, modelo AUTH.User', on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Usuario')),
            ],
            options={
                'verbose_name': 'Perfil de Usuario',
                'verbose_name_plural': 'Perfiles de usuario',
                'ordering': ['-id'],
            },
        ),
        migrations.CreateModel(
            name='HistoricalProfile',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('created', models.DateTimeField(blank=True, editable=False)),
                ('modified', models.DateTimeField(blank=True, editable=False)),
                ('status_field', models.BooleanField(default=True)),
                ('id_data', models.UUIDField(blank=True, db_index=True, null=True)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('user', models.ForeignKey(blank=True, db_constraint=False, help_text='OneToOne, modelo AUTH.User', null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Usuario')),
            ],
            options={
                'verbose_name': 'historical Perfil de Usuario',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
    ]
