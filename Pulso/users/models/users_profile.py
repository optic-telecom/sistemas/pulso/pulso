# -*- coding: utf-8 -*-
from django.db import models
from multifiberpy.models import BaseModel
from django.contrib.auth import get_user_model

User = get_user_model()


class Profile(BaseModel):
    user = models.OneToOneField(
        User,
        on_delete=models.PROTECT,
        verbose_name="Usuario",
        help_text="OneToOne, modelo AUTH.User"
    )
    enterprise = models.ManyToManyField(
        "enterprise.Enterprise",
        blank=True,
        verbose_name="Empresas",
        help_text="M2M, relación con el modelo enterpise.Enterprise"
    )
    operator = models.ManyToManyField(
        "models.Operator",
        blank=True,
        verbose_name="Operadores",
        help_text="M2M, relación con el modelo models.Operator"
    )

    def __str__(self):
        return f"{self.user}"

    class Meta:
        ordering = ["-id"]
        verbose_name = "Perfil de Usuario"
        verbose_name_plural = "Perfiles de usuario"