from django.contrib import admin

from multifiberpy.admin import BaseAdmin
from users.models import Profile


@admin.register(Profile)
class ProfileAdmin(BaseAdmin):
    list_display = (
        "id",
        "user"
    )
