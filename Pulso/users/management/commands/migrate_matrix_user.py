import requests
from django.contrib.auth.models import User
from django.db import transaction, IntegrityError
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Displays current time'

    def connect_to_matrix(self, url):
        headers = {
            "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s"
        }
        request = requests.get(url, headers=headers, verify=False)
        if request.status_code == 200:
            response = request.json()["results"]
            return response
        else:
            return None

    def create_or_update_user(self, usuario_pulso, u):
        try:
            usuario_pulso.first_name = u["first_name"]
            usuario_pulso.last_name = u["last_name"]
            usuario_pulso.password = u["password"]
            usuario_pulso.email = u["email"]
            usuario_pulso.username = u["username"]
            usuario_pulso.is_staff = u["is_staff"]
        except AttributeError:
            return False
        try:
            with transaction.atomic():
                usuario_pulso.save()
        except IntegrityError as e:
            print(str(e))
            return False
        else:
            return True

    def handle(self, *args, **kwargs):
        # URL = "http://localhost:8000"
        URL = "https://optic.matrix2.cl"
        url = f"{URL}/api/v1/user-migration/"
        usuarios_matrix = self.connect_to_matrix(url)
        for u in usuarios_matrix:
            usuario_pulso, nuevo = User.objects.get_or_create(
                username=u["username"]
            )

            self.create_or_update_user(usuario_pulso, u)
