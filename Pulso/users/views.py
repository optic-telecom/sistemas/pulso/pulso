from rest_framework.response import Response
from datetime import datetime
from rest_framework import status
from .models import *
from enterprise.serializers.enterprise import EnterpriseReadSerializer
from models.serializers import OperatorSerializer

def jwt_response_payload_handler(token, user=None, request=None):
    final_response = {
        'token': token,
        'empresas': [],
        'operadores': []
    }
    print(user)
    # Iteramos sobre las empresas:
    for enterprise in user.profile.enterprise.all():
        serialized_enterprise = EnterpriseReadSerializer(enterprise, context={'request': request}).data
        final_response['empresas'].append(serialized_enterprise)
    # Iteramos sobre los operadores
    for operator in user.profile.operator.all():
        serialized_operator = OperatorSerializer(operator, context={'request': request}).data
        final_response['operadores'].append(serialized_operator)

    return final_response