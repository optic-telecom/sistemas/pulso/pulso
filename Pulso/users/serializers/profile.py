from django.contrib.auth import get_user_model
from rest_framework import serializers
from users.models import Profile
User = get_user_model()


class ProfileUserCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = (
            "user",
            "operator",
            "enterprise",
        )


class ProfileUserReadSerializer(ProfileUserCreateSerializer):
    class Meta(ProfileUserCreateSerializer.Meta):
        pass

    def to_representation(self, instance):
        rep = super().to_representation(instance)
        if instance.operator:
            operator = instance.operator.all()
            rep["operator"] = list(
                map(
                    lambda x: {
                        "id": x.pk,
                        "name": x.name
                    },
                    operator
                )
            )
        if instance.enterprise:
            enterprise = instance.enterprise.all()
            rep["enterprise"] = list(
                map(
                    lambda x: {
                        "id": x.pk,
                        "name": x.name,
                        "identification": x.identification
                    },
                    enterprise
                )
            )
        return rep
