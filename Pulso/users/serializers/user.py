from django.contrib.auth import get_user_model
from rest_framework.serializers import HyperlinkedModelSerializer

User = get_user_model()


class UserSerializer(HyperlinkedModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')
