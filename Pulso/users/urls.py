from django.urls import path, re_path
from rest_framework import routers
from rest_framework_jwt.views import (
    obtain_jwt_token,
    refresh_jwt_token,
    verify_jwt_token
)
from users.apis import (
    UserViewSet,
    UserView,
    PasswordReset,
    ProfileViewset
)
from places.autocompletes import (
    NodeAutocomplete,
    RegionAutocomplete,
    CommuneAutocomplete,
    StreetAutocomplete,
    StreetLocationAutocomplete,
    CompleteLocationAutocomplete,
    TowerAutocomplete,
    FloorAutocomplete,
    OperatorAutocomplete)

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r"profiles", ProfileViewset, "perfil")


urlpatterns = [
    # url(r'^api/user', UserView.as_view(), name='user_view'),
    # url(r'^api/password_reset', PasswordReset.as_view()),
    re_path(
        r'^api/user',
        UserView.as_view(),
        name='user_view'
    ),
    re_path(
        r'^api/password_reset',
        PasswordReset.as_view()
    ),
    # jwt
    re_path(
        r'^api/(?P<version>[-\w]+)/auth/obtain_token/',
        obtain_jwt_token,
        name='obtain_token'
        ),
    re_path(
        r'^api/(?P<version>[-\w]+)/auth/refresh_token/',
        refresh_jwt_token,
        name="refresh_token"
        ),
    re_path(
        r'^api/(?P<version>[-\w]+)/auth/verify_jwt_token/',
        verify_jwt_token,
        name="verify_token"
        ),

]
# autocompletes del sistema
urlpatterns += [
    path(
        'node-ac/',
        NodeAutocomplete.as_view(),
        name="node-ac"
        ),
    path(
        'region-ac/',
        RegionAutocomplete.as_view(),
        name="region-ac"
        ),
    path(
        'commune-ac/',
        CommuneAutocomplete.as_view(),
        name="commune-ac"
        ),
    path(
        'street-ac/',
        StreetAutocomplete.as_view(),
        name="street-ac"
        ),
    path(
        'streetlocation-ac/',
        StreetLocationAutocomplete.as_view(),
        name="streetlocation-ac"
        ),
    path(
        'tower-ac/',
        TowerAutocomplete.as_view(),
        name="tower-ac"
        ),
    path(
        'floor-ac/',
        FloorAutocomplete.as_view(),
        name="floor-ac"
        ),
    path(
        'completelocation-ac/',
        CompleteLocationAutocomplete.as_view(),
        name="completelocation-ac"
        ),
    path(
        'operator-ac/',
        OperatorAutocomplete.as_view(),
        name="operator-ac"
        ),

]