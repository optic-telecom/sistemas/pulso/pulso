from rest_framework import viewsets
from users.models import Profile
from users.serializers import (
        ProfileUserReadSerializer,
        ProfileUserCreateSerializer
    )


class ProfileViewset(viewsets.ModelViewSet):
    queryset = Profile.objects.all()

    def get_serializer_class(self):
        if self.request.method.lower() in ['post', 'put', 'patch']:
            return ProfileUserCreateSerializer
        else:
            return ProfileUserReadSerializer
