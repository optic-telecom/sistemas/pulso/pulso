from django.contrib.auth import get_user_model
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.generics import (
    RetrieveAPIView,
    UpdateAPIView,
    )
from users.serializers import (
    UserSerializer,
    CreateUserSerializer
    )
from places.apis.internals.base_viewset import BaseViewSet
from utils.views import send_webhook_transaction
from utils.permissions import IsSystemInternalPermission

User = get_user_model()


class UserViewSet(BaseViewSet):
    """
    create:
        Create a user
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsSystemInternalPermission | IsAuthenticated,)
    model = 'user'

    def get_serializer_class(self):
        if self.request.method.lower() == "post":
            return CreateUserSerializer
        return super(UserViewSet, self).get_serializer_class()

    def get_permissions(self):
        if self.request.method.lower() == "post":
            return [AllowAny()]
        return super(UserViewSet, self).get_permissions()


class UserView(RetrieveAPIView, UpdateAPIView):
    """
    get:
        Connected user information

    put:
        Update user information.

    pacth:
        Update user information.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsSystemInternalPermission | IsAuthenticated,)

    def get_object(self):
        return self.request.user

    def update(self, request, *args, **kwargs):
        """
        Envía una transacción al endpoint de
        WebHookTransaction si no se ha replicado aún
        """
        partial = kwargs.pop('partial', False)
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=request.data, partial=partial
            )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        # Si tiene replicate ya se replicó
        if not replicate:
            send_webhook_transaction(
                model='user', action=2, data=serializer.data
                )

        return Response(serializer.data)

