from rest_framework.generics import (CreateAPIView)
from rest_framework import status
from rest_framework.response import Response
from users.serializers import (PasswordResetSerializer)
from utils.views import send_webhook_transaction


class PasswordReset(CreateAPIView):
    """
    post:
        End point for password change
    """
    serializer_class = PasswordResetSerializer

    def create(self, request, *args, **kwargs):
        """
            Envía una transacción al endpoint de
            WebHookTransaction si no se ha replicado aún
        """
        replicate = request.data.get('replicate', None)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        # Si tiene replicate ya se replicó
        if not replicate:
            send_webhook_transaction(
                model='user', action=1, data=serializer.data
                )
        headers = self.get_success_headers(serializer.data)
        return Response(
                serializer.data,
                status=status.HTTP_201_CREATED,
                headers=headers
            )
