#!/bin/bash

echo Ejecutando Celery para PULSO
LOGFILE=/home/pulso/pulso_backend/logs/celery.log
LOGDIR=$(dirname $LOGFILE)
source /home/pulso/pulso_env/bin/activate
cd /home/pulso/pulso_backend/Pulso
exec celery worker -A tasks -B -l WARNING --logfile=$LOGFILE 2>>$LOGFILE