#!/bin/bash

NAME="Servidor_pulso" # Name of the application
DJANGODIR=/home/pulso/pulso_backend/Pulso/ # Django project directory
LOGFILE=/home/pulso/pulso_backend/logs/gunicorn_pulso.log
LOGDIR=$(dirname $LOGFILE)
SOCKFILE=/home/pulso/pulso_backend/run/gunicorn.sock # we will communicate using this unix socket
LOGDIR_PROJECT=/home/pulso/pulso_backend/logs # we will communicate using this unix socket
USER=pulso # the user to run as
GROUP=pulso # the group to run as
NUM_WORKERS=3 # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=config.settings.$1 # which settings file should Django use
DJANGO_WSGI_MODULE=config.wsgi
TIMEOUT=900

echo "Starting $NAME as `whoami`"

export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR
# Create the run directory if it doesn't exist for logs sentinel
test -d $LOGDIR_PROJECT || mkdir -p $LOGDIR_PROJECT

source /home/pulso/pulso_env/bin/activate

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec /home/pulso/pulso_env/bin/gunicorn  ${DJANGO_WSGI_MODULE}:application \
--name $NAME \
--workers $NUM_WORKERS \
--user=$USER --group=$GROUP \
--bind=unix:$SOCKFILE \
--log-level=debug \
--timeout $TIMEOUT \
--capture-output \
--log-file=$LOGFILE 2>>$LOGFILE
